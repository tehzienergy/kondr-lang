<?php include VIEWPATH . $template_path . 'common/header.php'; ?>

<div class="container mx-6">
	<?php if (isset($orders) && !empty($orders)) { ?>
		<div class="row">
			<h2 class="mb-5"><?php echo lang('application_user')?>:</h2>
			<?php foreach ($orders as $order) { ?>

				<div class="col-xl-4">

					<a href="/project/order/<?php echo $order['id'] ?>"
					   class="card bg-<?php echo $statuses[$order['status']]['color']; ?> hoverable card mb-5 mb-xl-8 text-white">
						<div class="card-header">
							<div class="card-title text-white">
								<div><?php echo $order['id'] ?></div>
								<div class="ms-auto"><?php echo $statuses[$order['status']]['name'] ?></div>
							</div>
						</div>
					</a>
				</div>


			<?php } ?>



		</div>
	<?php } else { ?>
		<?php echo lang('application_user_none')?>
	<?php } ?>

</div>


<?php include VIEWPATH . $template_path . 'common/footer.php'; ?>
