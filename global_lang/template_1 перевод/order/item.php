<?php include VIEWPATH . $template_path . 'common/header.php'; ?>

<div class="container mx-md-6">
	<?php if (isset($order) && !empty($order)) { ?>


		<ol class="breadcrumb text-muted fs-6 fw-bold mb-6">
			<li class="breadcrumb-item pe-3"><a href="#" class="pe-3"><?php echo lang('projects')?></a></li>
			<li class="breadcrumb-item pe-3"><a href="/project/show/<?php echo $project['id'] ?>"
												class="pe-3"><?php echo $project['name'] ?></a></li>
			<li class="breadcrumb-item px-3 text-muted"><?php echo lang('order')?> №<?php echo $order['id'] ?></li>

		</ol>

		<div class="row mb-6">
			<div class="col-lg-1"></div>
			<div class="col-lg-10">

				<div class="row mb-6">
					<h1 class="col-xl-auto">
						<?php echo lang('order')?> <strong>№<?php echo $order['id'] ?> </strong> <?php echo lang('project_participation')?>
						<strong><a href="/project/show/<?php echo $project['id'] ?>"><?php echo $project['name'] ?></a></strong>
					</h1>
					<div class="col-auto fw-bolder ms-auto text-<?php echo $statuses[$order['status']]['color'] ?> mb-3"><?php echo $statuses[$order['status']]['name'] ?></div>
				</div>

				<?php if ($order['status'] == 1) { ?>

					<?php if ($user['tg_nic'] == '@konar777') { ?>
						<?php /*$this->my_functions->vardump($project['added_info_settings']); */ ?><!--
						--><?php /*$this->my_functions->vardump($order['added_info']); */ ?>
					<?php } ?>

					<?php if (!isset($project['added_info_settings']) || count($project['added_info_settings']) == count($order['added_info'])) { ?>


						<h3 class="card-title fw-bold"><?php echo lang('transfer_do')?> <?php echo $order['sum_with_percent'] ?>$ <?php echo lang('wallet_from')?></h3>
						<ul>
							<li><?php echo lang('wallet_number_copy')?></li>
							<li><?php echo lang('wallet_transfer_do')?> <?php echo $user['wallet_address'] ?> <?php echo lang('bep_sum')?> <?php echo $order['sum_with_percent'] ?>$.
							</li>
							<li><strong><?php echo lang('application_tranfer')?></strong> <?php echo lang('application_limit')?>
							</li>
							<li><?php echo lang('payment_less')?></li>
							<li><?php echo lang('test_btn_comment')?></li>
							<li><?php echo lang('operation_comment')?>
							</li>
						</ul>


						<div class="card p-5 shadow-sm mb-5">
							<div class="row">
								<?php if (isset($order['qr'])) { ?>
									<div class="col-auto border-1 ">
										<img src="<?php echo $order['qr'] ?>" alt="">
									</div>
								<?php } ?>
								<div class="col p-5">
									<div class="countdown_block_project align-middle mb-3">
										<?php if (strtotime($order['date_close']) > time()) { ?>

											<div class="zag"><?php echo lang('payment_time')?></div>
											<div class="countdown" id="countdown"></div>


										<?php } ?>
									</div>

									<div class="flex-row mb-6">
										<label for="kt_clipboard_1" class="mb-4"><?php echo lang('payment_transfer_address')?></label>
										<!--begin::Input group-->
										<div class="input-group">
											<!--begin::Input-->
											<input id="kt_clipboard_1" type="text" class="form-control" placeholder=""
												   value="<?php echo $project['cripto_wallet'] ?>"/>
											<!--end::Input-->

											<!--begin::Button-->
											<button class="btn btn-icon btn-light"
													data-clipboard-target="#kt_clipboard_1">
									<span class="svg-icon svg-icon-muted svg-icon-2hx"><svg
												xmlns="http://www.w3.org/2000/svg" width="24" height="24"
												viewBox="0 0 24 24" fill="none">
										<path opacity="0.5"
											  d="M18 2H9C7.34315 2 6 3.34315 6 5H8C8 4.44772 8.44772 4 9 4H18C18.5523 4 19 4.44772 19 5V16C19 16.5523 18.5523 17 18 17V19C19.6569 19 21 17.6569 21 16V5C21 3.34315 19.6569 2 18 2Z"
											  fill="black"/>
										<path fill-rule="evenodd" clip-rule="evenodd"
											  d="M14.7857 7.125H6.21429C5.62255 7.125 5.14286 7.6007 5.14286 8.1875V18.8125C5.14286 19.3993 5.62255 19.875 6.21429 19.875H14.7857C15.3774 19.875 15.8571 19.3993 15.8571 18.8125V8.1875C15.8571 7.6007 15.3774 7.125 14.7857 7.125ZM6.21429 5C4.43908 5 3 6.42709 3 8.1875V18.8125C3 20.5729 4.43909 22 6.21429 22H14.7857C16.5609 22 18 20.5729 18 18.8125V8.1875C18 6.42709 16.5609 5 14.7857 5H6.21429Z"
											  fill="black"/>
											<!--end::Button-->
										</div>
										<!--begin::Input group-->
									</div>
									<div class="flex-row">
										<div class="alert alert-danger d-flex align-items-center p-5 mb-10">
                                    <span class="svg-icon svg-icon-2hx svg-icon-danger me-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
											 viewBox="0 0 24 24" fill="none">
                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"/>
                                        <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)"
											  fill="black"/>
                                        <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)"
											  fill="black"/>
                                        </svg>
                                    </span>
											<div class="d-flex flex-column">
												<h4 class="mb-1 text-danger"><?php echo lang('warning')?></h4>
												<span><?php echo lang('transfer_rules')?></span>
												<span><?php echo lang('transfers_comment')?></span>
											</div>
										</div>

										<p class="fs-3 fw-bolder text-dark">
											<?php echo lang('wallet_amount')?> <?php echo $order['sum_with_percent'] ?>
											$</p>

										<p class="fs-3 fw-bolder text-dark"><?php echo lang('wallet_which')?></p>
										<p><?php echo $user['wallet_address'] ?></p>

									</div>

								</div>
							</div>
							<?php
							// Показываем кнопку только в статусах ожидает опалты, и частично оплачен
							if (in_array($order['status'], [1, 2])) { ?>
								<div class="row">
									<div class="col-12 text-end">


										<button type="button" class="btn btn-bg-warning text-white" id="cancelOrder"
												data-order_id="<?php echo $order['id'] ?>"><?php echo lang('application_cancel')?>
										</button>
										<button type="button" class="btn btn-bg-success text-white"
												id="checkOrderStatus"
												data-order_id="<?php echo $order['id'] ?>"><?php echo lang('payment_check')?>
										</button>
										<!--<button type="button" class="btn btn-bg-success text-white" id="checkPayOrder"
											data-order_id="<?php /*echo $order['id'] */ ?>"><?php echo lang('test')?>
										оплату
									</button>-->
									</div>
								</div>
							<?php } ?>
						</div>

					<?php } ?>

					<?php if (isset($project['added_info_settings']) && !empty($project['added_info_settings'])) { ?>
						<div class="card shadow">
							<div class="card-body">


								<form action="/project/addOrderInfo" method="post" id="addedinfo" class="mb-5">
									<input type="hidden" name="order_id" value="<?php echo $order['id'] ?>"/>

									<h1><?php echo lang('warning')?>!</h1>
									<p><?php echo lang('project_info_add')?>:</p>

									<?php foreach ($project['added_info_settings'] as $setting) { ?>
										<div class="mb-5">


											<?php
											// Проверяем заполнено ли у нас дополнительное поле
											$key_value = array_search($setting['added_info_id'], array_column($order['added_info'], 'added_info_id'));


											$current_value = !is_bool($key_value) && isset($order['added_info'][$key_value]['value']) ? $order['added_info'][$key_value]['value'] : '';


											?>

											<h2 class="mb-3">
												<?php echo lang('wallet')?> <?php echo isset($setting['name']) ? $setting['name'] : '' ?>.
											</h2>

											<?php if ($current_value != '') { ?>
												<div class="saved_value">
													<!-- Если значение уже заполнено -->
													<div class="h3 d-inline-block">
														<?php echo $current_value; ?>
													</div>
													<div class="btn btn-active-icon-primary btn-active-text-primary  edit_add_value"><span
																class="svg-icon svg-icon-1"><svg
																	xmlns="http://www.w3.org/2000/svg" width="24"
																	height="24" viewBox="0 0 24 24" fill="none">
													<path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd"
														  d="M2 4.63158C2 3.1782 3.1782 2 4.63158 2H13.47C14.0155 2 14.278 2.66919 13.8778 3.04006L12.4556 4.35821C11.9009 4.87228 11.1726 5.15789 10.4163 5.15789H7.1579C6.05333 5.15789 5.15789 6.05333 5.15789 7.1579V16.8421C5.15789 17.9467 6.05333 18.8421 7.1579 18.8421H16.8421C17.9467 18.8421 18.8421 17.9467 18.8421 16.8421V13.7518C18.8421 12.927 19.1817 12.1387 19.7809 11.572L20.9878 10.4308C21.3703 10.0691 22 10.3403 22 10.8668V19.3684C22 20.8218 20.8218 22 19.3684 22H4.63158C3.1782 22 2 20.8218 2 19.3684V4.63158Z"
														  fill="black"></path>
													<path d="M10.9256 11.1882C10.5351 10.7977 10.5351 10.1645 10.9256 9.77397L18.0669 2.6327C18.8479 1.85165 20.1143 1.85165 20.8953 2.6327L21.3665 3.10391C22.1476 3.88496 22.1476 5.15129 21.3665 5.93234L14.2252 13.0736C13.8347 13.4641 13.2016 13.4641 12.811 13.0736L10.9256 11.1882Z"
														  fill="black"></path>
													<path d="M8.82343 12.0064L8.08852 14.3348C7.8655 15.0414 8.46151 15.7366 9.19388 15.6242L11.8974 15.2092C12.4642 15.1222 12.6916 14.4278 12.2861 14.0223L9.98595 11.7221C9.61452 11.3507 8.98154 11.5055 8.82343 12.0064Z"
														  fill="black"></path>
													</svg></span><?php echo lang('edit')?>
													</div>

												</div>
											<?php } ?>

											<input
													<?php echo (isset($user['added_info'][$setting['added_info_id']][0]) && count($user['added_info'][$setting['added_info_id']]) > 1) ? 'list="added_info_' . $setting['added_info_id'] . '"' : '' ?>
													type="text"
													class="form-control mb-3 <?php if ($current_value != '') {
														echo 'd-none';
													} ?>"
													name="value[<?php echo $setting['added_info_id'] ?>]"
													value="<?php echo ($current_value == '' && isset($user['added_info'][$setting['added_info_id']][0])) ? $user['added_info'][$setting['added_info_id']][0] : $current_value; ?>"/>

											<?php
											if (isset($user['added_info'][$setting['added_info_id']]) && !empty($user['added_info'][$setting['added_info_id']])) {
												?>
												<datalist
														id="<?php echo 'added_info_' . $setting['added_info_id'] ?>">
													<?php foreach ($user['added_info'][$setting['added_info_id']] as $item) { ?>
													<option value="<?php echo $item ?>">
														<?php } ?>
												</datalist>
											<?php } ?>

										</div>
									<?php } ?>

									<button type="submit"
											class="btn btn-bg-success text-white <?php if (isset($current_value) && $current_value != '') {
												echo 'd-none';
											} ?>">
										<?php echo lang('wallet_save')?>
									</button>


								</form>
							</div>
						</div>
					<?php } ?>


				<?php } elseif ($order['status'] == 3) { ?>

					<div class="card shadow-sm">
						<div class="card-body">
							<table class="table fs-2x ">
								<tr>
									<td><?php echo lang('application_number')?>:</td>
									<td class="text-end fw-bolder"><?php echo $order['id'] ?></td>
								</tr>
								<tr>
									<td><?php echo lang('project')?>:</td>
									<td class="text-end fw-bolder"><?php echo $project['name'] ?></td>
								</tr>
								<tr>
									<td><?php echo lang('enrollment_amount')?>:</td>
									<td class="text-end fw-bolder"><?php echo $order['sum_with_percent'] ?> $</td>
								</tr>
								<tr>
									<td><?php echo lang('commission')?>:</td>
									<td class="text-end fw-bolder"><?php echo $order['sum_with_percent'] - $order['sum'] ?>
										$
									</td>
								</tr>
								<tr>
									<td><?php echo lang('allocation')?>:</td>
									<td class="text-end fw-bolder"><?php echo $order['sum'] ?> $</td>
								</tr>
								<?php if (isset($order['pereplata'])) { ?>
									<tr>
										<td><?php echo lang('overpayment')?>:</td>
										<td class="text-end fw-bolder text-danger"><?php echo $order['pereplata'] ?>$
										</td>
									</tr>
								<?php } ?>
							</table>
							<?php if (isset($order['pereplata'])) { ?>
								<div class="">
									<p><?php echo lang('overflow_reason')?></p>
									<ul>
										<li><?php echo lang('overflow_allocation')?></li>
										<li><?php echo lang('overlow_overdue')?>
										</li>
									</ul>
									<p><?php echo lang('overflow_end')?></p>
								</div>
							<?php } ?>





							<?php if ($user['tg_nic'] == '@konar777') { ?>
								<?php /*$this->my_functions->vardump($order);*/ ?>
								<?php /*$this->my_functions->vardump($user['added_info']);*/ ?>
								<?php /*$this->my_functions->vardump($added_info_initial);*/ ?>
								<?php /*$this->my_functions->vardump($project['added_info_settings']);*/ ?>

							<?php } ?>

						</div>
					</div>

				<?php } elseif ($order['status'] == 5) { ?>

					<p><?php echo lang('application_time_end')?></p>
					<p><?php echo lang('payment_scenario')?></p>
					<ul>
						<li><?php echo lang('tir_comment')?>
						</li>
						<li><?php echo lang('allocation_comment')?>
						</li>
					</ul>
					<p><?php echo lang('application_comment')?></p>

				<?php } ?>
			</div>
			<div class="col-lg-1"></div>
		</div>

		<?php if (isset($operations) and $operations != []) { ?>
			<div class="row mb-6">
				<div class="col-lg-1"></div>
				<div class="col-lg-10">
					<div class="card p-md-5 shadow-sm">
						<div class="card-body">
							<h1 class="my-5"><?php echo lang('transactions')?></h1>
							<table class="table table-hover">
								<thead>
								<tr class="fw-bolder fs-6 text-gray-800  border-bottom  border-gray-400">
									<td><?php echo lang('from')?></td>
									<td><?php echo lang('hash')?></td>
									<td class="d-none d-sm-table-cell"><?php echo lang('token')?></td>
									<td><?php echo lang('sum')?></td>
									<td class="d-none d-sm-table-cell"><?php echo lang('date')?></td>
								</tr>
								</thead>
								<?php foreach ($operations as $operation) { ?>
									<tr class="">
										<td><?php echo substr($operation['from'], 0, 4) . "..." . substr($operation['from'], -8) ?></td>
										<td><?php echo substr($operation['hash'], 0, 4) . "..." . substr($operation['hash'], -8) ?>
										</td>
										<td class="d-none d-sm-table-cell"><?php echo $operation['tokenSymbol'] ?></td>
										<td><?php echo round($operation['value'] / pow(10, $operation['tokenDecimal']), 2) ?></td>
										<td class="d-none d-sm-table-cell"><?php echo $operation['date'] ?></td>
									</tr>
								<?php } ?>
							</table>
							<?php // $this->my_functions->vardump($operations) ?>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>


		<?php $this->my_functions->vardump($not_distribute_operations); ?>

		<?php if (isset($not_distribute_operations) and $not_distribute_operations != []) { ?>
			<div class="row mb-6">
				<div class="col-lg-1"></div>
				<div class="col-lg-10">
					<div class="card p-md-5 shadow-sm">
						<div class="card-body">
							<h1 class="my-5"><?php echo lang('transactions_free')?></h1>
							<table class="table table-hover">
								<thead>
								<tr class="fw-bolder fs-6 text-gray-800  border-bottom  border-gray-400">
									<td><?php echo lang('from')?></td>
									<td><?php echo lang('hash')?></td>
									<td class="d-none d-sm-table-cell"><?php echo lang('token')?></td>
									<td><?php echo lang('sum')?></td>
									<td class="d-none d-sm-table-cell"><?php echo lang('date')?></td>
								</tr>
								</thead>
								<?php foreach ($not_distribute_operations as $operation) { ?>
									<tr class="">
										<td><?php echo $operation['from'] ?></td>
										<td><?php echo substr($operation['hash'], 0, 8) . "..." . substr($operation['hash'], -8) ?>
										</td>
										<td class="d-none d-sm-table-cell"><?php echo $operation['tokenSymbol'] ?></td>
										<td><?php echo round($operation['value'] / pow(10, $operation['tokenDecimal']), 2) ?></td>
										<td class="d-none d-sm-table-cell"><?php echo $operation['date'] ?></td>
									</tr>
								<?php } ?>
							</table>
							<?php // $this->my_functions->vardump($operations) ?>
						</div>
					</div>

				</div>
			</div>
		<?php } ?>


	<?php } else { ?>

		<p><?php echo lang('application_none')?></p>

	<?php } ?>

</div>


<script>
	function setTimer(elem_id, date) {
		// set the date we're counting down to
		var target_date = new Date(date).getTime();
		// variables for time units
		var days, hours, minutes, seconds;
		// get tag element

		var countDownElem = document.getElementById(elem_id);
		//update the tag with id "countdown" every 1 second
		setInterval(function () {

			var offset = ((new Date()).getTimezoneOffset() + 180) * 60 * 1000; // Получаем отклонение от гринвича в минутах, и прибавляем к нему 3 часа (180 минут)

			var current_date = new Date().getTime() + offset;

			var seconds_left = (target_date - current_date) / 1000;
			// do some time calculations
			days = parseInt(seconds_left / 86400);
			seconds_left = seconds_left % 86400;
			hours = parseInt(seconds_left / 3600);
			seconds_left = seconds_left % 3600;
			minutes = parseInt(seconds_left / 60);
			seconds = parseInt(seconds_left % 60);


			// format countdown string + set tag value

			if (current_date < target_date) {
				countDownElem.innerHTML = days + " Д : " + hours + " Ч : " + minutes + " M : " + seconds + " С";
			} else {
				document.querySelectorAll('.countdown_block_project').forEach(function (el) {
					el.style.display = 'none';
				});
			}

		}, 1000);

	}

	<?php if ($order['status'] == 1 ) { ?>
	setTimer("countdown", "<?php echo $order['date_close'] ?>");
	<?php } ?>

</script>

<?php include VIEWPATH . $template_path . 'common/footer.php'; ?>
