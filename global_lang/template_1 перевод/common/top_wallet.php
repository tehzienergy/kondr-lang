<?php if (isset($user)) { ?>

	<div class="row mb-1 notranslate">
		<div class="col-md-6">
			<p class="mb-2 text-gray-600"><?php echo lang('on_your_wallet') ?> <?php echo substr($user['wallet_address'], 0, 8) . "..." . substr($user['wallet_address'], -8) ?>
				<!--begin::Svg Icon | path: assets/media/icons/duotune/general/gen055.svg-->
				<a href="/user/info/">
            <span class="svg-icon svg-icon-muted svg-icon-4 ms-2"><svg
						xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
<path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd"
	  d="M2 4.63158C2 3.1782 3.1782 2 4.63158 2H13.47C14.0155 2 14.278 2.66919 13.8778 3.04006L12.4556 4.35821C11.9009 4.87228 11.1726 5.15789 10.4163 5.15789H7.1579C6.05333 5.15789 5.15789 6.05333 5.15789 7.1579V16.8421C5.15789 17.9467 6.05333 18.8421 7.1579 18.8421H16.8421C17.9467 18.8421 18.8421 17.9467 18.8421 16.8421V13.7518C18.8421 12.927 19.1817 12.1387 19.7809 11.572L20.9878 10.4308C21.3703 10.0691 22 10.3403 22 10.8668V19.3684C22 20.8218 20.8218 22 19.3684 22H4.63158C3.1782 22 2 20.8218 2 19.3684V4.63158Z"
	  fill="black"/>
<path d="M10.9256 11.1882C10.5351 10.7977 10.5351 10.1645 10.9256 9.77397L18.0669 2.6327C18.8479 1.85165 20.1143 1.85165 20.8953 2.6327L21.3665 3.10391C22.1476 3.88496 22.1476 5.15129 21.3665 5.93234L14.2252 13.0736C13.8347 13.4641 13.2016 13.4641 12.811 13.0736L10.9256 11.1882Z"
	  fill="black"/>
<path d="M8.82343 12.0064L8.08852 14.3348C7.8655 15.0414 8.46151 15.7366 9.19388 15.6242L11.8974 15.2092C12.4642 15.1222 12.6916 14.4278 12.2861 14.0223L9.98595 11.7221C9.61452 11.3507 8.98154 11.5055 8.82343 12.0064Z"
	  fill="black"/>
</svg></span>
					<!--end::Svg Icon-->
				</a>
			<p class="big-gradient"><?php echo number_format($user['sum'], 2, ',', ' ') ?> KON</p>
		</div>
		<div class="col-md-6">

			<?php
			// Проверяем доступы к продуктовым тирам
			if (isset($user['portfolios']) && !empty($user['portfolios'])) {
				$product_tears_array = [];

				foreach ($user['portfolios'] as $portfolio) {

					$key = array_search($portfolio['portfolio_id'], array_column($tirs_tree, 'portfolio_id', 'id'));

					if (!is_bool($key)) {
						$product_tears_array[$key] = $tirs_tree[$key]['name'];
					}

				}

			}
			?>


			<p class="mb-2 text-gray-600"><?php echo lang('in_tirs') ?></p>
			<?php if (isset($tirs_tree[$user['active_tir']])) { ?>
				<p class="big-gradient"><?php echo $tirs_tree[$user['active_tir']]['name'] ?>
					<?php if (isset($product_tears_array) && !empty($product_tears_array)) { ?>
						<?php echo ' + ' . implode(', ', $product_tears_array) ?>
					<?php } ?>
				</p>
			<?php } elseif (isset($product_tears_array) && !empty($product_tears_array)) { ?>
				<p class="big-gradient"><?php echo implode(', ', $product_tears_array) ?></p>
			<?php } else { ?>
				<p class="big-gradient"><?php echo lang('tir_none')?></p>

			<?php } ?>
		</div>
	</div>

<?php } ?>
