</div>
<!--end::Post-->
</div>
<!--end::Content-->

<!--layout-partial:layout/_footer.html-->

</div>
<!--end::Wrapper-->
</div>
<!--end::Page-->
</div>
<!--end::Root-->


<footer>

	<div class="modal fade" tabindex="-1" id="modal_buy_kon">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-body text-center p-10">

					<h1><?php echo lang('kon_buy_do')?></h1>
					<div class="my-5">ETH</div>

					<div class="mb-5">
						<a class="btn btn-kondr-warning fw-bolder w-300px"
						   href="https://app.uniswap.org/#/swap?inputCurrency=0x5a520e593f89c908cd2bc27d928bc75913c55c42"
						   target="_blank">
						<span class="icon me-3">
							<img src="/assets/img/Uniswap.svg" alt="">
						</span>
							<?php echo lang('buy_uniswap')?>
						</a>
					</div>
					<div class="mb-5">
						<a class="btn btn-kondr-warning fw-bolder w-300px"
						   href="https://www.dextools.io/app/ether/pair-explorer/0xd5aa74dd2446656f15c6de748066692607cddc69"
						   target="_blank">
						<span class="icon me-3">
							<img src="/assets/img/Dextools.svg" alt="">
						</span>
							<?php echo lang('buy_dextools')?>
						</a>
					</div>

					<div class="my-5">BSC</div>

					<div class="mb-5">
						<a class="btn btn-kondr-warning fw-bolder w-300px"
						   href="https://pancakeswap.finance/swap?outputCurrency=0x97ff368E832f76B0eAC4922F664921c11399fc11"
						   target="_blank">
						<span class="icon me-3">
							<img src="/assets/img/PancakeSwap.svg" alt="">
						</span>
							<?php echo lang('buy_pancake')?>
						</a>
					</div>
					<div class="mb-5">
						<a class="btn btn-kondr-warning fw-bolder w-300px"
						   href="https://poocoin.app/tokens/0x97ff368e832f76b0eac4922f664921c11399fc11"
						   target="_blank">
						<span class="icon me-3">
							<img src="/assets/img/Pookoin.svg" alt="">
						</span>
							<?php echo lang('buy_pookoin')?>
						</a>
					</div>

					<a href="https://bridge.kondr.io" class="text-gray-600 mt-10 d-block text-decoration-underline">
						<?php echo lang('kon_bep_access')?>
					</a>

				</div>

			</div>
		</div>
	</div>

</footer>
<script>var hostUrl = "/assets/metronic";</script>
<!--begin::Javascript-->
<!--begin::Global Javascript Bundle(used by all pages)-->
<script src="/assets/metronic/plugins/global/plugins.bundle.js"></script>
<script src="/assets/metronic/js/scripts.bundle.js"></script>
<!--end::Global Javascript Bundle-->
<!--begin::Page Vendors Javascript(used by this page)-->
<script src="/assets/metronic/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
<!--end::Page Vendors Javascript-->
<!--begin::Page Custom Javascript(used by this page)-->
<script src="/assets/metronic/js/custom/widgets.js"></script>
<script src="/assets/metronic/js/custom/apps/chat/chat.js"></script>
<script src="/assets/metronic/js/custom/modals/create-app.js"></script>
<script src="/assets/metronic/js/custom/modals/upgrade-plan.js"></script>
<script src="/assets/metronic/js/custom/intro.js"></script>
<script src="/assets/js/flipclock.min.js"></script>
<!--end::Page Custom Javascript-->
<!--end::Javascript-->


<script src="/assets/js/myscript.js"></script>

<script>

	<?php if(isset($calendar)) { ?>
	const months = ['<?php echo $filter_vesting['time_start']?>'];

	var todayDate = moment().startOf("day");
	var YM = todayDate.format("YYYY-MM");
	var YESTERDAY = todayDate.clone().subtract(1, "day").format("YYYY-MM-DD");
	var TODAY = todayDate.format("YYYY-MM-DD");
	var TOMORROW = todayDate.clone().add(1, "day").format("YYYY-MM-DD");

	var calendarEl = document.getElementById("vestingCalendar");
	var calendar = new FullCalendar.Calendar(calendarEl, {
		/*headerToolbar: {
			left: "prev,next today",
			center: "title",
			right: "dayGridMonth,timeGridWeek,timeGridDay,listMonth"
		},*/
		headerToolbar: false,

		height: 800,
		contentHeight: 780,
		aspectRatio: 3,  // see: https://fullcalendar.io/docs/aspectRatio

		nowIndicator: true,
		//now: TODAY + "T09:25:00", // just for demo

		views: {
			dayGridMonth: {buttonText: "month"},
			timeGridWeek: {buttonText: "week"},
			timeGridDay: {buttonText: "day"}
		},

		initialView: "dayGridMonth",
		//initialDate: '2022-05-31',

		editable: false,
		dayMaxEvents: true, // allow "more" link when too many events
		navLinks: false,
		events: <?php echo json_encode($events)?>,

		eventContent: function (info) {
			var element = $(info.el);


			if (info.event.extendedProps && info.event.extendedProps.description) {

				if (element.find(".fc-event-title").lenght !== 0) {
					//console.log(info.event.extendedProps.description);
					//element.find(".fc-event-title").append('<div class="fc-description">' + info.event.extendedProps.description + '</div>');

				}

			}
		}
	});

	calendar.render();

	<?php } ?>

	/* Преключение месяцев в каендаре вестинга */
	$('.next_month_rounds, .prew_month_rounds').on('click', function () {

		let button = $(this);
		let date_start = $('#date_start').data('date_start');

		console.log(date_start);

		let new_date_start = new Date(date_start);

		let current_month = new_date_start.getMonth();

		if (button.hasClass('prew_month_rounds')) {
			new_date_start.setMonth(current_month - 1);
			if (calendar) {
				calendar.prev();
			}
		}
		if (button.hasClass('next_month_rounds')) {
			new_date_start.setMonth(current_month + 1);
			if (calendar) {
				calendar.next();
			}
		}

		const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
			"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
		];

		$('#date_start').data('date_start', new_date_start.toJSON().slice(0, 10));
		$('#date_start').text(monthNames[new_date_start.getMonth()] + ', ' + new_date_start.getFullYear());

		console.log(new_date_start.toJSON().slice(0, 10));

		const data = {
			'time_start': new_date_start.toJSON().slice(0, 10),
			'ajax': true,
		}

		getRounds(data);

	});

	function getRounds(data) {

		if ($("input[name='filter_my']:checked").val()) {
			data.filter_my = 1
		}
		if ($("input[name='filter_everyday']:checked").val()) {
			data.filter_everyday = 1
		}

		$.ajax({
			url: '/user/vestingcalendar',
			type: 'get',
			data: data,
			dataType: 'json',
			success: function (response) {
				//console.log(response);

				$('#contentVesting').html(response.content);


				if (months.includes(response.filter_vesting.time_start) === false) {
					months.push(response.filter_vesting.time_start);

					$(response.events).each(function (k, event) {
						//console.log(event);
						calendar.addEvent(event);
					})

				}


			}
		});

	}

	// Клик по событию в календаре
	$('.vestingCalendarBlock').on('click', 'a.fc-event', function (e) {
		e.preventDefault();
		let link = $(this);

		let href = document.location.origin + link.attr('href');
		;

		$.ajax({
			url: href,
			type: 'get',
			success: function (response) {
				console.log(response);

				$("#modalVesting").modal("show");

				$('.modal-body').html(response);
				$('.modal-title').html('Vesting info');
			}
		});

	})


	// Включить показ только своих проектов в которых принимали участие
	$('input[name="filter_my"]').on('change', function () {
		if ($(this).is(':checked')) {
			$('.main-container').addClass('only_my_projects');
		} else {
			$('.main-container').removeClass('only_my_projects');
		}
	});

	// Включить показ ежедневных вестингов
	$('input[name="filter_everyday"]').on('change', function () {

		if ($(this).is(':checked')) {
			$('.main-container').removeClass('hide_every_day');
		} else {
			$('.main-container').addClass('hide_every_day');
		}

	});

	// Переключает показ на календарь вестинга или на список
	$('.toogle_vesting_calendar').on('click', function () {
		$('.toogle_vesting_calendar').removeClass('active');
		$('.toogle_vesting_calendar').removeClass('btn-info');

		$(this).addClass('active');
		$(this).addClass('btn-info');

		if ($(this).hasClass('sh_calendar')) {
			$('.main-container').removeClass('d-calendar');
			$('.main-container').addClass('d-contentVesting');
		} else {

			$('.main-container').removeClass('d-contentVesting');
			$('.main-container').addClass('d-calendar');
			calendar.render();
		}
	});




	<?php if(isset($term_page_id)) { ?>

	/* Считаем количество процентов прокрутки и записываем в базу дошел ли до конца */
	jQuery(document).ready(function ($) {

		$('.modal-footer').on('click', function () {
			//$('#term_button_popup_agree, #term_button_popup_cancel').removeAttr('disabled');
		})

		$('#term_button_popup_agree, #term_button_popup_cancel').on('click', function () {

			var myModalEl = document.getElementById('exampleModal');
			var modal = bootstrap.Modal.getInstance(myModalEl)
			modal.hide();

			$('#term_button_agree, #term_button_cancel').removeAttr('disabled');
		})

		var top_percent = 0;

		$('.terms_modal .modal-body').scroll(function () {

			var s = $(this).scrollTop();
			var f = $(this).prop('scrollHeight') - $(this).outerHeight();

			var d = s / f * 100;
			var p = Math.round(d);

			if (p % 20 == 0 && top_percent != p) {
				top_percent = p;

				if (p == 100) {
					$('#term_button_agree, #term_button_cancel').removeAttr('disabled');
					$('#term_button_popup_agree, #term_button_popup_cancel').removeAttr('disabled');

				}

				//console.log(p);

				$.ajax({
					url: '/user/addScrollLog',
					type: 'post',
					data: {
						'top_percent': p,
						'term_id': <?php echo($term_page_id) ?>,
						'action': 'scroll'
					},
					dataType: 'json',
					success: function (response) {
						console.log(response);
					}
				});

			}

		});

	});

	// Отправка подтверждения или отказа от подписи соглашения
	$('#term_button_cancel, #term_button_agree').on('click', function () {
		$.ajax({
			url: '/user/ajaxTerms',
			type: 'post',
			data: {
				'status': $(this).data('status'),
				'term_id': <?php echo($term_page_id) ?>,
			},
			dataType: 'json',
			success: function (response) {
				if (response.status) {
					location.href = '/';
				}
				console.log(response);
			}
		});
	});

	<?php } ?>

	$('button.save_action').on('click', function () {
		let tarif_id = $('#selected_tarif_vesting').val();
		let type_id = $('#selected_type_vesting').val();

		$.ajax({
			url: '/user/changeAction',
			type: 'post',
			data: {
				tarif_id,
				type_id
			},
			dataType: 'json',
			success: function (response) {
				if (response.status) {
					location.reload();
				}
				console.log(response);
			}
		});

	});

	$('button.save_action_stacking').on('click', function () {
		let tarif_id = $('#selected_tarif').val();
		let type_id = $('#selected_type').val();
		var stacking_years = document.querySelector('input[name="stacking_years"]:checked').value;

		$.ajax({
			url: '/user/changeAction',
			type: 'post',
			data: {
				tarif_id,
				type_id,
				stacking_years
			},
			dataType: 'json',
			success: function (response) {
				if (response.status) {
					location.reload();
				}
				console.log(response);
			}
		});

	});


	// Добавляет реалные коны в стэйкинг
	$('button.save_real_kon_stacking').on('click', function () {
		let sum = $('#real_kon_sum').val();

		let stacking_years_input = document.querySelector('input[name="stacking_years"]:checked');


		var stacking_years = 0;
		if (stacking_years_input) {
			stacking_years = stacking_years_input.value;
		}

		$.ajax({
			url: '/user/stackingRealKon',
			type: 'post',
			data: {
				sum,
				stacking_years
			},
			dataType: 'json',
			success: function (response) {
				if (response.status) {
					location.reload();
				} else {
					if (response.error) {

						let text = '';

						response.error.forEach(function (value) {
							text += value + '. ';
						})


						Swal.fire({
							text: text,
							icon: "warning",
							buttonsStyling: false,
							confirmButtonText: "Ok",
							customClass: {
								confirmButton: "btn btn-warning"
							}
						});
					}
				}
				console.log(response);
			}

		});

	});


	// Удаляем из стэйкинга real kon
	$('button.save_real_kon_cancel_stacking').on('click', function () {
		let stacking_id = $('#stacking_id').val();

		$.ajax({
			url: '/user/cancelStacking',
			type: 'post',
			data: {
				stacking_id,
			},
			dataType: 'json',
			success: function (response) {
				if (response.status) {
					location.reload();
				}
				console.log(response);
			}
		});

	});

	// Удаляет строку дополнительного кошелька в личном кабинете
	$('.additional_info').on('click', '.delete_added_wallet', function () {
		let removed_wallet = $(this).closest('.saved_wallet');
		removed_wallet.slideUp('normal', function () {
			$(this).remove();
		});
	});

	// Добавляет строку допольнителього кошелька в личном кабинете
	$('.addAddedInfo').on('click', function () {
		let added_id = $(this).data('parent');

		$('#added_info_repeater_' + added_id).append('<div class="saved_wallet"><div class="form-group row"><div class="col-md-8"><input type="text" class="form-control mb-3"	placeholder="Enter value" name="added_info[' + added_id + '][]" value=""/></div><div class="col-md-4"><button			class="btn btn-sm btn-light-danger mt-1 delete_added_wallet"><i class="la la-trash-o"></i><?php echo lang('remove')?></button></div></div></div>');

	})

	// Удаляет все строки и сохраняет если есть в выбранном типе дополнительной информации
	$('.saveAddedInfo').on('click', function () {


		let button = $(this);
		let keyAddedInfo = button.data('id_added_info');

		let values = $("input[name='added_info[" + keyAddedInfo + "][]']")
			.map(function () {
				return $(this).val();
			}).get();


		button.prop('disabled', true);
		$.ajax({
			url: '/user/saveAddedInfo',
			type: 'post',
			data: {
				keyAddedInfo,
				values
			},
			dataType: 'json',
			success: function (response) {
				button.prop('disabled', false);
				console.log(response);
				if (response.status) {

					Swal.fire({
						text: "Данные успешно обновлены",
						icon: "success",
						buttonsStyling: false,
						confirmButtonText: "ok",
						customClass: {
							confirmButton: "btn btn-primary"
						}
					});

				}
			}
		});

		button.prop('disabled', false);
	});

	// Проверка кошелька на поступление оплаты по заявке на покупку KON
	$('#checkPayOrder').on('click', function () {
		let button = $(this);

		button.prop('disabled', true);
		$.ajax({
			url: '/project/checkPayOrder/' + button.data('order_id'),
			type: 'get',
			dataType: 'json',
			success: function (response) {
				button.prop('disabled', false);
				console.log(response);
				if (response.status) {

					Swal.fire({
						text: "<?php echo lang('payment_credited')?>",
						icon: "success",
						buttonsStyling: false,
						confirmButtonText: "<?php echo lang('go_to_the_project')?>",
						customClass: {
							confirmButton: "btn btn-primary"
						}
					});

					setInterval(function () {
						location.reload();
					}, 2000);

				} else {

					Swal.fire({
						text: "<?php echo lang('no_new_operations')?>",
						icon: "warning",
						buttonsStyling: false,
						confirmButtonText: "Ok",
						customClass: {
							confirmButton: "btn btn-warning"
						}
					});

				}
			}
		});
	});

	// Проверка статуса заявки. Если оплачен - или просрочен - перезагрузить
	$('#checkOrderStatus').on('click', function () {
		let button = $(this);

		button.prop('disabled', true);
		$.ajax({
			url: '/project/checkOrderStatus/' + button.data('order_id'),
			type: 'get',
			dataType: 'json',
			success: function (response) {
				button.prop('disabled', false);
				console.log(response);
				if (response.status) {

					if (response.status == 3) {
						Swal.fire({
							text: "<?php echo lang('payment_credited')?>",
							icon: "success",
							buttonsStyling: false,
							confirmButtonText: "<?php echo lang('go_to_the_project')?>",
							customClass: {
								confirmButton: "btn btn-primary"
							}
						});
					}

					if (response.status == 5) {
						Swal.fire({
							text: "<?php echo lang('the_application_is_overdue')?>",
							icon: "warning",
							buttonsStyling: false,
							confirmButtonText: "<?php echo lang('go_to_the_project')?>",
							customClass: {
								confirmButton: "btn btn-warning"
							}
						});
					}


					setInterval(function () {
						location.reload();
					}, 2000);

				} else {

					Swal.fire({
						text: "<?php echo lang('no_new_operations')?>",
						icon: "warning",
						buttonsStyling: false,
						confirmButtonText: "Ok",
						customClass: {
							confirmButton: "btn btn-warning"
						}
					});

				}
			}
		});
	});


	$('form#addedinfo').submit(function (e) {
		//отмена действия по умолчанию для кнопки submit
		e.preventDefault();

		var form = $(this);
		$.ajax({
			type: form.attr('method'),
			url: form.attr('action'),
			data: form.serialize(),
			dataType: 'json',
		}).done(function (response) {
			if (response.status === true) {

				Swal.fire({
					text: "Данные успешно сохранены",
					icon: "success",
					buttonsStyling: false,
					confirmButtonText: "ok",
					customClass: {
						confirmButton: "btn btn-primary"
					}
				});

				$('button', form).hide();

				setInterval(function () {
					location.reload();
				}, 2000);

			} else {
				if (response.error) {

					let text = '';

					response.error.forEach(function (value) {
						text += value;
					})


					Swal.fire({
						text: text,
						icon: "warning",
						buttonsStyling: false,
						confirmButtonText: "Ok",
						customClass: {
							confirmButton: "btn btn-warning"
						}
					});
				}
				console.log(response);
			}
		}).fail(function () {
			console.log('fail');
		});

	});

	// Клик по редактированию дополнительной информации в заявке на участие в проекте
	$('.edit_add_value').on('click', function () {
		$('.saved_value').hide();
		$('form#addedinfo button, form#addedinfo input').removeClass('d-none');
	});


</script>

<!-- Функция инициализации скрипта добавления строк -->
<?php if (isset($repeater_ids)) { ?>
	<?php
	foreach ($repeater_ids as $key => $value) {
		$repeater_ids[$key] = '#' . $value;
	}
	?>
	<script src="/assets/metronic/plugins/custom/formrepeater/formrepeater.bundle.js"></script>
	<script>
		$('<?php echo implode(',', $repeater_ids)?>').repeater({
			initEmpty: true,

			defaultValues: {
				'text-input': 'foo'
			},

			show: function () {
				$(this).slideDown();
			},

			hide: function (deleteElement) {
				$(this).slideUp(deleteElement);
			}
		});
	</script>
<?php } ?>


<!--  Калькулятор  -->

<script>
	var kt_slider_bitcoin = document.querySelector("#kt_slider_bitcoin");
	if (kt_slider_bitcoin) {
		noUiSlider.create(kt_slider_bitcoin, {
			start: [100000],
			connect: true,
			step: 100,
			//tooltips: [wNumb({decimals: 0})],
			range: {
				"min": 0,
				"max": 200000
			}
		});

		var kt_slider_bitcoin_val = document.querySelector("#kt_slider_bitcoin_val");
		kt_slider_bitcoin.noUiSlider.on("update", function (values, handle) {
			//kt_slider_bitcoin_val.innerHTML = new Intl.NumberFormat('ru-RU').format(Math.round(values[handle]));
			kt_slider_bitcoin_val.innerHTML = Math.round(values[handle]);

			calc_stacking();
		});


	}

	var kt_slider_sskon = document.querySelector("#kt_slider_sskon");
	if (kt_slider_sskon) {
		noUiSlider.create(kt_slider_sskon, {
			start: [1],
			connect: true,
			range: {
				"min": 0.5,
				"max": 10
			}
		});

		var kt_slider_sskon_val = document.querySelector("#kt_slider_sskon_val");
		kt_slider_sskon.noUiSlider.on("update", function (values, handle) {
			kt_slider_sskon_val.innerHTML = values[handle];

			calc_stacking();
		});

	}

	var kt_slider_spkon = document.querySelector("#kt_slider_spkon");
	if (kt_slider_spkon) {
		noUiSlider.create(kt_slider_spkon, {
			start: [<?php echo (isset($get['buyprice'])) ? $get['buyprice'] : 0.25 ?>],
			connect: true,
			range: {
				"min": 0.15,
				"max": 1
			}
		});

		var kt_slider_spkon_val = document.querySelector("#kt_slider_spkon_val");
		kt_slider_spkon.noUiSlider.on("update", function (values, handle) {
			kt_slider_spkon_val.innerHTML = values[handle];

			calc_stacking();
		});


	}

	var kt_slider_sdkon = document.querySelector("#kt_slider_sdkon");
	if (kt_slider_sdkon) {
		noUiSlider.create(kt_slider_sdkon, {
			start: [10],
			connect: true,
			range: {
				"min": 0,
				"max": 30
			}
		});

		var kt_slider_sdkon_val = document.querySelector("#kt_slider_sdkon_val");
		kt_slider_sdkon.noUiSlider.on("update", function (values, handle) {
			kt_slider_sdkon_val.innerHTML = Math.round(values[handle]);

			calc_stacking();
		});
	}


	var kt_slider_pul = document.querySelector("#kt_slider_pul");
	if (kt_slider_pul) {
		noUiSlider.create(kt_slider_pul, {
			start: [10, 30],
			connect: [true, true, true],
			range: {
				"min": 0,
				"max": 99
			}
		});

		var connect = kt_slider_pul.querySelectorAll('.noUi-connect');
		var classes = ['c-1-color', 'c-2-color', 'c-3-color'];

		for (var i = 0; i < connect.length; i++) {
			connect[i].classList.add(classes[i]);
		}

		var kt_slider_pul_1 = document.querySelector("#kt_slider_pul_1");
		var kt_slider_pul_2 = document.querySelector("#kt_slider_pul_2");
		var kt_slider_pul_3 = document.querySelector("#kt_slider_pul_3");

		kt_slider_pul.noUiSlider.on("update", function (values, handle) {

			// Если первый пользунок за первый год
			if (handle == 0) {
				kt_slider_pul_3_val = Number(kt_slider_pul_3.innerHTML);
				kt_slider_pul_1.innerHTML = Math.round(values[handle]);
				kt_slider_pul_2.innerHTML = Math.round(100 - kt_slider_pul_3_val - values[handle]);


			}
			// Если второй ползунок за второй год
			if (handle == 1) {
				kt_slider_pul_1_val = kt_slider_pul_1.innerHTML;

				kt_slider_pul_2.innerHTML = Math.round(values[handle] - kt_slider_pul_1_val);
				kt_slider_pul_3.innerHTML = Math.round(100 - values[handle]);
			}

			calc_stacking();

		});
	}

	function calc_stacking() {
		var sumStacking = $('input[name="sumStacking"]').val();
		if (sumStacking != '') {
			var srok = $('select[name="srok"]').val();


			$('.srok_2, .srok_3, .srok_4').removeClass('d-none').hide();

			if (srok == 2) {
				$('.srok_2').show();
			}
			if (srok == 3) {
				$('.srok_2, .srok_3, .srok_4').show();
			}

			var fix_percent = 0;
			var fix_srok = 0;
			var fix_srok1 = sumStacking * 0.15;
			var fix_srok2 = 0;
			var fix_srok3 = 0;

			if (srok > 1) {
				fix_srok2 = sumStacking * 0.2;
			}
			if (srok > 2) {
				fix_srok3 = sumStacking * 0.25;
			}

			fix_srok = fix_srok1 + fix_srok2 + fix_srok3;
			if (fix_srok != 0 && sumStacking != 0) {
				fix_percent = fix_srok / sumStacking * 100;
			}

			$('#fix_percent').text(fix_percent.toFixed(2));
			$('.fix_part .srok span').text(new Intl.NumberFormat('ru-RU').format(Math.round(fix_srok)));
			$('.fix_part .srok_1 span').text(new Intl.NumberFormat('ru-RU').format(Math.round(fix_srok1)));
			$('.fix_part .srok_2 span').text(new Intl.NumberFormat('ru-RU').format(Math.round(fix_srok2)));
			$('.fix_part .srok_3 span').text(new Intl.NumberFormat('ru-RU').format(Math.round(fix_srok3)));

			var bitcoin_val = parseFloat($('#kt_slider_bitcoin_val').html());
			var sskon_val = parseFloat($('#kt_slider_sskon_val').html());
			var spkon_val = parseFloat($('#kt_slider_spkon_val').html());
			var sdkon_val = parseFloat($('#kt_slider_sdkon_val').html());

			var total_kon = 20000000;
			var year_const_1 = Math.round((bitcoin_val * 4.13 * sdkon_val) / sskon_val / 2);
			var year_const_2 = Math.round((bitcoin_val * 10.80 * sdkon_val) / sskon_val / 2);
			var year_const_3 = Math.round((bitcoin_val * 12 * sdkon_val) / sskon_val / 2);
			var year_const_4 = Math.round((bitcoin_val * 7.92 * sdkon_val) / sskon_val / 2);


			var float_percent = 0;
			var float_srok = 0;
			var float_srok1 = sumStacking * year_const_1 / total_kon;
			var float_srok2 = 0;
			var float_srok3 = 0;
			var float_srok4 = 0;

			var pul_percent_1 = parseInt($('#kt_slider_pul_1').html());
			var pul_percent_2 = parseInt($('#kt_slider_pul_2').html());
			var pul_percent_3 = parseInt($('#kt_slider_pul_3').html());

			if (srok > 1) {
				float_srok2 = sumStacking * year_const_2 / (total_kon - total_kon / 100 * pul_percent_1);
			}
			if (srok > 2) {
				float_srok3 = sumStacking * year_const_3 / (total_kon - total_kon / 100 * (pul_percent_2 + pul_percent_1));
				float_srok4 = sumStacking * year_const_4 / (total_kon - total_kon / 100 * (pul_percent_2 + pul_percent_1));
			}

			float_srok = float_srok1 + float_srok2 + float_srok3 + float_srok4;
			if (float_srok != 0 && sumStacking != 0) {
				float_percent = float_srok / sumStacking * 100;
			}

			$('#float_percent').text(float_percent.toFixed(2));
			$('.float_part .srok span').text(new Intl.NumberFormat('ru-RU').format(Math.round(float_srok)));
			$('.float_part .srok_1 span').text(new Intl.NumberFormat('ru-RU').format(Math.round(float_srok1)));
			$('.float_part .srok_2 span').text(new Intl.NumberFormat('ru-RU').format(Math.round(float_srok2)));
			$('.float_part .srok_3 span').text(new Intl.NumberFormat('ru-RU').format(Math.round(float_srok3)));
			$('.float_part .srok_4 span').text(new Intl.NumberFormat('ru-RU').format(Math.round(float_srok4)));


			/* Считаем итоговый блок */
			var profit_itog_percent = 0;
			var profit_itog = +fix_srok + float_srok;
			var profit_itog_dollar = 0;
			var profit_itog_percent_dollar = 0;
			var kon_after = +sumStacking + profit_itog;
			var money_do = +sumStacking * spkon_val;
			var money_after = +kon_after * sskon_val;

			if (profit_itog !== 0) {
				profit_itog_percent = profit_itog / sumStacking * 100;
			}
			profit_itog_dollar = money_after - money_do;

			if (money_after !== 0 && money_do !== 0) {
				profit_itog_percent_dollar = profit_itog_dollar / money_do * 100;
			}

			$('#profit_itog').text(new Intl.NumberFormat('ru-RU').format(Math.round(profit_itog)));
			$('#profit_itog_percent').text(profit_itog_percent.toFixed(2));

			$('#kon_do').text(new Intl.NumberFormat('ru-RU').format(Math.round(sumStacking)));
			$('#kon_after').text(new Intl.NumberFormat('ru-RU').format(Math.round(kon_after)));
			$('#money_do').text(new Intl.NumberFormat('ru-RU').format(Math.round(money_do)));
			$('#money_after').text(new Intl.NumberFormat('ru-RU').format(Math.round(money_after)));
			$('#profit_itog_dollar').text(new Intl.NumberFormat('ru-RU').format(Math.round(profit_itog_dollar)));
			$('#profit_itog_percent_dollar').text(profit_itog_percent_dollar.toFixed(2));

		}
	}

	$('input[name="sumStacking"] , select[name="srok"]').on('change', calc_stacking);


	//Переключение калькулятора расширенный - простой
	$('input[name="simple_extended"').change(function () {
		if ($(this).val() == "simple") {
			$('.calc_block').addClass('simple');
		} else {
			$('.calc_block').removeClass('simple');
		}
	});

</script>


</body>
</html>
