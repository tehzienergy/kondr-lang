<!doctype html>
<html lang="ru">
<head>
	<base href="<?php echo site_url() ?>">
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">


	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">

	<!--begin::Fonts-->
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Russo+One&display=swap" rel="stylesheet">
	<!--end::Fonts-->
	<!--begin::Global Stylesheets Bundle(used by all pages)-->
	<link href="assets/metronic/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css"/>
	<link href="assets/metronic/css/style.bundle.css" rel="stylesheet" type="text/css"/>
	<!--end::Global Stylesheets Bundle-->
	<?php if (isset($calendar)) { ?>
		<link href="/assets/metronic/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet"
			  type="text/css"/>
	<?php } ?>
	<!--
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet"
			  integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
		<link rel="stylesheet" href="/assets/css/floating-labels.css"> -->
	<link rel="stylesheet" href="/assets/css/mystyle_template_1.css">

	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function (m, e, t, r, i, k, a) {
			m[i] = m[i] || function () {
				(m[i].a = m[i].a || []).push(arguments)
			};
			m[i].l = 1 * new Date();
			k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
		})
		(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

		ym(86941430, "init", {
			clickmap: true,
			trackLinks: true,
			accurateTrackBounce: true,
			webvisor: true,
			ecommerce: "dataLayer",
			userParams: {
				<?php if (isset($user['active_tir']) && isset($tirs_tree[$user['active_tir']]['name'])) {
					echo ' tir: "' . $tirs_tree[$user['active_tir']]['name'] . '",';
				} ?>
				<?php if (isset($user['sum'])) {
					echo " sum: " . round($user['sum']) . ",";
				} ?>
				<?php if (isset($user['tg_id'])) {
					echo " UserID: " . $user['tg_id'];
				} ?>
			}
		});
	</script>
	<noscript>
		<div><img src="https://mc.yandex.ru/watch/86941430" style="position:absolute; left:-9999px;" alt=""/></div>
	</noscript>
	<!-- /Yandex.Metrika counter -->
	<script type="text/javascript">
		window.dataLayer = window.dataLayer || [];
	</script>

	<title><?php echo isset($heading) ? $heading : ''; ?></title>

	<style>

	</style>

</head>
<body id="kt_body" <?php if (!isset($term_page_id)) { ?> class="header-fixed header-tablet-and-mobile-fixed aside-enabled aside-fixed" <?php } ?>>

<!--begin::Main-->
<!--begin::Root-->
<div class="d-flex flex-column flex-root">
	<!--begin::Page-->
	<div class="page d-flex flex-row flex-column-fluid">

		<?php if (!isset($term_page_id)) { ?>
			<?php include VIEWPATH . $template_path . 'common/metronic/aside/base.php'; ?>
		<?php } ?>

		<!--begin::Wrapper-->
		<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
			<?php if (!isset($term_page_id)) { ?>
				<?php include VIEWPATH . $template_path . 'common/metronic/header/base.php'; ?>
			<?php } ?>

			<?php // include VIEWPATH . $template_path . 'common/metronic/toolbar/toolbar.php'; ?>

			<!--begin::Content-->
			<div class="content d-flex flex-column flex-column-fluid bg-gray-200" id="kt_content">


				<!--begin::Post-->
				<div class="post d-flex flex-column" id="kt_post">

					<?php if (isset($user['tg_nic']) && is_bool(strpos($_SERVER['REQUEST_URI'], '/user')) && (!isset($user['status']) || $user['status'] != 1)) { ?>

						<div class="container-fluid">
							<div class="row mb-5 mb-md-10">
								<div class="col-12">


									<!-- ВЕрификация отклонена -->



								</div>
							</div>
						</div>

					<?php } ?>



					<?php if (isset($user['tg_nic']) && is_bool(strpos($_SERVER['REQUEST_URI'], '/user')) && (!isset($user['approved'])
									|| $user['approved'] != 1)) { ?>

						<div class="container-fluid">
							<div class="row mb-5 mb-md-10">
								<div class="col-12">

									<?php if (isset($user['status']) && $user['status'] == 0) { ?>

										<!-- ВЕрификация отклонена -->

										<div class="alert d-flex border-dashed bg-gray-100 p-5 mb-5">
											<!--begin::Svg Icon | path: icons/duotune/general/gen048.svg-->
											<span class="svg-icon svg-icon-2hx svg-icon-danger me-4">


											<svg width="34" height="34" viewBox="0 0 34 34" fill="none"
												 xmlns="http://www.w3.org/2000/svg">
												<path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd"
													  d="M17.2557 30.2696C24.8225 30.2696 30.9566 24.1355 30.9566 16.5687C30.9566 9.00181 24.8225 2.86767 17.2557 2.86767C9.68883 2.86767 3.55469 9.00181 3.55469 16.5687C3.55469 24.1355 9.68883 30.2696 17.2557 30.2696Z"
													  fill="#FF2525"/>
												<rect x="15.8857" y="9.71816" width="2.7402" height="10.9608" rx="1"
													  fill="#FF2525"/>
												<rect x="15.8857" y="22.049" width="2.7402" height="2.7402" rx="1"
													  fill="#FF2525"/>
											</svg>

								</span>


											<!--end::Svg Icon-->
											<div class="d-flex flex-column">
												<h4 class="mb-2 text-danger"><?php echo lang('wallet_confirmation')?></h4>
												<span class="mb-2"><?php echo lang('wallet_ownership')?></span>
												<!--end::Svg Icon-->
												<div class="d-md-flex">
													<a class="btn btn-kondr-warning d-block fw-bold mb-5 mb-md-0 me-md-5"
													   href="/user/wallet"><?php echo lang('wallet_setting')?></a>
												</div>
											</div>


										</div>

									<?php } ?>

									<?php if (isset($user['approved']) && $user['approved'] == 2) { ?>

										<!-- ВЕрификация отклонена -->

										<div class="alert d-flex border-dashed bg-gray-100 p-5 mb-0">
											<!--begin::Svg Icon | path: icons/duotune/general/gen048.svg-->
											<span class="svg-icon svg-icon-2hx svg-icon-danger me-4">


											<svg width="34" height="34" viewBox="0 0 34 34" fill="none"
												 xmlns="http://www.w3.org/2000/svg">
												<path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd"
													  d="M17.2557 30.2696C24.8225 30.2696 30.9566 24.1355 30.9566 16.5687C30.9566 9.00181 24.8225 2.86767 17.2557 2.86767C9.68883 2.86767 3.55469 9.00181 3.55469 16.5687C3.55469 24.1355 9.68883 30.2696 17.2557 30.2696Z"
													  fill="#FF2525"/>
												<rect x="15.8857" y="9.71816" width="2.7402" height="10.9608" rx="1"
													  fill="#FF2525"/>
												<rect x="15.8857" y="22.049" width="2.7402" height="2.7402" rx="1"
													  fill="#FF2525"/>
											</svg>

								</span>


											<!--end::Svg Icon-->
											<div class="d-flex flex-column">
												<h4 class="mb-2 text-danger"><?php echo lang('auth_denied')?></h4>
												<span class="mb-2"><?php echo lang('auth_fractal_denied')?></span>
												<!--end::Svg Icon-->
												<div class="d-md-flex">
													<a class="btn btn-kondr-warning d-block fw-bold mb-5 mb-md-0 me-md-5"
													   href="/user"><?php echo lang('profile_setting')?></a>
												</div>
											</div>


										</div>

									<?php } elseif (isset($user['approved']) && $user['approved'] == 0) { ?>

										<!-- На модерации -->

										<div class="alert d-flex border-dashed bg-gray-100 p-5 mb-0">
											<!--begin::Svg Icon | path: icons/duotune/general/gen048.svg-->
											<span class="svg-icon svg-icon-2hx svg-icon-danger me-4">


											<svg width="34" height="34" viewBox="0 0 34 34" fill="none"
												 xmlns="http://www.w3.org/2000/svg">
												<path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd"
													  d="M17.2557 30.2696C24.8225 30.2696 30.9566 24.1355 30.9566 16.5687C30.9566 9.00181 24.8225 2.86767 17.2557 2.86767C9.68883 2.86767 3.55469 9.00181 3.55469 16.5687C3.55469 24.1355 9.68883 30.2696 17.2557 30.2696Z"
													  fill="#FF2525"/>
												<rect x="15.8857" y="9.71816" width="2.7402" height="10.9608" rx="1"
													  fill="#FF2525"/>
												<rect x="15.8857" y="22.049" width="2.7402" height="2.7402" rx="1"
													  fill="#FF2525"/>
											</svg>

								</span>
											<!--end::Svg Icon-->
											<div class="d-flex flex-column">
												<h4 class="mb-2 text-danger"><?php echo lang('data_confirmation_pending')?></h4>
												<span class="mb-2"><?php echo lang('fractal_confirmation_comment')?></span>
												<a class="btn btn-kondr-warning d-block fw-bold mb-5 mb-md-0 me-md-5"
												   href="<?php echo 'https://fractal.id/authorize?client_id=' . (isset($domain['kyc_client_id']) ? $domain['kyc_client_id'] : 'jiN1ehEE6Z3CW5eAcH_EqpSz6BJ01CdDvaAyAZSSS98') . '&redirect_uri=https%3A%2F%2F' . (isset($domain['domain']) ? $domain['domain'] : 'lk.kondr.io') . '%2Fuser%2Fafractal&response_type=code&scope=contact%3Aread%20verification.plus%3Aread%20verification.plus.details%3Aread%20verification.liveness%3Aread%20verification.liveness.details%3Aread'; ?>"
												   target="_blank"><?php echo lang('fractal_go')?></a>
											</div>
										</div>

									<?php } else { ?>
										<!--  Не пройдена верификация  -->

										<div class="alert d-flex border-dashed bg-gray-100 p-5 mb-0">
											<!--begin::Svg Icon | path: icons/duotune/general/gen048.svg-->
											<span class="svg-icon svg-icon-2hx svg-icon-danger me-4">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
													 viewBox="0 0 24 24" fill="none">
													<path opacity="0.3"
														  d="M20.5543 4.37824L12.1798 2.02473C12.0626 1.99176 11.9376 1.99176 11.8203 2.02473L3.44572 4.37824C3.18118 4.45258 3 4.6807 3 4.93945V13.569C3 14.6914 3.48509 15.8404 4.4417 16.984C5.17231 17.8575 6.18314 18.7345 7.446 19.5909C9.56752 21.0295 11.6566 21.912 11.7445 21.9488C11.8258 21.9829 11.9129 22 12.0001 22C12.0872 22 12.1744 21.983 12.2557 21.9488C12.3435 21.912 14.4326 21.0295 16.5541 19.5909C17.8169 18.7345 18.8277 17.8575 19.5584 16.984C20.515 15.8404 21 14.6914 21 13.569V4.93945C21 4.6807 20.8189 4.45258 20.5543 4.37824Z"
														  fill="black"></path>
													<path d="M10.5606 11.3042L9.57283 10.3018C9.28174 10.0065 8.80522 10.0065 8.51412 10.3018C8.22897 10.5912 8.22897 11.0559 8.51412 11.3452L10.4182 13.2773C10.8099 13.6747 11.451 13.6747 11.8427 13.2773L15.4859 9.58051C15.771 9.29117 15.771 8.82648 15.4859 8.53714C15.1948 8.24176 14.7183 8.24176 14.4272 8.53714L11.7002 11.3042C11.3869 11.6221 10.874 11.6221 10.5606 11.3042Z"
														  fill="black"></path>
												</svg>
											</span>
											<!--end::Svg Icon-->

											<div class="d-flex flex-column">
												<h4 class="mb-2 text-danger"><?php echo lang('kyc_process')?></h4>
												<span class="mb-2"><?php echo lang('cabinet_access')?></span>
												<span class="fw-bold mb-2"><?php echo lang('kyc_comment')?></span>
												<!--end::Svg Icon-->
												<div class="d-md-flex">

													<a class="btn btn-kondr-warning d-block fw-bold mb-5 mb-md-0 me-md-5"
													   href="<?php echo 'https://fractal.id/authorize?client_id=' . (isset($domain['kyc_client_id']) ? $domain['kyc_client_id'] : 'jiN1ehEE6Z3CW5eAcH_EqpSz6BJ01CdDvaAyAZSSS98') . '&redirect_uri=https%3A%2F%2F' . (isset($domain['domain']) ? $domain['domain'] : 'lk.kondr.io') . '%2Fuser%2Fafractal&response_type=code&scope=contact%3Aread%20verification.plus%3Aread%20verification.plus.details%3Aread%20verification.liveness%3Aread%20verification.liveness.details%3Aread'; ?>">Пройти
														KYC</a>
													<span class="text-center mt-4 text-decoration-underline cursor-pointer"
														  data-bs-toggle="tooltip" data-bs-custom-class="tooltip-dark"
														  data-bs-placement="top"
														  title="<?php echo lang('kyc_profile_auth')?>">
											<?php echo lang('kyc_question')?>
										</span>
												</div>
											</div>


										</div>


									<?php } ?>
								</div>
							</div>
						</div>
					<?php } ?>

					<?php if (is_null($domain) && isset($user['tg_nic']) && is_bool(strpos($_SERVER['REQUEST_URI'], '/user')) && isset($user['terms']) && $user['terms'] == 2) { ?>

						<div class="container-fluid">
							<div class="row mb-5 mb-md-10">
								<div class="col-12">


									<!-- ВЕрификация отклонена -->

									<div class="alert alert-danger d-flex align-items-center p-5 mb-0">
										<!--begin::Svg Icon | path: icons/duotune/general/gen048.svg-->
										<span class="svg-icon svg-icon-2hx svg-icon-danger me-4">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
													 viewBox="0 0 24 24"
													 fill="none">
													<path opacity="0.3"
														  d="M20.5543 4.37824L12.1798 2.02473C12.0626 1.99176 11.9376 1.99176 11.8203 2.02473L3.44572 4.37824C3.18118 4.45258 3 4.6807 3 4.93945V13.569C3 14.6914 3.48509 15.8404 4.4417 16.984C5.17231 17.8575 6.18314 18.7345 7.446 19.5909C9.56752 21.0295 11.6566 21.912 11.7445 21.9488C11.8258 21.9829 11.9129 22 12.0001 22C12.0872 22 12.1744 21.983 12.2557 21.9488C12.3435 21.912 14.4326 21.0295 16.5541 19.5909C17.8169 18.7345 18.8277 17.8575 19.5584 16.984C20.515 15.8404 21 14.6914 21 13.569V4.93945C21 4.6807 20.8189 4.45258 20.5543 4.37824Z"
														  fill="black"></path>
													<path d="M10.5606 11.3042L9.57283 10.3018C9.28174 10.0065 8.80522 10.0065 8.51412 10.3018C8.22897 10.5912 8.22897 11.0559 8.51412 11.3452L10.4182 13.2773C10.8099 13.6747 11.451 13.6747 11.8427 13.2773L15.4859 9.58051C15.771 9.29117 15.771 8.82648 15.4859 8.53714C15.1948 8.24176 14.7183 8.24176 14.4272 8.53714L11.7002 11.3042C11.3869 11.6221 10.874 11.6221 10.5606 11.3042Z"
														  fill="black"></path>
													</svg>
												</span>
										<!--end::Svg Icon-->
										<div class="d-flex flex-column">
											<h4 class="mb-2 text-danger"><?php echo lang('warning')?></h4>
											<span class="mb-2"><?php echo lang('cabinet_denial')?></span>
											<!--end::Svg Icon-->
											<div class="col-12 col-xl-6">
												<a class="btn btn-danger  w-100" href="/user"><?php echo lang('profile_setting')?></a>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
					<?php } ?>
