<?php include VIEWPATH . $template_path . 'common/header.php'; ?>

<div class="container mx-6">

	<h1><?php echo lang('terms')?></h1>

	<?php if(isset($terms)) { ?>
		<ul>
		<?php foreach($terms as $term) { ?>

			<li><a href="/user/terms/<?php echo $term['id']?>"><?php echo $term['name']?></a></li>

		<?php } ?>
		</ul>
	<?php } ?>

</div>



<?php include VIEWPATH . $template_path . 'common/footer.php'; ?>
