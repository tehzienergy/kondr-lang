<?php include VIEWPATH . $template_path . 'common/header.php'; ?>

<div class="container mw-1000px">
	<h1><?php echo lang('agreement_updated')?></h1>

	<p><?php echo lang('read_sign_dscr')?></p>

	<div class="card p-5">
		<div class="card-body">
			<div class="term mb-10">
				<?php echo lang('terms_comment')?>
			</div>


			<!-- Button trigger modal -->

			<div class="row d-flex align-items-center">
				<div class="col-auto">
					<?php echo $term['name'] ?>
				</div>
				<div class="col">
				<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
					<?php echo lang('read')?>
				</button>
				</div>
			</div>

			<?php if (isset($user)) { ?>

				<div class="text-end mt-10">
					<p class="fw-boldest"><?php echo lang('agree_dao')?></p>
					<?php if (!isset($term_agreement) || $term_agreement['status'] != 1) { ?>
						<div class="term_buttons mt-10">
							<button type="button" disabled class="btn btn-secondary" id="term_button_cancel"
									data-status="2">
								<?php echo lang('disagree')?>
							</button>
							<button type="button" disabled class="btn btn-primary ms-3" id="term_button_agree"
									data-status="1"><?php echo lang('agree')?>
							</button>
						</div>
					<?php } else { ?>
						<p>
							<?php echo lang('agreement_signed')?> <?php echo $term_agreement['date'] ?>
						</p>
					<?php } ?>
				</div>
			<?php } ?>


			<!-- Modal -->
			<div class="modal  fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog terms_modal">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel"><?php echo $term['name'] ?></h5>
							<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						</div>
						<div class="modal-body">
							<div class="terms-and-conditions">
								<?php echo $term['text_document'] ?>
							</div>
						</div>
						<div class="modal-footer">
							<?php if (isset($user)) { ?>

								<div class="text-end">
									<?php if (!isset($term_agreement) || $term_agreement['status'] != 1) { ?>
										<div class="term_buttons_popup">
											<button type="button" disabled class="btn btn-primary ms-3" id="term_button_popup_agree"
													data-status="1"><?php echo lang('agree')?>
											</button>
										</div>
									<?php } else { ?>
										<p>
											<?php echo lang('agreement_signed')?> <?php echo $term_agreement['date'] ?>
										</p>
									<?php } ?>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>


		</div>
	</div>


</div>


<?php include VIEWPATH . $template_path . 'common/footer.php'; ?>
