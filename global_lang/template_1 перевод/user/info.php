<?php include VIEWPATH . $template_path . 'common/header.php'; ?>

<div id="kt_content_container" class="container-xxl">

	<?php if (isset($user['tg_nic']) && ($user['tg_nic'] == '@konar777' || $user['tg_nic'] == '@crm_master')) { ?>

		<?php //$this->my_functions->vardump($user); ?>

	<?php } ?>


	<div class="row align-items-stretch">
		<div class="col-12 col-lg-6 mb-5">
			<div class="card h-100">
				<div class="card-body">

					<div class="d-flex flex-wrap flex-sm-nowrap">
						<!--begin: Pic-->
						<div class="me-7">

							<div class="symbol symbol-100px symbol-lg-160px symbol-fixed position-relative">

								<?php if ($user['avatar_url']) { ?>
									<img src="<?php echo $user['avatar_url'] ?>"
										 alt="<?php if (isset($user['tg_nic'])) {
											 echo $user['tg_nic'];
										 } ?>">
									<div class="position-absolute translate-middle bottom-0 start-100 mb-6 bg-success rounded-circle border border-4 border-white h-20px w-20px"></div>
								<?php } else { ?>

									<div class="symbol-label fs-2 fw-bold text-success"></div>

								<?php } ?>
							</div>

						</div>
						<!--end::Pic-->
						<!--begin::Info-->
						<div class="flex-grow-1">
							<!--begin::Title-->
							<div class="d-flex justify-content-between align-items-start flex-wrap">
								<!--begin::User-->
								<div class="d-flex flex-column">
									<?php if (isset($user['full_name']) && $user['full_name'] != '') { ?>
										<!--begin::Name-->
										<div class="d-flex align-items-center mb-2">
											<div class="text-gray-900 text-hover-primary fs-2 fw-bolder me-1"><?php echo $user['full_name'] ?></div>
										</div>
										<!--end::Name-->
									<?php } ?>
									<!--begin::Info-->
									<div class="d-flex flex-wrap fw-bold fs-6 mb-4 pe-2">
										<div class="d-flex align-items-center text-gray-400 text-hover-primary me-7 mb-2">
											id <?php echo $user['tg_id']; ?>
										</div>
                                        <?php  if (!isset($domain) || $domain == NULL) { ?>
                                        <div class="d-flex align-items-center text-gray-400 text-hover-primary mb-2">

											<?php
											// Проверяем доступы к продуктовым тирам
											if (isset($user['portfolios']) && !empty($user['portfolios'])) {
												$product_tears_array = [];

												foreach ($user['portfolios'] as $portfolio) {

													$key = array_search($portfolio['portfolio_id'], array_column($tirs_tree, 'portfolio_id', 'id'));

													if (!is_bool($key)) {
														$product_tears_array[$key] = $tirs_tree[$key]['name'];
													}

												}

											}
											?>

											<?php if (isset($user['active_tir']) && $user['active_tir'] > 0) { ?>
												<?php

												if (isset($tirs_tree[$user['active_tir']]['name'])) {
													echo 'Тир ' . $tirs_tree[$user['active_tir']]['name'];
												}

												if (isset($product_tears_array) && !empty($product_tears_array)) {
													echo ' + ' . count($product_tears_array);
												}

												?>

											<?php } else { ?>
												<?php echo lang('tir_none')?>
											<?php } ?>
										</div>
                                        <?php } ?>
									</div>
									<!--end::Info-->
								</div>
								<!--end::User-->

							</div>
							<!--end::Title-->
							<!--begin::Stats-->
							<div class="d-flex flex-wrap flex-stack">
								<!--begin::Wrapper-->
								<div class="d-flex flex-column flex-grow-1 pe-8">
									<!--begin::Stats-->
									<div class="d-flex flex-wrap">
										<!--begin::Stat-->

                                        <?php  if (!isset($domain) || $domain == NULL) { ?>
                                            <div class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3">
                                                <!--begin::Number-->
                                                <div class="d-flex align-items-center">
                                                    <div class="fs-2 fw-bolder" data-kt-countup="true"
                                                         data-kt-countup-value="<?php echo (isset($user['sum']) && $user['sum'] > 0) ? round($user['sum']) : 0 ?>"
                                                         data-kt-countup-suffix=" KON" data-kt-countup-separator=''>
                                                        0
                                                    </div>
                                                </div>
                                                <!--end::Number-->
                                                <!--begin::Label-->
                                                <div class="fw-bold fs-6 text-gray-400"><?php echo lang('balance')?></div>
                                                <!--end::Label-->
                                            </div>

                                        <?php }  ?>
										<!--end::Stat-->
										<!--begin::Stat-->
										<a href="/project/my"
										   class="border border-gray-300 border-dashed rounded min-w-125px py-3 px-4 me-6 mb-3">
											<!--begin::Number-->
											<div class="d-flex align-items-center">
												<div class="fs-2 fw-bolder" data-kt-countup="true"
													 data-kt-countup-value="<?php echo (isset($orders_data['sum']) && $orders_data['sum'] > 0) ? round($orders_data['sum']) : 0 ?>"
													 data-kt-countup-suffix=" $" data-kt-countup-separator=''>0
												</div>
											</div>
											<!--end::Number-->
											<!--begin::Label-->
											<div class="fw-bold fs-6 text-gray-400"><?php echo lang('invested')?></div>
											<!--end::Label-->
										</a>
										<!--end::Stat-->

									</div>
									<!--end::Stats-->
								</div>
								<!--end::Wrapper-->

							</div>
							<!--end::Stats-->
						</div>
						<!--end::Info-->
					</div>


				</div>
			</div>
		</div>

		<div class="col-12 col-lg-6 mb-5">
			<div class="card h-100">
				<div class="card-body">
					<h2 class="mb-3"><?php echo lang('kyc_identification')?></h2>

					<?php if (!isset($user['kyc_id']) || !$user['kyc_id']) { ?>

						<!-- Не пройдена KYC -->

						<div class="alert alert-warning d-flex align-items-center p-5 mb-0">
							<!--begin::Svg Icon | path: icons/duotune/general/gen048.svg-->
							<span class="svg-icon svg-icon-2hx svg-icon-warning me-4">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
									 fill="none">
									<path opacity="0.3"
										  d="M20.5543 4.37824L12.1798 2.02473C12.0626 1.99176 11.9376 1.99176 11.8203 2.02473L3.44572 4.37824C3.18118 4.45258 3 4.6807 3 4.93945V13.569C3 14.6914 3.48509 15.8404 4.4417 16.984C5.17231 17.8575 6.18314 18.7345 7.446 19.5909C9.56752 21.0295 11.6566 21.912 11.7445 21.9488C11.8258 21.9829 11.9129 22 12.0001 22C12.0872 22 12.1744 21.983 12.2557 21.9488C12.3435 21.912 14.4326 21.0295 16.5541 19.5909C17.8169 18.7345 18.8277 17.8575 19.5584 16.984C20.515 15.8404 21 14.6914 21 13.569V4.93945C21 4.6807 20.8189 4.45258 20.5543 4.37824Z"
										  fill="black"></path>
									<path d="M10.5606 11.3042L9.57283 10.3018C9.28174 10.0065 8.80522 10.0065 8.51412 10.3018C8.22897 10.5912 8.22897 11.0559 8.51412 11.3452L10.4182 13.2773C10.8099 13.6747 11.451 13.6747 11.8427 13.2773L15.4859 9.58051C15.771 9.29117 15.771 8.82648 15.4859 8.53714C15.1948 8.24176 14.7183 8.24176 14.4272 8.53714L11.7002 11.3042C11.3869 11.6221 10.874 11.6221 10.5606 11.3042Z"
										  fill="black"></path>
									</svg>
								</span>
							<!--end::Svg Icon-->
							<div class="d-flex flex-column">
								<h4 class="mb-2 text-warning"><?php echo lang('auth_none')?></h4>
								<span class="mb-2"><?php echo lang('kyc_profile_auth')?></span>
								<a class="btn btn-warning" href="<?php echo $kyc_url; ?>"><?php echo lang('verification_action')?></a>

								<span class="text-center mt-4 text-decoration-underline cursor-pointer"
									  data-bs-toggle="tooltip" data-bs-custom-class="tooltip-dark"
									  data-bs-placement="top"
									  title="<?php echo lang('auth_fractal_comment')?>">
											<?php echo lang('kyc_question')?>
										</span>
							</div>
						</div>


					<?php } elseif (isset($user['approved']) || !$user['approved']) { ?>
						<?php if ($user['approved'] == 0) { ?>

							<!-- На модерации -->

							<div class="alert alert-primary d-flex align-items-center p-5 mb-0">
								<!--begin::Svg Icon | path: icons/duotune/general/gen048.svg-->
								<span class="svg-icon svg-icon-2hx svg-icon-primary me-4">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
									 fill="none">
									<path opacity="0.3"
										  d="M20.5543 4.37824L12.1798 2.02473C12.0626 1.99176 11.9376 1.99176 11.8203 2.02473L3.44572 4.37824C3.18118 4.45258 3 4.6807 3 4.93945V13.569C3 14.6914 3.48509 15.8404 4.4417 16.984C5.17231 17.8575 6.18314 18.7345 7.446 19.5909C9.56752 21.0295 11.6566 21.912 11.7445 21.9488C11.8258 21.9829 11.9129 22 12.0001 22C12.0872 22 12.1744 21.983 12.2557 21.9488C12.3435 21.912 14.4326 21.0295 16.5541 19.5909C17.8169 18.7345 18.8277 17.8575 19.5584 16.984C20.515 15.8404 21 14.6914 21 13.569V4.93945C21 4.6807 20.8189 4.45258 20.5543 4.37824Z"
										  fill="black"></path>
									<path d="M10.5606 11.3042L9.57283 10.3018C9.28174 10.0065 8.80522 10.0065 8.51412 10.3018C8.22897 10.5912 8.22897 11.0559 8.51412 11.3452L10.4182 13.2773C10.8099 13.6747 11.451 13.6747 11.8427 13.2773L15.4859 9.58051C15.771 9.29117 15.771 8.82648 15.4859 8.53714C15.1948 8.24176 14.7183 8.24176 14.4272 8.53714L11.7002 11.3042C11.3869 11.6221 10.874 11.6221 10.5606 11.3042Z"
										  fill="black"></path>
									</svg>
								</span>
								<!--end::Svg Icon-->
								<div class="d-flex flex-column">
									<h4 class="mb-2 text-primary"><?php echo lang('data_confirmation_pending')?></h4>
									<span class="mb-2"><?php echo lang('fractal_confirmation_comment')?></span>
									<a class="btn btn-primary"
									   href="<?php echo 'https://' . $frontend_domain; ?>"
									   target="_blank"><?php echo lang('fractal_go')?></a>
								</div>
							</div>


						<?php } elseif ($user['approved'] == 1) { ?>
							<!-- ВЕрифицирован -->

							<div class="alert alert-success d-flex align-items-center p-5 mb-0">
								<!--begin::Svg Icon | path: icons/duotune/general/gen048.svg-->
								<span class="svg-icon svg-icon-2hx svg-icon-success me-4">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
									 fill="none">
									<path opacity="0.3"
										  d="M20.5543 4.37824L12.1798 2.02473C12.0626 1.99176 11.9376 1.99176 11.8203 2.02473L3.44572 4.37824C3.18118 4.45258 3 4.6807 3 4.93945V13.569C3 14.6914 3.48509 15.8404 4.4417 16.984C5.17231 17.8575 6.18314 18.7345 7.446 19.5909C9.56752 21.0295 11.6566 21.912 11.7445 21.9488C11.8258 21.9829 11.9129 22 12.0001 22C12.0872 22 12.1744 21.983 12.2557 21.9488C12.3435 21.912 14.4326 21.0295 16.5541 19.5909C17.8169 18.7345 18.8277 17.8575 19.5584 16.984C20.515 15.8404 21 14.6914 21 13.569V4.93945C21 4.6807 20.8189 4.45258 20.5543 4.37824Z"
										  fill="black"></path>
									<path d="M10.5606 11.3042L9.57283 10.3018C9.28174 10.0065 8.80522 10.0065 8.51412 10.3018C8.22897 10.5912 8.22897 11.0559 8.51412 11.3452L10.4182 13.2773C10.8099 13.6747 11.451 13.6747 11.8427 13.2773L15.4859 9.58051C15.771 9.29117 15.771 8.82648 15.4859 8.53714C15.1948 8.24176 14.7183 8.24176 14.4272 8.53714L11.7002 11.3042C11.3869 11.6221 10.874 11.6221 10.5606 11.3042Z"
										  fill="black"></path>
									</svg>
								</span>
								<!--end::Svg Icon-->
								<div class="d-flex flex-column">
									<h4 class="mb-2 text-success"><?php echo lang('id_success')?></h4>
									<span class="mb-2"><?php echo lang('fractal_data_success')?></span>
									<span>Fractal ID</span>
									<span class="mb-2"><?php echo $user['fractal_id'] ?></span>

								</div>
							</div>
						<?php } elseif ($user['approved'] == 2) { ?>

							<!-- ВЕрификация отклонена -->

							<div class="alert alert-danger d-flex align-items-center p-5 mb-0">
								<!--begin::Svg Icon | path: icons/duotune/general/gen048.svg-->
								<span class="svg-icon svg-icon-2hx svg-icon-danger me-4">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
									 fill="none">
									<path opacity="0.3"
										  d="M20.5543 4.37824L12.1798 2.02473C12.0626 1.99176 11.9376 1.99176 11.8203 2.02473L3.44572 4.37824C3.18118 4.45258 3 4.6807 3 4.93945V13.569C3 14.6914 3.48509 15.8404 4.4417 16.984C5.17231 17.8575 6.18314 18.7345 7.446 19.5909C9.56752 21.0295 11.6566 21.912 11.7445 21.9488C11.8258 21.9829 11.9129 22 12.0001 22C12.0872 22 12.1744 21.983 12.2557 21.9488C12.3435 21.912 14.4326 21.0295 16.5541 19.5909C17.8169 18.7345 18.8277 17.8575 19.5584 16.984C20.515 15.8404 21 14.6914 21 13.569V4.93945C21 4.6807 20.8189 4.45258 20.5543 4.37824Z"
										  fill="black"></path>
									<path d="M10.5606 11.3042L9.57283 10.3018C9.28174 10.0065 8.80522 10.0065 8.51412 10.3018C8.22897 10.5912 8.22897 11.0559 8.51412 11.3452L10.4182 13.2773C10.8099 13.6747 11.451 13.6747 11.8427 13.2773L15.4859 9.58051C15.771 9.29117 15.771 8.82648 15.4859 8.53714C15.1948 8.24176 14.7183 8.24176 14.4272 8.53714L11.7002 11.3042C11.3869 11.6221 10.874 11.6221 10.5606 11.3042Z"
										  fill="black"></path>
									</svg>
								</span>
								<!--end::Svg Icon-->
								<div class="d-flex flex-column">
									<h4 class="mb-2 text-danger"><?php echo lang('auth_denied')?></h4>
									<span class="mb-2"><?php echo lang('auth_fractal_denied')?></span>
									<div class="row">
										<div class="col-12 col-xl-6 mb-5 mb-xl-0">
											<a class="btn btn-danger w-100"
											   href="<?php echo 'https://' . $frontend_domain; ?>"
											   target="_blank"><?php echo lang('fractal_go')?></a>
										</div>
										<div class="col-12 col-xl-6">
											<a class="btn btn-light-danger  w-100" data-bs-toggle="modal"
											   data-bs-target="#modal_delete_kyc"><?php echo lang('profile_disable')?></a>
										</div>
									</div>
								</div>
							</div>


						<?php } ?>
					<?php } ?>

				</div>
			</div>
		</div>
		<div class="col-12 mb-5">
			<div class="card">
				<div class="card-header">
					<div class="card-title">
						<h2><?php echo lang('profile')?></h2>
					</div>
				</div>
				<div class="card-body">
					<?php /*$this->my_functions->vardump($user); */ ?>
					<div class="row mb-7">
						<!--begin::Label-->
						<label class="col-lg-4 fw-bold text-muted"><?php echo lang('wallet')?></label>
						<!--end::Label-->
						<!--begin::Col-->
						<div class="col-lg-8">

							<?php if (!isset($user['wallet_address']) || trim($user['wallet_address']) == '') { ?>
								<a href="/user/wallet" class="btn btn-warning"><?php echo lang('wallet_add')?></a>

							<?php } else { ?>

								<?php if ($user['status'] == 0) { ?>
									<a href="/user/wallet" class="btn btn-warning"><?php echo lang('wallet_confirm')?></a>
								<?php } ?>
								<span class="fw-bolder fs-6 text-gray-800"><?php echo $user['wallet_address'] ?></span>

								<button type="button" class="btn btn-secondary btn-sm ms-5" data-bs-toggle="modal"
										data-bs-target="#modal_change_wallet"><?php echo lang('change')?>
								</button>
							<?php } ?>
						</div>
						<!--end::Col-->
					</div>


					<?php if (isset($user['tg_id']) && $user['tg_id'] < 100000000000) { ?>
						<div class="row mb-7">
							<!--begin::Label-->
							<label class="col-lg-4 fw-bold text-muted">Telegram</label>
							<!--end::Label-->
							<!--begin::Col-->
							<div class="col-lg-8">
								<span class="fw-bolder fs-6 text-gray-800"><?php echo $user['tg_nic'] . ' (' . $user['tg_id'] . ')' ?><?php if ($user['salebot_id']) echo '<span class="badge badge-success ms-3">' $lang['bot_connected'] '</span>' ?></span>
							</div>
							<!--end::Col-->
						</div>
					<?php } ?>

					<?php if (isset($user['email']) && $user['email']) { ?>
						<div class="row mb-7">
							<!--begin::Label-->
							<label class="col-lg-4 fw-bold text-muted">E-mail</label>
							<!--end::Label-->
							<!--begin::Col-->
							<div class="col-lg-8"><?php echo $user['email'] ?>
								<button type="button" class="btn btn-danger ms-3" data-bs-toggle="modal"
										data-bs-target="#modal_delete_email"><?php echo lang('mail_disconnect')?>
								</button>
							</div>
							<!--end::Col-->
						</div>
					<?php } ?>


					<div class="row mb-7">
						<!--begin::Label-->
						<label class="col-lg-4 fw-bold text-muted"><?php echo lang('registration')?></label>
						<!--end::Label-->
						<!--begin::Col-->
						<div class="col-lg-8">
							<span class="fw-bolder fs-6 text-gray-800"><?php echo $user['create_date'] ?></span>
						</div>
						<!--end::Col-->
					</div>
					<div class="row mb-7">
						<!--begin::Label-->
						<label class="col-lg-4 fw-bold text-muted"><?php echo lang('update')?></label>
						<!--end::Label-->
						<!--begin::Col-->
						<div class="col-lg-8">
							<span class="fw-bolder fs-6 text-gray-800"><?php echo $user['update_date'] ?></span>
						</div>
						<!--end::Col-->
					</div>

                    <?php  if (!isset($domain) || $domain == NULL) { ?>

                    <div class="row mb-7">
						<!--begin::Label-->
						<label class="col-lg-4 fw-bold text-muted"><?php echo lang('defi_end')?></label>
						<!--end::Label-->
						<!--begin::Col-->
						<div class="col-lg-8">
							<?php if (isset($user['defi']['date_to'])) { ?>
								<?php if (strtotime($user['defi']['date_to']) > time()) { ?>
									<span class="fw-bolder fs-6 text-gray-800"><?php echo $user['defi']['date_to'] ?></span>
								<?php } else { ?>
									<span class="fw-bolder fs-6 text-danger"><?php echo $user['defi']['date_to'] ?></span>
									<a href="https://t.me/KondrashovSupportbot"
									   class="btn btn-primary btn-sm ms-10"><?php echo lang('support_action')?></a>
								<?php } ?>
							<?php } else { ?>
								<a href="https://t.me/KondrashovSupportbot" class="btn btn-primary btn-sm"><?php echo lang('defi_start')?></a>
							<?php } ?>
						</div>
						<!--end::Col-->
					</div>
                    <?php } ?>

				</div>
			</div>
		</div>


		<div class="col-12 mb-5 d-none">
			<div class="card">
				<div class="card-header">
					<div class="card-title"><h2><?php echo lang('agreements')?></h2></div>
				</div>

				<div class="card-body">

					<?php foreach ($terms as $term) { ?>

						<div class="row mb-7">
							<a href="/user/terms/<?php echo $term['id'] ?>"
							   class="col-lg-4 fw-bold text-muted"><?php echo $term['name'] ?></a>
							<!--begin::Col-->
							<div class="col-lg-8">
								<?php

								if (isset($user_terms) && !empty($user_terms)) {
									$key = array_search($term['id'], array_column($user_terms, 'term_id'));

									if (is_bool($key) || $user_terms[$key]['status'] == 2) { ?>

										<a href="/user/terms/<?php echo $term['id'] ?>"
										   class="btn btn-primary btn-sm"><?php echo lang('sign')?></a>

									<?php } else { ?>
										<?php echo lang('wallet_none_content')?> <?php echo $user_terms[$key]['date'] ?>
									<?php }

								} else { ?>
									<a href="/user/terms/<?php echo $term['id'] ?>"
									   class="btn btn-primary btn-sm"><?php echo lang('sign')?></a>
								<?php }

								?>
							</div>
							<!--end::Col-->
						</div>
					<?php } ?>

				</div>
			</div>
		</div>


	</div>


	<?php if (isset($added_info_initial) && !empty($added_info_initial)) { ?>

		<div class="row mb-5 mb-xl-10">
			<div class="col px-10">
				<h2><?php echo lang('wallets_additional')?></h2>
				<?php echo lang('wallet_vesting_comment')?>
			</div>
		</div>

		<?php $repeater_ids = array(); // Массив ID для иницализации модуля в футере ?>
		<?php foreach ($added_info_initial as $item) { ?>
			<?php $repeater_ids[$item['id']] = 'added_info_repeater_' . $item['id']; ?>
			<div class="card mb-4">
				<div class="card-header">
					<div class="card-title">
						<h2><?php echo $item['name'] ?></h2>
					</div>
				</div>
				<div class="card-body">
					<!--begin::Repeater-->
					<div class="offset-xl-2">


						<div class="alert alert-primary d-flex align-items-center p-5 mb-3">
							<!--begin::Svg Icon | path: icons/duotune/general/gen048.svg-->
							<span class="svg-icon svg-icon-2hx svg-icon-primary me-4">
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
											 viewBox="0 0 24 24"
											 fill="none">
											<path opacity="0.3"
												  d="M20.5543 4.37824L12.1798 2.02473C12.0626 1.99176 11.9376 1.99176 11.8203 2.02473L3.44572 4.37824C3.18118 4.45258 3 4.6807 3 4.93945V13.569C3 14.6914 3.48509 15.8404 4.4417 16.984C5.17231 17.8575 6.18314 18.7345 7.446 19.5909C9.56752 21.0295 11.6566 21.912 11.7445 21.9488C11.8258 21.9829 11.9129 22 12.0001 22C12.0872 22 12.1744 21.983 12.2557 21.9488C12.3435 21.912 14.4326 21.0295 16.5541 19.5909C17.8169 18.7345 18.8277 17.8575 19.5584 16.984C20.515 15.8404 21 14.6914 21 13.569V4.93945C21 4.6807 20.8189 4.45258 20.5543 4.37824Z"
												  fill="black"></path>
											<path d="M10.5606 11.3042L9.57283 10.3018C9.28174 10.0065 8.80522 10.0065 8.51412 10.3018C8.22897 10.5912 8.22897 11.0559 8.51412 11.3452L10.4182 13.2773C10.8099 13.6747 11.451 13.6747 11.8427 13.2773L15.4859 9.58051C15.771 9.29117 15.771 8.82648 15.4859 8.53714C15.1948 8.24176 14.7183 8.24176 14.4272 8.53714L11.7002 11.3042C11.3869 11.6221 10.874 11.6221 10.5606 11.3042Z"
												  fill="black"></path>
											</svg>
										</span>
							<!--end::Svg Icon-->
							<div class="d-flex flex-column">
								<span class="mb-2"><?php echo $item['description'] ?></span>
							</div>
						</div>


						<!--begin::Form group-->
						<div class="form-group additional_info" id="<?php echo $repeater_ids[$item['id']]; ?>">
							<?php if (isset($user['added_info'][$item['id']]) && !empty($user['added_info'][$item['id']])) { ?>
								<?php foreach ($user['added_info'][$item['id']] as $value) { ?>
									<div class="saved_wallet">
										<div class="form-group row">
											<div class="col-md-8">
												<input type="text" class="form-control mb-3"
													   placeholder="Enter value"
													   name="added_info[<?php echo $item['id'] ?>][]"
													   value="<?php echo $value; ?>"/>
											</div>
											<div class="col-md-4">
												<button
														class="btn btn-sm btn-light-danger mt-1 delete_added_wallet">
													<i class="la la-trash-o"></i><?php echo lang('remove')?>
												</button>
											</div>
										</div>
									</div>
								<?php } ?>
							<?php } ?>
						</div>
						<!--end::Form group-->

						<!--begin::Form group-->
						<div class="form-group row mt-5">
							<div class="col-md-8">
								<button class="btn btn-light-primary addAddedInfo"
										data-parent="<?php echo $item['id']; ?>">
									<i class="la la-plus"></i><?php echo lang('add')?>
								</button>
							</div>
							<div class="col-md-4">
								<button class="btn btn-success saveAddedInfo"
										data-id_added_info="<?php echo $item['id'] ?>"><?php echo lang('save')?>
								</button>
							</div>
						</div>
						<!--end::Form group-->
					</div>
					<!--end::Repeater-->
				</div>
			</div>
		<?php } ?>
	<?php } ?>


</div>

<div class="modal fade" tabindex="-1" id="modal_change_wallet">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><?php echo lang('wallet_change')?></h5>

				<!--begin::Close-->
				<div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
					 aria-label="Close">
					<span class="svg-icon svg-icon-2x"></span>
				</div>
				<!--end::Close-->
			</div>

			<div class="modal-body">
				<p><?php echo lang('wallet_reset')?></p>
				<p><?php echo lang('wallet_reset_comment')?></p>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-light" data-bs-dismiss="modal"><?php echo lang('cancel')?></button>
				<button type="button" class="btn btn-danger" id="removeWalletAddress"><?php echo lang('wallet_change_item')?></button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" id="modal_delete_kyc">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><?php echo lang('wallet_kyc_disable')?></h5>

				<!--begin::Close-->
				<div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
					 aria-label="Close">
					<span class="svg-icon svg-icon-2x"></span>
				</div>
				<!--end::Close-->
			</div>

			<div class="modal-body">
				<p><?php echo lang('wallet_fractal_disable')?></p>
				<p><?php echo lang('wallet_reset_comment_sm')?></p>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-light" data-bs-dismiss="modal"><?php echo lang('cancel')?></button>
				<a href="/user/deletekyc" class="btn btn-danger"><?php echo lang('profile_disable')?></a>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" tabindex="-1" id="modal_delete_email">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><?php echo lang('email_disconnect')?></h5>

				<!--begin::Close-->
				<div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
					 aria-label="Close">
					<span class="svg-icon svg-icon-2x"></span>
				</div>
				<!--end::Close-->
			</div>

			<div class="modal-body">
				<p><?php echo lang('email_disconnect_confirmation')?></p>
				<p><?php echo lang('account_remove_comment')?></p>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-light" data-bs-dismiss="modal"><?php echo lang('cancel')?></button>
				<a href="/user/deleteemail" class="btn btn-danger"><?php echo lang('email_disconnect_header')?></a>
			</div>
		</div>
	</div>
</div>


<?php include VIEWPATH . $template_path . 'common/footer.php'; ?>
