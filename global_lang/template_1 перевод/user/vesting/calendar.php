<?php include VIEWPATH . $template_path . 'common/header.php'; ?>

<div id="kt_content_container" class="container-xxl">

	<div class="card">
		<div class="card-header">
		<div class="card-title">
			<h1><?php echo lang('payments_schedule')?></h1>
		</div>
		</div>
		<div class="card-body">

			<div id="kt_docs_fullcalendar_basic"></div>


		</div>
	</div>

</div>


<?php include VIEWPATH . $template_path . 'common/footer.php'; ?>
