<?php if (!isset($table)) { ?>
<?php //$this->my_functions->vardump($filter_vesting);?>
<div class="table-responsive">
	<table class="table table-row-dashed align-middle gy-4 gs-2 gs-md-9">
		<!--begin::Thead-->
		<thead class="border-gray-200 fs-md-5 fw-bold bg-lighten fw-bolder">
		<tr class="thead">
			<th>Date</th>
			<th><?php echo lang('project_round')?></th>
			<th class="d-none d-md-table-cell text-center"><?php echo lang('vesting_progress')?></th>
			<th class="text-center"><?php echo lang('unlock')?> %</th>
			<th class="text-end"><?php echo lang('coins_my')?></th>
		</tr>
		</thead>
		<tbody>
		<?php } ?>


		<?php foreach ($array_with_dates as $item) { ?>
			<?php if ($item['type'] == 'vesting') { ?>
				<?php $vesting = $vestings[$item['key']]; ?>

				<tr class="<?php echo implode(' ', $vesting['class_names'])?>">
					<td class="bg-white rounded-bottom rounded-start mb-3">
						<div class="d-flex justify-content-start flex-column">
							<span class="text-gray-800 fw-bolder fs-md-2x mb-0"><?php echo date('d M', strtotime($vesting['date'])) ?></span>
							<span class="text-gray-800 mt-md-n2 fs-md-2"><?php echo date('H:i', strtotime($vesting['date'])) ?></span>
							<div>
								<?php

								// Устанавливаем текущую дату
								$time = time();
								//$time = strtotime('2022-05-30');

								if (strtotime($vesting['date']) < $time) {
									?>
									<span class="badge badge-success"><?php echo lang('completed')?></span>
								<?php } else {
									$seconds = (strtotime($vesting['date']) - $time);

									if ($seconds < 86400) {

										echo 'In ' . floor($seconds / 3600) . ' hours';

									} else {

										echo 'In ' . floor($seconds / 86400) . ' days';

									}

								}

								?>
							</div>
						</div>
					</td>
					<td class="bg-white">
						<div class="d-flex align-items-center">
							<div class="symbol symbol-45px me-5  d-none d-md-block">
								<img class="w-50px h-50px shadow"
									 src="<?php echo 'https://invest-results.ru/' . $vesting['logo'] ?>" alt="">
							</div>
							<div class="d-flex justify-content-start flex-column">
								<a href="/project/show/<?php echo $vesting['project_id'] ?>"
								   class="text-gray-800 fw-bolder text-hover-info fs-6"><?php echo $vesting['project_name'] ?></a>
								<?php if ($vesting['description_small'] != '') { ?>
									<span class="text-muted fw-bold d-block fs-7"><?php echo $vesting['description_small'] ?></span>
								<?php } ?>
								<span class="text-muted "><?php echo lang('seed_round')?> (<?php echo round($vesting['price'], 3) . ' $' ?>)</span>
							</div>
						</div>
					</td>
					<td class="bg-white d-none d-md-table-cell">

						<?php

						$payed = $vesting['tge_percent'] + $vesting['vesting_id'] * $vesting['percent'] ; // Переменная для подсчета сколько выплачено %


						?>

						<div class="d-flex flex-column text-muted mw-300px m-auto">
							<div class="d-flex justify-content-between">
								<span>TGE <?php echo date('j M Y', strtotime($vesting['tge_fact'])); ?></span>
								<span><?php echo isset($last_vestings[$vesting['project_id']][$vesting['round_id']]) ? date('j M Y', strtotime($last_vestings[$vesting['project_id']][$vesting['round_id']])) : '' ?></span>
							</div>
							<div class="progress h-7px bg-success bg-opacity-50 my-2">
								<div class="progress-bar" role="progressbar"
									 style="width: <?php echo $payed; ?>%; background: linear-gradient(90deg, #4E98BF 0%, #5EBD9A 100%);"
									 aria-valuenow="<?php echo $payed; ?>" aria-valuemin="0"
									 aria-valuemax="100"></div>

							</div>
							<div class="text-center"><?php echo lang('vesting_progress_tge')?> <?php echo floor($payed); ?>%</div>

						</div>

					</td>
					<td class="bg-white text-center text-gray-800 fw-bolder fs-2">
						<?php echo number_format($vesting['percent'], 3, ',', '') ?>%
					</td>
					<td class="bg-white text-end  text-gray-800 fw-bolder fs-2 rounded-top rounded-end">
						<?php echo $vesting['pay_sum'] ? floor($vesting['pay_sum']) . ' ' . $vesting['ticker'] : '' ?>
					</td>


				</tr>

			<?php } elseif ($item['type'] == 'tge_plan' || $item['type'] == 'tge_fact') { ?>

				<?php
				// Присваиваем
				$round = $item['type'] == 'tge_plan' ? $tges_plan[$item['key']] : $tges_fact[$item['key']];
				$round['tge_date'] = $item['type'] == 'tge_plan' ? $round['tge_planed'] : $round['tge_fact'];
				?>
				<tr class="<?php echo implode(' ', $round['class_names'])?>">

					<td class="bg-white rounded-bottom rounded-start mb-3">
						<div class="d-flex justify-content-start flex-column">
							<span class="text-gray-800 fw-bolder fs-md-2x mb-0"><?php echo date('d M', strtotime($round['tge_date'])) ?></span>
							<span class="text-gray-800 mt-md-n2 fs-md-2"><?php echo date('H:i', strtotime($round['tge_date'])) ?></span>
						</div>
						<div>
							<?php

							// Устанавливаем текущую дату
							$time = time();

							if (strtotime($round['tge_date']) < $time) {
								?>
								<span class="badge badge-success"><?php echo lang('completed')?></span>
							<?php } else {
								$seconds = (strtotime($round['tge_date']) - $time);

								if ($seconds < 86400) {

									echo 'In ' . floor($seconds / 3600) . ' hours';

								} else {

									echo 'In ' . floor($seconds / 86400) . ' days';

								}

							}

							?>
						</div>
					</td>


					<td class="bg-white">
						<div class="d-flex align-items-center">
							<div class="symbol symbol-45px me-5  d-none d-md-block">
								<img class="w-50px h-50px shadow"
									 src="<?php echo 'https://invest-results.ru/' . $round['logo'] ?>" alt="">
							</div>
							<div class="d-flex justify-content-start flex-column">
								<a href="/project/show/<?php echo $round['id'] ?>"
								   class="text-gray-800 fw-bolder text-hover-info fs-6"><?php echo $round['name'] ?></a>
								<?php if ($round['description_small'] != '') { ?>
									<span class="text-muted fw-bold d-block fs-7"><?php echo $round['description_small'] ?></span>
								<?php } ?>
								<span class="text-muted "><?php echo lang('seed_round')?> (<?php echo round($round['price'], 3) . ' $' ?>)</span>
							</div>
						</div>
					</td>
					<td class="bg-white d-none d-md-table-cell">

						<div class="d-flex flex-column text-muted mw-300px m-auto">
							<div class="d-flex justify-content-between">
								<span>TGE <?php echo date('j M Y', strtotime($round['tge_fact'])); ?></span>
								<span><?php echo isset($last_vestings[$round['project_id']][$round['round_id']]) ? date('j M Y', strtotime($last_vestings[$round['project_id']][$round['round_id']])) : '' ?></span>
							</div>
							<div class="progress h-7px bg-success bg-opacity-50 my-2">
								<div class="progress-bar" role="progressbar"
									 style="width: <?php echo $round['tge_percent']; ?>%; background: linear-gradient(90deg, #4E98BF 0%, #5EBD9A 100%);"
									 aria-valuenow="<?php echo $round['tge_percent']; ?>" aria-valuemin="0"
									 aria-valuemax="100"></div>

							</div>
							<div class="text-center"><?php echo lang('vesting_progress_tge')?> <?php echo floor($round['tge_percent']); ?>%
							</div>

						</div>

					</td>
					<td class="bg-white text-center text-gray-800 fw-bolder fs-2">
						<span><?php echo number_format($round['tge_percent'], 3, ',', '') ?>%</span><br/>
						<span class="badge badge-info">TGE</span>
					</td>
					<td class="bg-white text-end  text-gray-800 fw-bolder fs-2 rounded-top rounded-end">
						<?php echo $round['pay_sum'] ? floor($round['pay_sum']) . ' ' . $round['ticker'] : '' ?>
					</td>
				</tr>

			<?php } ?>

		<?php } ?>

		<?php if (!isset($table)) { ?>
		</tbody>
	</table>
</div>
<?php } ?>

<!--<div class="d-flex justify-content-center">
	<div class="btn btn-info load_more_rounds" data-time_start="">Load more</div>
</div>
-->
