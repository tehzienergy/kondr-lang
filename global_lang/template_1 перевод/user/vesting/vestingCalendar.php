<?php include VIEWPATH . $template_path . 'common/header.php'; ?>

<div id="kt_content_container" class="main-container container-xxl hide_every_day d-contentVesting">


	<h1 class="mb-5"><?php echo lang('vesting_calendar')?></h1>

	<div class="row my-5">
		<div class="col">
			<div class="d-md-flex justify-content-between  align-self-center ">
				<div class="d-flex align-self-center">
					<div class="input-group ">
						<button class="btn btn-outline-info border border-info prew_month_rounds btn-sm"><</button>
						<div class="input-group-text text-white bg-info border border-info min-w-125px py-0">
							<span class="w-100 text-center" id="date_start"
								  data-date_start="<?php echo date('Y-m-01', strtotime($filter_vesting['time_start'])); ?>"><?php echo date('M, Y', strtotime($filter_vesting['time_start'])); ?></span>
						</div>
						<button class="btn btn-outline-info btn-sm border border-info next_month_rounds">></i>
						</button>
					</div>
					<div class="d-md-none">
						<button class="btn btn-sm btn-info navbar-toggler" type="button" data-bs-toggle="collapse"
								data-bs-target="#navbarToggleExternalContent"
								aria-controls="navbarToggleExternalContent" aria-expanded="false"
								aria-label="Toggle navigation"><i class="bi bi-gear fs-2x p-0"></i></button>
					</div>
				</div>

				<div class="collapse d-md-flex" id="navbarToggleExternalContent">
					<div class="d-flex mt-5 mt-md-0 flex-row">
						<div class="form-check form-check-custom form-check me-5">
							<input class="form-check-input" type="checkbox"
								   name="filter_my" value="1"
								   id="my_project"/>
							<label class="form-check-label"
								   for="my_project">
								<?php echo lang('project_only_users')?>
							</label>
						</div>
						<div class="form-check form-check-custom form-check">
							<input class="form-check-input" type="checkbox"
								   name="filter_everyday" value="1"
								   id="everyday"/>
							<label class="form-check-label"
								   for="everyday">
								<?php echo lang('vesting_show_everyday')?>
							</label>
						</div>

						<div class="ms-5 toggle_buttons d-flex">

							<button class="btn btn-sm rounded-end-0 active btn-info toogle_vesting_calendar sh_calendar p-0 px-2"
									type="button">
								<i class="bi bi-list fs-2x p-0 border-end-0"></i></button>
							<button class="btn btn-sm rounded-start-0 border-start-0 toogle_vesting_calendar sh_vesting border-1 p-0 px-2"
									type="button"><i
										class="bi bi-grid fs-2x p-0"></i></button>


						</div>

					</div>

				</div>
			</div>
		</div>
	</div>

	<?php if (isset($user['tg_nic']) && $user['tg_nic'] == '@konar777') {
		//$calendar = true;
		//$last_vestings = array_unique($last_vestings, SORT_REGULAR);


	} ?>

	<div class="vestingCalendarBlock">
		<div id="vestingCalendar"></div>
	</div>


	<div id="contentVesting">

		<?php if (isset($array_with_dates) && !empty($array_with_dates)) { ?>
			<?php include VIEWPATH . $template_path . 'user/vesting/vesting_table.php'; ?>
		<?php } else { ?>
			<p class="m5">
				<?php echo lang('no_recent_payments')?>
			</p>
		<?php } ?>
	</div>
	<?php //$this->my_functions->vardump($projects);?>
	<?php //$this->my_functions->vardump($vestings); ?>


</div>

<!-- Modal -->
<div class="modal fade" id="modalVesting" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog terms_modal">
		<div class="modal-content">

			<div class="modal-body">
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				Body
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><?php echo lang('close')?></button>
			</div>
		</div>
	</div>
</div>


<?php include VIEWPATH . $template_path . 'common/footer.php'; ?>
