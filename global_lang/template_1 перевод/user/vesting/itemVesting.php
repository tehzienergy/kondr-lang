<?php if (isset($vesting)) { ?>
	<div class="bg-white d-flex align-items-center gap-10  justify-content-between">

		<div class="bg-white rounded-bottom rounded-start mb-3">
			<div class="d-flex justify-content-start flex-column">
				<span class="text-gray-800 fw-bolder fs-md-2x mb-0"><?php echo date('d M', strtotime($vesting['date'])) ?></span>
				<span class="text-gray-800 mt-md-n2 fs-md-2"><?php echo date('H:i', strtotime($vesting['date'])) ?></span>
				<div>
					<?php

					// Устанавливаем текущую дату
					$time = time();
					//$time = strtotime('2022-05-30');

					if (strtotime($vesting['date']) < $time) {
						?>
						<span class="badge badge-success"><?php echo lang('completed')?></span>
					<?php } else {
						$seconds = (strtotime($vesting['date']) - $time);


						if ($seconds < 86400) {

							echo 'In ' . floor($seconds / 3600) . ' hours';

						} else {

							echo 'In ' . floor($seconds / 86400) . ' days';

						}

					}

					?>
				</div>
			</div>
		</div>

		<div class="bg-white">
			<div class="d-flex align-items-center">
				<div class="symbol symbol-45px me-5  d-none d-md-block">
					<img class="w-50px h-50px shadow"
						 src="<?php echo 'https://invest-results.ru/' . $vesting['logo'] ?>" alt="">
				</div>
				<div class="d-flex justify-content-start flex-column">
					<a href="/project/show/<?php echo $vesting['project_id'] ?>"
					   class="text-gray-800 fw-bolder text-hover-info fs-6"><?php echo $vesting['project_name'] ?></a>
					<?php if ($vesting['description_small'] != '') { ?>
						<span class="text-muted fw-bold d-block fs-7"><?php echo $vesting['description_small'] ?></span>
					<?php } ?>
					<span class="text-muted "><?php echo lang('seed_round')?> (<?php echo round($vesting['price'], 3) . ' $' ?>)</span>
				</div>
			</div>
		</div>


		<div class="bg-white text-center text-gray-800 fw-bolder fs-2">
			<?php echo number_format($vesting['percent'], 3, ',', '') ?>%
		</div>
		<div class="bg-white text-end  text-gray-800 fw-bolder fs-2 rounded-top rounded-end">
			<?php

			$sum = false;
			if (isset($projects) && !empty($projects)) {
				$key = array_search($vesting['project_id'], array_column($projects, 'project_id'));
				if (!is_bool($key) && $vesting['price'] != 0) {

					$arr = array(
							'project_sum' => $projects[$key]['sum'],
							'price' => $vesting['price'],
							'percent' => $vesting['percent']
					);
					//$this->my_functions->vardump($arr);

					$project_sum = $projects[$key]['sum'];
					$sum = $project_sum / $vesting['price'] * $vesting['percent'] / 100;
				}
			}

			?>

			<?php echo $sum ? $sum . ' ' . $vesting['ticker'] : '' ?></div>


	</div>

<?php } ?>

