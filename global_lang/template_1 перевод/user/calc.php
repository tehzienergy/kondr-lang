<?php include VIEWPATH . $template_path . 'common/header.php'; ?>

<div id="kt_content_container" class="container-xxl">

	<h2 class="mb-5"><?php echo lang('staking_calculator')?></h2>

	<div class="row mb-5">
		<div class="col-12">
			<div class="btn-group text-white w-100 w-md-auto toogles" role="group">
				<input type="radio" class="btn-check" name="simple_extended" id="btnradio1" value="simple" autocomplete="off" checked>
				<label class="btn btn-toogles w-50 w-md-auto" for="btnradio1"><?php echo lang('simple')?></label>

				<input type="radio" class="btn-check" name="simple_extended" id="btnradio2" value="extended" autocomplete="off">
				<label class="btn btn-toogles w-50 w-md-auto" for="btnradio2"><?php echo lang('extended')?></label>

			</div>
		</div>
	</div>

	<div class="row calc_block simple">
		<div class="col-md-4 mb-5">
			<div class="bg-white rounded p-8">
				<h4 class="mb-5"><?php echo lang('staking_conditions')?></h4>
				<div class="row mb-5">
					<div class="col-6">
						<label for="" class="form-label p1"><?php echo lang('staking_sum')?></label>
						<input type="text" class="form-control form-control-solid" placeholder="KON" name="sumStacking"
							   value="<?php echo isset($get['sum']) ? $get['sum'] : 10000 ?>"/>
					</div>
					<div class="col-6">
						<label for="" class="form-label p1"><?php echo lang('staking_term')?></label>
						<select class="form-select  form-select-solid" aria-label="<?php echo lang('term')?>" name="srok">
							<option value="1" <?php if (isset($get['srok']) && $get['srok'] == 1) echo 'selected' ?>>1
								<?php echo lang('year')?>
							</option>
							<option value="2" <?php if (isset($get['srok']) && $get['srok'] == 2) echo 'selected' ?>>2
								<?php echo lang('year')?>
							</option>
							<option value="3" <?php if ((isset($get['srok']) && $get['srok'] == 3) || !isset($get['srok'])) echo 'selected' ?>>3
								<?php echo lang('year')?>
							</option>
						</select>
					</div>
				</div>

				<div class="extended">
					<h4 class="mb-5"><?php echo lang('courses_average')?></h4>

					<div class="mb-4">
						<label class="form-label p1">Bitcoin</label>
						<div id="kt_slider_bitcoin"></div>

						<div class="pt-2">
							<div class="p1 color-2 mb-2"><span id="kt_slider_bitcoin_val"></span> USDT</div>
						</div>
					</div>
					<div class="mb-4">
						<label class="form-label p1"><?php echo lang('kon_average')?> </label>
						<div id="kt_slider_sskon"></div>

						<div class="pt-2">
							<div class="p1 color-2 mb-2"><span id="kt_slider_sskon_val"></span> USDT</div>
						</div>
					</div>
					<div class="mb-4">
						<label class="form-label p1"><?php echo lang('kon_price')?></label>
						<div id="kt_slider_spkon"></div>

						<div class="pt-2">
							<div class="p1 color-2 mb-2"><span id="kt_slider_spkon_val"></span> USDT</div>
						</div>
					</div>


					<h4 class="mb-5"><?php echo lang('projects_profit')?></h4>

					<div class="mb-4">

						<div id="kt_slider_sdkon"></div>

						<div class="pt-2">
							<div class="p1 color-2 mb-2">x<span id="kt_slider_sdkon_val"></span></div>
						</div>
					</div>

					<h4 class="mb-5"><?php echo lang('pool_parameters')?></h4>

					<div class="mb-4">
						<label class="form-label p1"><?php echo lang('pool_terms')?></label>
						<div id="kt_slider_pul"></div>

						<div class="pt-2 d-flex justify-content-between">
							<div class="p1 color-2 mb-2">1 <?php echo lang('year')?>: <span id="kt_slider_pul_1"></span>%</div>
							<div class="p1 color-2 mb-2">2 <?php echo lang('year')?>: <span id="kt_slider_pul_2"></span>%</div>
							<div class="p1 color-2 mb-2">3 <?php echo lang('year')?>: <span id="kt_slider_pul_3"></span>%</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		<div class="col-md-8">
			<h3 class="mb-5"><?php echo lang('staking')?></h3>
			<div class="row mb-5">
				<div class="col-md-6 mb-5">
					<div class="bg-darkblue fix_part text-white rounded p-8">
						<div class="d-flex bd-highlight mb-3">
							<div class="align-self-center"><h3 class="text-white"><?php echo lang('fixed_part')?></h3></div>
							<div class="ms-auto rounded bg-electro text-white p-3 fw-bold "><span
										id="fix_percent"></span>&nbsp%
							</div>
						</div>


						<div class="fw-bold">
							<div class="srok">
								<div class="fs-1  fw-bolder"><span>0</span> KON</div>
								<p class="text-biruza extended"><?php echo lang('final_payment')?></p>
							</div>
							<div class="extended">
								<div class="srok_1">
									<span>0</span> KON
									<p class="small text-gray-600 fw-normal">1 <?php echo lang('year')?></p>
								</div>
								<div class="srok_2 d-none">
									<span>0</span> KON
									<p class="small text-gray-600 fw-normal">2 <?php echo lang('year')?></p>
								</div>
								<div class="srok_3 d-none">
									<span>0</span> KON
									<p class="small text-gray-600 fw-normal">3 <?php echo lang('year')?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 mb-5">
					<div class="bg-violet float_part text-white rounded p-8">
						<div class="d-flex bd-highlight mb-3">
							<div class="align-self-center"><h3 class="text-white"><?php echo lang('floating_part')?></h3></div>
							<div class="ms-auto rounded bg-malin text-white p-3 fw-bold "><span
										id="float_percent"></span>&nbsp%
							</div>
						</div>
						<div class="fw-bold position-relative">
							<div class="srok">
								<div class="fs-1 fw-bolder"><span>0</span>&nbspKON</div>
								<p class="text-biruza extended"><?php echo lang('total_year')?></p>
							</div>
							<div class="extended">
								<div class="srok_1">
									<span>0</span> KON
									<p class="small text-gray-600 fw-normal">1 <?php echo lang('year')?></p>
								</div>
								<div class="srok_2 d-none">
									<span>0</span> KON
									<p class="small text-gray-600 fw-normal">2 <?php echo lang('year')?></p>
								</div>
								<div class="srok_3 d-none">
									<span>0</span> KON
									<p class="small text-gray-600 fw-normal">3 <?php echo lang('year')?></p>
								</div>
								<div class="srok_4 min-w-125px d-none position-absolute bottom-0 end-0 p-5 rounded bg-darkblue">
									<div class="position-absolute end-0 me-3">
										<button type="button" data-bs-toggle="tooltip"
												class="border-0 rounded-1 bg-malin text-white"
												data-bs-custom-class="tooltip-dark" data-bs-placement="top"
												title="<?php echo lang('bonus_comment')?>">
											?
										</button>
									</div>
									<div class="text-malin"><?php echo lang('bonus')?></div>
									<div class="my-2"><span>0</span> KON</div>

									<p class="small text-gray-600 fw-normal mb-0">4 <?php echo lang('year')?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<h3 class="mb-5"><?php echo lang('profit_period')?></h3>
				<div class="col-md-6 mb-5">
					<div class="bg-white rounded p-8">
						<div class="d-flex mb-3">
							<div class="align-self-center"><h3><?php echo lang('project_tokens')?></h3></div>
							<div class="ms-auto">
								<img src="/assets/img/Color-profile.svg" alt="" />
							</div>
						</div>
						<div class="d-flex mb-3">
							<div class="align-self-center fs-35">+<span id="profit_itog"></span> KON</div>
							<div class="ms-auto">
								<div class=" rounded bg-electro text-white p-3 fw-bolder">
									+<span id="profit_itog_percent"></span>&nbsp%
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-md">
								<h3 class="color-2"><?php echo lang('portfolio_stats')?></h3>
								<table class="table mb-0">
									<tr>
										<td>
											<div class="itog_price"><span id="kon_do"></span> KON</div>
											<div class="small opacity-50"><?php echo lang('before_staking')?></div>
										</td>
										<td>
											<div class="itog_price"><span id="kon_after"></span> KON</div>
											<div class="small opacity-50"><?php echo lang('after_staking')?></div>
										</td>
									</tr>
								</table>

							</div>

						</div>
					</div>
				</div>
				<div class="col-md-6 mb-5">
					<div class="bg-violet text-white rounded p-8">
						<div class="d-flex mb-3">
							<div class="align-self-center"><h3 class="text-white"><?php echo lang('in')?> USDT</h3></div>
							<div class="ms-auto">
								<img src="/assets/img/Equalizer.svg" alt="" />
							</div>
						</div>
						<div class="d-flex mb-3">
							<div class="align-self-center fs-35">+<span id="profit_itog_dollar"></span> USDT</div>
							<div class="ms-auto">
								<div class=" rounded bg-white text-electro p-3 fw-bolder">
									+<span id="profit_itog_percent_dollar"></span>&nbsp%
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-12 col-md">
								<h3 class="opacity-50 text-white"><?php echo lang('portfolio_stats')?></h3>
								<table class="table mb-0 text-white">
									<tr>

										<td>
											<div class="itog_price text-white"><span id="money_do"></span> USDT</div>
											<div class="small opacity-50"><?php echo lang('before_staking')?></div>
										</td>
										<td>
											<div class="itog_price text-white"><span id="money_after"></span> USDT</div>
											<div class="small opacity-50"><?php echo lang('after_staking')?></div>
										</td>
									</tr>

								</table>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>


<?php include VIEWPATH . $template_path . 'common/footer.php'; ?>
