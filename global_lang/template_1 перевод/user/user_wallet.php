<?php include VIEWPATH . $template_path . 'common/header.php'; ?>

<div id="kt_content_container" class="container-xxl">

	<?php if (isset($user['tg_nic']) && ($user['tg_nic'] == '@konar777' || $user['tg_nic'] == '@crm_master')) { ?>

		<?php //$this->my_functions->vardump($user); ?>

	<?php } ?>



	<?php if (!isset($user['wallet_address']) || trim($user['wallet_address']) == '') { ?>

		<div class="row">
			<div class="col-lg-3"></div>
			<div class="col-lg-6">
				<h3 class="card-title fw-bold"><?php echo lang('wallet_auth_header')?></h3>
				<p><?php echo lang('wallet_auth_dscr')?></p>
				<div class="alert alert-danger d-flex align-items-center p-5 mb-10">
                     <span class="svg-icon svg-icon-2hx svg-icon-danger me-3">
                         <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
							  viewBox="0 0 24 24" fill="none">
                         <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"/>
                         <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)"
							   fill="black"/>
                         <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)"
							   fill="black"/>
                         </svg>
                     </span>
					<div class="d-flex flex-column">
						<h4 class="mb-1 text-danger"><?php echo lang('warning')?></h4>
						<ul>
							<li><?php echo lang('wallet_auth_id')?></li>
							<li><?php echo lang('wallet_warning')?>
							</li>
						</ul>
					</div>
				</div>
				<div class="card mb-5 mb-xl-8 shadow-sm ">
					<div class="card-header">
						<h3 class="card-title fw-bold"><?php echo lang('wallet_add')?></h3>
					</div>
					<div class="card-body border-top p-9 ">
						<div class="row mb-6">
							<div class="flex-row">
								<p class="text-gray-800 fw-normal mb-5"><?php if (!isset($domain) || $domain == NULL) { ?>
                                        $lang['wallet_current_dscr']
                                    <?php } else { ?>
                                        $lang['wallet_current_projects']
                                    <?php } ?></p>

								<!--begin::Input group-->
								<!--begin::Input-->
								<input type="text" class="form-control"
									   placeholder="0x9B6912cf3051d9598EB3FeC959E0C58C91bE31e2"
									   value="" name="wallet_address"/>
								<!--end::Input-->

								<div class="row mt-2">
									<div class="col-12 text-end">
										<button class="btn btn-success" id="sendWallet">
											<?php echo lang('btn_send')?>
										</button>
									</div>
								</div>
								<!--begin::Input group-->
							</div>
							<div class="error_text text-danger mt-2"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3"></div>
		</div>


	<?php } elseif ($user['status'] == 0) { ?>

		<div class="verification_main">
			<div class="row">
				<div class="col-lg-3"></div>
				<div class="col-lg-6">
					<h3 class="card-title fw-bold"><?php echo lang('wallet_auth_action')?></h3>
					<p>            <?php echo $user['wallet_address'] ?> <a href="">
            <span class="svg-icon svg-icon-muted svg-icon-4 ms-2" id="removeWalletAddress"><svg
						xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
<path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd"
	  d="M2 4.63158C2 3.1782 3.1782 2 4.63158 2H13.47C14.0155 2 14.278 2.66919 13.8778 3.04006L12.4556 4.35821C11.9009 4.87228 11.1726 5.15789 10.4163 5.15789H7.1579C6.05333 5.15789 5.15789 6.05333 5.15789 7.1579V16.8421C5.15789 17.9467 6.05333 18.8421 7.1579 18.8421H16.8421C17.9467 18.8421 18.8421 17.9467 18.8421 16.8421V13.7518C18.8421 12.927 19.1817 12.1387 19.7809 11.572L20.9878 10.4308C21.3703 10.0691 22 10.3403 22 10.8668V19.3684C22 20.8218 20.8218 22 19.3684 22H4.63158C3.1782 22 2 20.8218 2 19.3684V4.63158Z"
	  fill="black"/>
<path d="M10.9256 11.1882C10.5351 10.7977 10.5351 10.1645 10.9256 9.77397L18.0669 2.6327C18.8479 1.85165 20.1143 1.85165 20.8953 2.6327L21.3665 3.10391C22.1476 3.88496 22.1476 5.15129 21.3665 5.93234L14.2252 13.0736C13.8347 13.4641 13.2016 13.4641 12.811 13.0736L10.9256 11.1882Z"
	  fill="black"/>
<path d="M8.82343 12.0064L8.08852 14.3348C7.8655 15.0414 8.46151 15.7366 9.19388 15.6242L11.8974 15.2092C12.4642 15.1222 12.6916 14.4278 12.2861 14.0223L9.98595 11.7221C9.61452 11.3507 8.98154 11.5055 8.82343 12.0064Z"
	  fill="black"/>
</svg></span>
						</a>
					</p>
					<p><?php echo lang('wallet_owner_id')?></p>
					<p><?php echo lang('wallet_owner_ways')?></p>
					<ul>
						<li><?php echo lang('wallet_owner_meta')?></li>
						<li><?php echo lang('wallet_owner_transfer')?>
						</li>
					</ul>
				</div>
				<div class="col-lg-3"></div>
			</div>


			<div class="row ">
				<div class="col-lg-3"></div>
				<div class="col-lg-6">
					<div class="card mb-5 mb-xl-10 shadow-sm">
						<div class="card-header">
							<h3 class="card-title fw-bold"><?php echo lang('wallet_owner_auth')?></h3>
						</div>
						<div class="card-body border-top gy-9">
							<div class="row text-center">
								<a class="col-sm-5 bg-gray-300 p-5 rounded" href="" data-class="verification_code">
									<h2 class="my-4"><?php echo lang('wallet_owner_message')?></h2>
									<div class="mb-5">
                                    <span class="svg-icon svg-icon-5hx"><svg xmlns="http://www.w3.org/2000/svg"
																			 width="24" height="24" viewBox="0 0 24 24"
																			 fill="none">
                                    <path opacity="0.3"
										  d="M20 3H4C2.89543 3 2 3.89543 2 5V16C2 17.1046 2.89543 18 4 18H4.5C5.05228 18 5.5 18.4477 5.5 19V21.5052C5.5 22.1441 6.21212 22.5253 6.74376 22.1708L11.4885 19.0077C12.4741 18.3506 13.6321 18 14.8167 18H20C21.1046 18 22 17.1046 22 16V5C22 3.89543 21.1046 3 20 3Z"
										  fill="black"/>
                                    <rect x="6" y="12" width="7" height="2" rx="1" fill="black"/>
                                    <rect x="6" y="7" width="12" height="2" rx="1" fill="black"/>
                                    </svg></span>
									</div>
									<span class="my-6 text-muted"><?php echo lang('wallet_owner_computer')?></span>
								</a>
								<p class="col-sm-2 mt-3"><?php echo lang('or')?></p>
								<a class="col-sm-5 bg-gray-300 p-5 rounded" href="" data-class="verification_transfer">
									<h2 class="my-4"><?php echo lang('wallet_owner_transfer_min')?></h2>
									<div class="mb-5">
                                    <span class="svg-icon svg-icon-muted svg-icon-5hx"><svg
												xmlns="http://www.w3.org/2000/svg" width="24" height="24"
												viewBox="0 0 24 24" fill="none">
                                    <path opacity="0.3"
										  d="M12.5 22C11.9 22 11.5 21.6 11.5 21V3C11.5 2.4 11.9 2 12.5 2C13.1 2 13.5 2.4 13.5 3V21C13.5 21.6 13.1 22 12.5 22Z"
										  fill="black"/>
                                    <path d="M17.8 14.7C17.8 15.5 17.6 16.3 17.2 16.9C16.8 17.6 16.2 18.1 15.3 18.4C14.5 18.8 13.5 19 12.4 19C11.1 19 10 18.7 9.10001 18.2C8.50001 17.8 8.00001 17.4 7.60001 16.7C7.20001 16.1 7 15.5 7 14.9C7 14.6 7.09999 14.3 7.29999 14C7.49999 13.8 7.80001 13.6 8.20001 13.6C8.50001 13.6 8.69999 13.7 8.89999 13.9C9.09999 14.1 9.29999 14.4 9.39999 14.7C9.59999 15.1 9.8 15.5 10 15.8C10.2 16.1 10.5 16.3 10.8 16.5C11.2 16.7 11.6 16.8 12.2 16.8C13 16.8 13.7 16.6 14.2 16.2C14.7 15.8 15 15.3 15 14.8C15 14.4 14.9 14 14.6 13.7C14.3 13.4 14 13.2 13.5 13.1C13.1 13 12.5 12.8 11.8 12.6C10.8 12.4 9.99999 12.1 9.39999 11.8C8.69999 11.5 8.19999 11.1 7.79999 10.6C7.39999 10.1 7.20001 9.39998 7.20001 8.59998C7.20001 7.89998 7.39999 7.19998 7.79999 6.59998C8.19999 5.99998 8.80001 5.60005 9.60001 5.30005C10.4 5.00005 11.3 4.80005 12.3 4.80005C13.1 4.80005 13.8 4.89998 14.5 5.09998C15.1 5.29998 15.6 5.60002 16 5.90002C16.4 6.20002 16.7 6.6 16.9 7C17.1 7.4 17.2 7.69998 17.2 8.09998C17.2 8.39998 17.1 8.7 16.9 9C16.7 9.3 16.4 9.40002 16 9.40002C15.7 9.40002 15.4 9.29995 15.3 9.19995C15.2 9.09995 15 8.80002 14.8 8.40002C14.6 7.90002 14.3 7.49995 13.9 7.19995C13.5 6.89995 13 6.80005 12.2 6.80005C11.5 6.80005 10.9 7.00005 10.5 7.30005C10.1 7.60005 9.79999 8.00002 9.79999 8.40002C9.79999 8.70002 9.9 8.89998 10 9.09998C10.1 9.29998 10.4 9.49998 10.6 9.59998C10.8 9.69998 11.1 9.90002 11.4 9.90002C11.7 10 12.1 10.1 12.7 10.3C13.5 10.5 14.2 10.7 14.8 10.9C15.4 11.1 15.9 11.4 16.4 11.7C16.8 12 17.2 12.4 17.4 12.9C17.6 13.4 17.8 14 17.8 14.7Z"
										  fill="black"/>
                                    </svg></span>
									</div>
									<span class="my-6 text-muted"><?php echo lang('wallet_owner_phone')?></span>
								</a>
							</div>
						</div>
					</div>
					<div class="col-lg-3"></div>
				</div>
			</div>
		</div>


		<div class="verification_code  d-none">
			<div class="row">
				<div class="col-lg-3"></div>
				<div class="col-lg-6">
					<h3 class="card-title fw-bold"><?php echo lang('wallet_owner_sign')?></h3>
					<p>            <?php echo $user['wallet_address'] ?> <a href="">
            <span class="svg-icon svg-icon-muted svg-icon-4 ms-2" id="removeWalletAddress"><svg
						xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
<path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd"
	  d="M2 4.63158C2 3.1782 3.1782 2 4.63158 2H13.47C14.0155 2 14.278 2.66919 13.8778 3.04006L12.4556 4.35821C11.9009 4.87228 11.1726 5.15789 10.4163 5.15789H7.1579C6.05333 5.15789 5.15789 6.05333 5.15789 7.1579V16.8421C5.15789 17.9467 6.05333 18.8421 7.1579 18.8421H16.8421C17.9467 18.8421 18.8421 17.9467 18.8421 16.8421V13.7518C18.8421 12.927 19.1817 12.1387 19.7809 11.572L20.9878 10.4308C21.3703 10.0691 22 10.3403 22 10.8668V19.3684C22 20.8218 20.8218 22 19.3684 22H4.63158C3.1782 22 2 20.8218 2 19.3684V4.63158Z"
	  fill="black"/>
<path d="M10.9256 11.1882C10.5351 10.7977 10.5351 10.1645 10.9256 9.77397L18.0669 2.6327C18.8479 1.85165 20.1143 1.85165 20.8953 2.6327L21.3665 3.10391C22.1476 3.88496 22.1476 5.15129 21.3665 5.93234L14.2252 13.0736C13.8347 13.4641 13.2016 13.4641 12.811 13.0736L10.9256 11.1882Z"
	  fill="black"/>
<path d="M8.82343 12.0064L8.08852 14.3348C7.8655 15.0414 8.46151 15.7366 9.19388 15.6242L11.8974 15.2092C12.4642 15.1222 12.6916 14.4278 12.2861 14.0223L9.98595 11.7221C9.61452 11.3507 8.98154 11.5055 8.82343 12.0064Z"
	  fill="black"/>
</svg></span>
						</a>
					</p>
					<p><?php echo lang('wallet_owner_manual')?></p>
					<ul>
						<li><?php echo lang('wallet_owner_message_copy')?></li>
						<li><?php echo lang('go_link')?> <a href="https://app.mycrypto.com/sign-message" target="_blank">app.mycrypto.com</a>
						</li>
						<li><?php echo lang('wallet_connect')?>
						</li>
						<li><?php echo lang('wallet_connect_meta')?></li>
						<li><?php echo lang('wallet_connect_manual')?>
						</li>
						<li><?php echo lang('wallet_sign_meta')?></li>
						<li><?php echo lang('wallet_reply_copy')?>
						</li>
					</ul>
				</div>
				<div class="col-lg-3"></div>
			</div>
			<div class="row">
				<div class="col-lg-3"></div>
				<div class="col-lg-6">
					<div class="">
						<div class="card mb-5 mb-xl-10 shadow-sm">
							<div class="card-header">
								<h3 class="card-title fw-bold"><?php echo lang('wallet_sign_message')?></h3>
							</div>
							<div class="card-body border-top gy-9">
								<div class="row mb-6 p-6">
									<div class="flex-row">
										<label for="kt_clipboard_1" class="mb-4"><?php echo lang('wallet_message_text')?></label>
										<!--begin::Input group-->
										<div class="">
											<div class="input-group mb-6">

												<input id="kt_clipboard_1" type="text" class="form-control"
													   placeholder=""
													   value="<?php echo lang('address_header')?>< <?php echo $user['code'] ?>"/>

												<button class="btn btn-icon btn-light"
														data-clipboard-target="#kt_clipboard_1">
									<span class="svg-icon svg-icon-muted svg-icon-2hx"><svg
												xmlns="http://www.w3.org/2000/svg" width="24" height="24"
												viewBox="0 0 24 24" fill="none">
<path opacity="0.5"
	  d="M18 2H9C7.34315 2 6 3.34315 6 5H8C8 4.44772 8.44772 4 9 4H18C18.5523 4 19 4.44772 19 5V16C19 16.5523 18.5523 17 18 17V19C19.6569 19 21 17.6569 21 16V5C21 3.34315 19.6569 2 18 2Z"
	  fill="black"/>
<path fill-rule="evenodd" clip-rule="evenodd"
	  d="M14.7857 7.125H6.21429C5.62255 7.125 5.14286 7.6007 5.14286 8.1875V18.8125C5.14286 19.3993 5.62255 19.875 6.21429 19.875H14.7857C15.3774 19.875 15.8571 19.3993 15.8571 18.8125V8.1875C15.8571 7.6007 15.3774 7.125 14.7857 7.125ZM6.21429 5C4.43908 5 3 6.42709 3 8.1875V18.8125C3 20.5729 4.43909 22 6.21429 22H14.7857C16.5609 22 18 20.5729 18 18.8125V8.1875C18 6.42709 16.5609 5 14.7857 5H6.21429Z"
	  fill="black"/>
</svg></span>
												</button>
											</div>
										</div>
									</div>
								</div>
								<div class="row p-6">
									<label for="kt_clipboard_1" class="mb-4"><?php echo lang('wallet_text_paste')?></label>

									<textarea class="col-12 form-control" name="sign" id="" cols="30"
											  rows="10"></textarea>
									<div class="error_text mt-3 text-danger"></div>
								</div>
								<div class="row">
									<div class="col-12 text-end">
										<a href="/user/info" class="btn btn-light-primary me-3">
                                        <span class="svg-icon svg-icon-3 me-1">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
													 viewBox="0 0 24 24" fill="none">
													<rect opacity="0.5" x="6" y="11" width="13" height="2" rx="1"
														  fill="black"></rect>
													<path d="M8.56569 11.4343L12.75 7.25C13.1642 6.83579 13.1642 6.16421 12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75L5.70711 11.2929C5.31658 11.6834 5.31658 12.3166 5.70711 12.7071L11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25C13.1642 17.8358 13.1642 17.1642 12.75 16.75L8.56569 12.5657C8.25327 12.2533 8.25327 11.7467 8.56569 11.4343Z"
														  fill="black"></path>
												</svg>
											</span>
											<?php echo lang('back')?>
										</a>

										<button type="button" id="sendSign" class="btn btn-bg-primary text-white">
											<?php echo lang('send')?>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3"></div>
			</div>
		</div>

		<div class="row verification_transfer d-none">
			<div class="col-lg-1"></div>
			<div class="col-lg-10">
				<h3 class="card-title fw-bold"><?php echo lang('wallet_transfer_action')?></h3>
				<p>            <?php echo $user['wallet_address'] ?> <a href="">
            <span class="svg-icon svg-icon-muted svg-icon-4 ms-2" id="removeWalletAddress"><svg
						xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
<path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd"
	  d="M2 4.63158C2 3.1782 3.1782 2 4.63158 2H13.47C14.0155 2 14.278 2.66919 13.8778 3.04006L12.4556 4.35821C11.9009 4.87228 11.1726 5.15789 10.4163 5.15789H7.1579C6.05333 5.15789 5.15789 6.05333 5.15789 7.1579V16.8421C5.15789 17.9467 6.05333 18.8421 7.1579 18.8421H16.8421C17.9467 18.8421 18.8421 17.9467 18.8421 16.8421V13.7518C18.8421 12.927 19.1817 12.1387 19.7809 11.572L20.9878 10.4308C21.3703 10.0691 22 10.3403 22 10.8668V19.3684C22 20.8218 20.8218 22 19.3684 22H4.63158C3.1782 22 2 20.8218 2 19.3684V4.63158Z"
	  fill="black"/>
<path d="M10.9256 11.1882C10.5351 10.7977 10.5351 10.1645 10.9256 9.77397L18.0669 2.6327C18.8479 1.85165 20.1143 1.85165 20.8953 2.6327L21.3665 3.10391C22.1476 3.88496 22.1476 5.15129 21.3665 5.93234L14.2252 13.0736C13.8347 13.4641 13.2016 13.4641 12.811 13.0736L10.9256 11.1882Z"
	  fill="black"/>
<path d="M8.82343 12.0064L8.08852 14.3348C7.8655 15.0414 8.46151 15.7366 9.19388 15.6242L11.8974 15.2092C12.4642 15.1222 12.6916 14.4278 12.2861 14.0223L9.98595 11.7221C9.61452 11.3507 8.98154 11.5055 8.82343 12.0064Z"
	  fill="black"/>
</svg></span>
					</a>
				</p>

				<ul>
					<li><?php echo lang('wallet_number_copy')?></li>
					<li><?php echo lang('wallet_transfer_do')?> <?php echo $user['wallet_address'] ?> <?php echo lang('bep_sum')?>
					</li>
					<li><?php echo lang('test_btn_comment')?></li>
					<li><?php echo lang('operation_comment')?>
					</li>
				</ul>
			</div>
			<div class="col-lg-1"></div>
			<div class="col-lg-1"></div>
			<div class="col-lg-10">
				<div class="">
					<div class="card p-5 shadow-sm">
						<div class="row">
							<div class="col-auto border-1 ">
								<img src="<?php echo $user['qr'] ?>" alt="" class="img-fluid">
							</div>
							<div class="col p-5">
								<div class="flex-row">
									<label for="kt_clipboard_2" class="mb-4"><?php echo lang('wallet_usd_address')?></label>
									<!--begin::Input group-->
									<div class="input-group mb-6">
										<!--begin::Input-->
										<input id="kt_clipboard_2" type="text" class="form-control" placeholder=""
											   value="<?php echo $user['check_wallet'] ?>"/>
										<!--end::Input-->

										<!--begin::Button-->
										<button class="btn btn-icon btn-light" data-clipboard-target="#kt_clipboard_2">
									<span class="svg-icon svg-icon-muted svg-icon-2hx"><svg
												xmlns="http://www.w3.org/2000/svg" width="24" height="24"
												viewBox="0 0 24 24" fill="none">
<path opacity="0.5"
	  d="M18 2H9C7.34315 2 6 3.34315 6 5H8C8 4.44772 8.44772 4 9 4H18C18.5523 4 19 4.44772 19 5V16C19 16.5523 18.5523 17 18 17V19C19.6569 19 21 17.6569 21 16V5C21 3.34315 19.6569 2 18 2Z"
	  fill="black"/>
<path fill-rule="evenodd" clip-rule="evenodd"
	  d="M14.7857 7.125H6.21429C5.62255 7.125 5.14286 7.6007 5.14286 8.1875V18.8125C5.14286 19.3993 5.62255 19.875 6.21429 19.875H14.7857C15.3774 19.875 15.8571 19.3993 15.8571 18.8125V8.1875C15.8571 7.6007 15.3774 7.125 14.7857 7.125ZM6.21429 5C4.43908 5 3 6.42709 3 8.1875V18.8125C3 20.5729 4.43909 22 6.21429 22H14.7857C16.5609 22 18 20.5729 18 18.8125V8.1875C18 6.42709 16.5609 5 14.7857 5H6.21429Z"
	  fill="black"/>
</svg></span></button>
										<!--end::Button-->
									</div>
									<!--begin::Input group-->
								</div>
								<div class="flex-row">
									<div class="alert alert-danger d-flex align-items-center p-5 mb-10">
                                    <span class="svg-icon svg-icon-2hx svg-icon-danger me-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
											 viewBox="0 0 24 24" fill="none">
                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"/>
                                        <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)"
											  fill="black"/>
                                        <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)"
											  fill="black"/>
                                        </svg>
                                    </span>
										<div class="d-flex flex-column">
											<h4 class="mb-1 text-danger"><?php echo lang('warning')?></h4>
											<span><?php echo lang('wallet_bep_comment')?></span>
										</div>
									</div>

									<p class="fs-3 fw-bolder text-dark"><?php echo lang('wallet_amount')?> 1$</p>

									<p class="fs-3 fw-bolder text-dark"><?php echo lang('wallet_which')?></p>
									<p><?php echo $user['wallet_address'] ?></p>

								</div>

							</div>
						</div>

						<div class="row">
							<div class="col-12 text-end">
								<a href="/user/info" class="btn btn-light-primary me-3">
                                        <span class="svg-icon svg-icon-3 me-1">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
													 viewBox="0 0 24 24" fill="none">
													<rect opacity="0.5" x="6" y="11" width="13" height="2" rx="1"
														  fill="black"></rect>
													<path d="M8.56569 11.4343L12.75 7.25C13.1642 6.83579 13.1642 6.16421 12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75L5.70711 11.2929C5.31658 11.6834 5.31658 12.3166 5.70711 12.7071L11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25C13.1642 17.8358 13.1642 17.1642 12.75 16.75L8.56569 12.5657C8.25327 12.2533 8.25327 11.7467 8.56569 11.4343Z"
														  fill="black"></path>
												</svg>
											</span>
									<?php echo lang('back')?>
								</a>
								<button type="button" class="btn btn-bg-primary text-white" id="checkAuthPay">
									<?php echo lang('test')?>
								</button>
							</div>
						</div>


					</div>
				</div>
			</div>
			<div class="col-lg-1"></div>
		</div>

	<?php } ?>

</div>

<div class="modal fade" tabindex="-1" id="modal_change_wallet">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><?php echo lang('wallet_change')?></h5>

				<!--begin::Close-->
				<div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
					 aria-label="Close">
					<span class="svg-icon svg-icon-2x"></span>
				</div>
				<!--end::Close-->
			</div>

			<div class="modal-body">
				<p><?php echo lang('wallet_reset')?></p>
				<p><?php echo lang('wallet_reset_comment')?></p>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-light" data-bs-dismiss="modal"><?php echo lang('cancel')?></button>
				<button type="button" class="btn btn-danger" id="removeWalletAddress"><?php echo lang('wallet_change_item')?></button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" id="modal_delete_kyc">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><?php echo lang('wallet_kyc_disable')?></h5>

				<!--begin::Close-->
				<div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
					 aria-label="Close">
					<span class="svg-icon svg-icon-2x"></span>
				</div>
				<!--end::Close-->
			</div>

			<div class="modal-body">
				<p><?php echo lang('wallet_fractal_disable')?></p>
				<p><?php echo lang('wallet_reset_comment_sm')?></p>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-light" data-bs-dismiss="modal"><?php echo lang('cancel')?></button>
				<a href="/user/deletekyc" class="btn btn-danger"><?php echo lang('profile_disable')?></a>
			</div>
		</div>
	</div>
</div>


<?php include VIEWPATH . $template_path . 'common/footer.php'; ?>

