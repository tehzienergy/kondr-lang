<?php include VIEWPATH . $template_path . 'common/header.php'; ?>

<div id="kt_content_container" class="container-xxl">

	<?php

	// Отключить возможность перевода в staking/vesting
	$disable_buttons = true;

	$kon_status[0]['text'] = $lang['not_chosen'];
	$kon_status[0]['class'] = 'warning';
	$kon_status[0]['icon'] = '<path opacity="0.3" d="M20.9 12.9C20.3 12.9 19.9 12.5 19.9 11.9C19.9 11.3 20.3 10.9 20.9 10.9H21.8C21.3 6.2 17.6 2.4 12.9 2V2.9C12.9 3.5 12.5 3.9 11.9 3.9C11.3 3.9 10.9 3.5 10.9 2.9V2C6.19999 2.5 2.4 6.2 2 10.9H2.89999C3.49999 10.9 3.89999 11.3 3.89999 11.9C3.89999 12.5 3.49999 12.9 2.89999 12.9H2C2.5 17.6 6.19999 21.4 10.9 21.8V20.9C10.9 20.3 11.3 19.9 11.9 19.9C12.5 19.9 12.9 20.3 12.9 20.9V21.8C17.6 21.3 21.4 17.6 21.8 12.9H20.9Z" fill="black"/><path d="M16.9 10.9H13.6C13.4 10.6 13.2 10.4 12.9 10.2V5.90002C12.9 5.30002 12.5 4.90002 11.9 4.90002C11.3 4.90002 10.9 5.30002 10.9 5.90002V10.2C10.6 10.4 10.4 10.6 10.2 10.9H9.89999C9.29999 10.9 8.89999 11.3 8.89999 11.9C8.89999 12.5 9.29999 12.9 9.89999 12.9H10.2C10.4 13.2 10.6 13.4 10.9 13.6V13.9C10.9 14.5 11.3 14.9 11.9 14.9C12.5 14.9 12.9 14.5 12.9 13.9V13.6C13.2 13.4 13.4 13.2 13.6 12.9H16.9C17.5 12.9 17.9 12.5 17.9 11.9C17.9 11.3 17.5 10.9 16.9 10.9Z" fill="black"/>';


	$kon_status[1]['text'] = $lang['vesting'];
	$kon_status[1]['class'] = 'info';
	$kon_status[1]['icon'] = '<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"/><path d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z" fill="black"/>';


	$kon_status[2]['text'] = $lang['staking'];
	$kon_status[2]['class'] = 'success';
	$kon_status[2]['icon'] = '<rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"/><path d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z" fill="black"/>';

	?>


	<div class="row">
		<h2 class="mb-5"><?php echo lang('user_balance')?></h2>
		<?php if (isset($real_kon) && $real_kon > 0) { ?>
			<div class="col-md-3">
				<!--begin::Statistics Widget 5-->
				<div class="card bg-warning card-xl-stretch mb-xl-8">
					<!--begin::Body-->
					<div class="card-body">
						<!--begin::Svg Icon | path: icons/duotune/finance/fin006.svg-->
						<span class="svg-icon svg-icon-white svg-icon-3x ms-n1">
							<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
								 viewBox="0 0 24 24" fill="none">
								<path opacity="0.3"
									  d="M20 15H4C2.9 15 2 14.1 2 13V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V13C22 14.1 21.1 15 20 15ZM13 12H11C10.5 12 10 12.4 10 13V16C10 16.5 10.4 17 11 17H13C13.6 17 14 16.6 14 16V13C14 12.4 13.6 12 13 12Z"
									  fill="black"></path>
								<path d="M14 6V5H10V6H8V5C8 3.9 8.9 3 10 3H14C15.1 3 16 3.9 16 5V6H14ZM20 15H14V16C14 16.6 13.5 17 13 17H11C10.5 17 10 16.6 10 16V15H4C3.6 15 3.3 14.9 3 14.7V18C3 19.1 3.9 20 5 20H19C20.1 20 21 19.1 21 18V14.7C20.7 14.9 20.4 15 20 15Z"
									  fill="black"></path>
							</svg>
						</span>
						<!--end::Svg Icon-->
						<div class="text-white fw-bolder fs-2 mb-2 mt-5"><?php echo number_format($real_kon, 2, ',', ' ') ?>
							KON
						</div>
						<div class="fw-bold text-white"><?php echo lang('market_bought')?></div>
						<?php if ($disable_buttons != true) { ?>
							<button
									type="button"
									class="btn btn-white w-100 mt-5"
									data-bs-toggle="modal"
									data-bs-target="#modal_kon_action_stacking"
									data-real-kon="<?php echo $real_kon - $real_kon_stacking_sum ?>"
									<?php if ($real_kon <= $real_kon_stacking_sum) echo 'disabled'; ?>
							><?php echo lang('staking_send')?>
							</button>
						<?php } ?>

					</div>
					<!--end::Body-->
				</div>
				<!--end::Statistics Widget 5-->

			</div>

		<?php } ?>

		<?php

		if (isset($real_kon) && $real_kon > 0) {
			$v_kon = $user['sum'] - $real_kon;
		} else {
			$v_kon = $user['sum'];
		}

		?>

		<?php if ($v_kon > 0) { ?>
			<div class="col-md-3">
				<!--begin::Statistics Widget 5-->
				<div class="card bg-info card-xl-stretch mb-5 mb-xl-8">
					<!--begin::Body-->
					<div class="card-body">
						<!--begin::Svg Icon | path: icons/duotune/graphs/gra007.svg-->
						<span class="svg-icon svg-icon-white svg-icon-3x ms-n1">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
														 viewBox="0 0 24 24" fill="none">
														<path opacity="0.3"
															  d="M10.9607 12.9128H18.8607C19.4607 12.9128 19.9607 13.4128 19.8607 14.0128C19.2607 19.0128 14.4607 22.7128 9.26068 21.7128C5.66068 21.0128 2.86071 18.2128 2.16071 14.6128C1.16071 9.31284 4.96069 4.61281 9.86069 4.01281C10.4607 3.91281 10.9607 4.41281 10.9607 5.01281V12.9128Z"
															  fill="black"></path>
														<path d="M12.9607 10.9128V3.01281C12.9607 2.41281 13.4607 1.91281 14.0607 2.01281C16.0607 2.21281 17.8607 3.11284 19.2607 4.61284C20.6607 6.01284 21.5607 7.91285 21.8607 9.81285C21.9607 10.4129 21.4607 10.9128 20.8607 10.9128H12.9607Z"
															  fill="black"></path>
													</svg>
												</span>
						<!--end::Svg Icon-->
						<div class="text-white fw-bolder fs-2 mb-2 mt-5"><?php echo number_format($v_kon, 2, ',', ' ') ?>
							KON
						</div>
						<div class="fw-bold text-white"><?php echo lang('private_bought')?></div>
						<a href="/user/balance#vesting_stacking"
						   class="btn btn-white w-100 mt-5"><?php echo lang('staking/vesting')?></a>
					</div>
					<!--end::Body-->
				</div>
				<!--end::Statistics Widget 5-->
			</div>
		<?php } ?>

	</div>


	<?php if ((isset($real_kon) && $real_kon > 0) || $real_kon_stacking) { ?>
		<h2 class="mb-5"><?php echo lang('market_staking')?></h2>

		<div class="row">
			<?php foreach ($real_kon_stacking as $real_kon) { ?>


				<div class="col-xl-6">

					<div class="card bg-<?php echo $kon_status[2]['class']; ?> hoverable card-xl-stretch mb-5 mb-xl-8">
						<!--begin::Body-->
						<div class="card-body pb-6 pt-6">
							<div class="d-flex flex-stack flex-grow-1">
                    <span class="svg-icon svg-icon-white svg-icon-4x ms-n1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <?php echo $kon_status[2]['icon']; ?>
                        </svg>
					</span>
								<div class="d-flex flex-column text-end">
									<div class="text-white fw-bolder h1 font_pop mb-0"><?php echo number_format($real_kon['stacking_sum'], 0, ',', ' '); ?>
										KON
									</div>
									<span class="text-white fw-bold"><?php echo $kon_status[2]['text']; ?></span>
								</div>
							</div>
							<div class="table-responsive">
								<table class="table align-middle gs-0 gy-3">
									<thead>
									<tr>
										<th class="p-0 min-w-150px"></th>
										<th class="p-0 min-w-40px"></th>
									</tr>
									</thead>

									<tbody>
									<tr>
										<td class="fw-bolder fs-6 pe-0"><?php echo lang('plan')?></td>
										<td class="text-end fw-bolder fs-6 pe-0"><?php /*echo $virtual_kon['name'] */ ?></td>
									</tr>

									<tr>
										<td class="fw-bolder fs-6 pe-0"><?php echo lang('staking')?></td>
										<td class="text-end fw-bolder fs-6 pe-0"><?php echo $real_kon['stacking_years'] ?>
											<?php echo lang('years')?>
										</td>
									</tr>

									<tr>
										<td class="fw-bolder fs-6 pe-0"><?php echo lang('start')?></td>
										<td class="text-end fw-bolder fs-6 pe-0">01.03.2022</td>
									</tr>


									</tbody>
									<!--end::Table body-->
								</table>
								<!--end::Table-->
							</div>


							<!--<div class="row mt-6">

									<div class="col-6">
										<button
												data-bs-toggle="modal"
												data-bs-target="#modal_real_kon_cancel_stacking"
												data-stacking_id="<?php /*echo $real_kon['id']; */ ?>"
												class="w-100 btn btn-white select_pt"
										>Отменить staking
										</button>
									</div>

								</div>-->

						</div>
					</div>
				</div>
			<?php } ?>

		</div>


	<?php } ?>



	<?php if (isset($virtual_kons) && !empty($virtual_kons)) { ?>

		<h2 id="vesting_stacking" class="mb-5"><?php echo lang('vesting_staking')?></h2>

		<div class="row">
			<?php foreach ($virtual_kons as $virtual_kon) {
				if ($virtual_kon['stacking_sum'] == 0) {
					?>


					<div class="col-xl-6">

						<div class="card bg-<?php echo $kon_status[$virtual_kon['action']]['class']; ?> hoverable card-xl-stretch mb-5 mb-xl-8">
							<!--begin::Body-->
							<div class="card-body pb-6 pt-6">
								<div class="d-flex flex-stack flex-grow-1">
                    <span class="svg-icon svg-icon-white svg-icon-4x ms-n1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <?php echo $kon_status[$virtual_kon['action']]['icon']; ?>
                        </svg>
					</span>
									<div class="d-flex flex-column text-end">
										<div class="text-white fw-bolder h1 font_pop mb-0"><?php echo number_format($virtual_kon['sum'], 0, ',', ' '); ?>
											KON
										</div>
										<span class="text-white fw-bold"><?php echo $kon_status[$virtual_kon['action']]['text']; ?></span>
									</div>
								</div>
								<div class="table-responsive">
									<table class="table align-middle gs-0 gy-3">
										<thead>
										<tr>
											<th class="p-0 min-w-150px"></th>
											<th class="p-0 min-w-40px"></th>
										</tr>
										</thead>

										<tbody>
										<tr>
											<td class="fw-bolder fs-6 pe-0"><?php echo lang('plan')?></td>
											<td class="text-end fw-bolder fs-6 pe-0"><?php echo $virtual_kon['name'] ?></td>
										</tr>
										<?php if ($virtual_kon['action'] == 0 or $virtual_kon['action'] == 1) { ?>

											<tr>
												<td class="fw-bolder fs-6 pe-0"><?php echo lang('start')?></td>
												<td class="text-end fw-bolder fs-6 pe-0"><?php echo date('d.m.Y', strtotime($virtual_kon['start'])) ?></td>
											</tr>

											<tr>
												<td class="fw-bolder fs-6 pe-0"><?php echo lang('cliff')?></td>
												<td class="text-end fw-bolder fs-6 pe-0"><?php echo $virtual_kon['cliff'] ?>
													мес.
												</td>
											</tr>

											<tr>
												<td class="fw-bolder fs-6 pe-0"><?php echo lang('vesting')?></td>
												<td class="text-end fw-bolder fs-6 pe-0"></td>
											</tr>
											<?php for ($i = 0; $i <= $virtual_kon['vesting'] - 1; $i++) {
												?>
												<tr>
													<td class="fs-6 pe-0"><?php echo date('d.m.Y', strtotime('+' . $i + $virtual_kon['cliff'] . ' month', strtotime($virtual_kon['start']))); ?></td>
													<?php if ($i == $virtual_kon['vesting'] - 1) { ?>
														<td class="text-end fs-6 pe-0"><?php echo number_format($virtual_kon['sum'] - round($virtual_kon['sum'] / $virtual_kon['vesting']) * $i, 0, ',', ' '); ?>
															KON
														</td>
													<?php } else { ?>
														<td class="text-end fs-6 pe-0"><?php echo number_format($virtual_kon['sum'] / $virtual_kon['vesting'], 0, ',', ' '); ?>
															KON
														</td>
													<?php } ?>

												</tr>
											<?php } ?>

										<?php } else { ?>
											<tr>
												<td class="fw-bolder fs-6 pe-0"><?php echo lang('staking')?></td>
												<td class="text-end fw-bolder fs-6 pe-0"><?php echo $virtual_kon['stacking_years'] ?>
													<?php echo lang('years')?>
												</td>
											</tr>

											<tr>
												<td class="fw-bolder fs-6 pe-0"><?php echo lang('start')?></td>
												<td class="text-end fw-bolder fs-6 pe-0">01.03.2022</td>
											</tr>
										<?php } ?>

										</tbody>
										<!--end::Table body-->
									</table>
									<!--end::Table-->
								</div>

								<?php if ($virtual_kon['action'] == 0) { ?>
									<?php echo lang('kon_options')?>
								<?php } ?>

								<?php if (!($virtual_kon['action'] > 0 && in_array($virtual_kon['tarif_id'], [3, 6])) && $disable_buttons != true) { ?>
									<div class="row mt-6">

										<div class="col-6 <?php echo $virtual_kon['action'] == 1 ? 'd-none' : '' ?>">
											<button
													data-bs-toggle="modal"
													data-bs-target="#modal_kon_action"
													data-tarif_id="<?php echo $virtual_kon['tarif_id'] ?>"
													data-type_id="1"
													class="w-100 btn btn-white select_pt "
											><?php echo $virtual_kon['action'] == 2 ? <?php echo lang('vesting_return')?> : <?php echo lang('vesting_leave')?> ?>
											</button>
										</div>


										<div class="col-6 <?php echo $virtual_kon['action'] == 2 ? 'd-none' : '' ?>">
											<button
													data-bs-toggle="modal"
													data-bs-target="#modal_kon_action_stacking"
													data-tarif_id="<?php echo $virtual_kon['tarif_id'] ?>"
													data-type_id="2"
													data-sum="<?php echo $virtual_kon['sum']; ?>"
													class="w-100 btn btn-white select_pt "
											><?php echo lang('staking_send')?>
											</button>
										</div>

									</div>
								<?php } ?>
							</div>
						</div>
					</div>
				<?php } else { ?>

					<?php

					$current = $virtual_kon;


					for ($k = 1; $k <= 2; $k++) {
						if ($k == 1) {
							// vesting
							$virtual_kon['action'] = 1;
							$virtual_kon['sum'] = $current['sum'] - $current['stacking_sum'];

						}
						if ($k == 2) {
							// Staking
							$virtual_kon['action'] = 2;
							$virtual_kon['sum'] = $virtual_kon['stacking_sum'];
						}
						?>
						<div class="col-xl-6">

							<div class="card bg-<?php echo $kon_status[$virtual_kon['action']]['class']; ?> hoverable card-xl-stretch mb-5 mb-xl-8">
								<!--begin::Body-->
								<div class="card-body pb-6 pt-6">
									<div class="d-flex flex-stack flex-grow-1">
                    <span class="svg-icon svg-icon-white svg-icon-4x ms-n1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <?php echo $kon_status[$virtual_kon['action']]['icon']; ?>
                        </svg>
					</span>
										<div class="d-flex flex-column text-end">
											<div class="text-white fw-bolder h1 font_pop mb-0"><?php echo number_format($virtual_kon['sum'], 0, ',', ' '); ?>
												KON
											</div>
											<span class="text-white fw-bold"><?php echo $kon_status[$virtual_kon['action']]['text']; ?></span>
										</div>
									</div>
									<div class="table-responsive">
										<table class="table align-middle gs-0 gy-3">
											<thead>
											<tr>
												<th class="p-0 min-w-150px"></th>
												<th class="p-0 min-w-40px"></th>
											</tr>
											</thead>

											<tbody>
											<tr>
												<td class="fw-bolder fs-6 pe-0"><?php echo lang('plan')?></td>
												<td class="text-end fw-bolder fs-6 pe-0"><?php echo $virtual_kon['name'] ?></td>
											</tr>
											<?php if ($virtual_kon['action'] == 0 or $virtual_kon['action'] == 1) { ?>

												<tr>
													<td class="fw-bolder fs-6 pe-0"><?php echo lang('start')?></td>
													<td class="text-end fw-bolder fs-6 pe-0"><?php echo date('d.m.Y', strtotime($virtual_kon['start'])) ?></td>
												</tr>

												<tr>
													<td class="fw-bolder fs-6 pe-0"><?php echo lang('cliff')?></td>
													<td class="text-end fw-bolder fs-6 pe-0"><?php echo $virtual_kon['cliff'] ?>
														мес.
													</td>
												</tr>

												<tr>
													<td class="fw-bolder fs-6 pe-0"><?php echo lang('vesting')?></td>
													<td class="text-end fw-bolder fs-6 pe-0"></td>
												</tr>
												<?php for ($i = 0; $i <= $virtual_kon['vesting'] - 1; $i++) {
													?>
													<tr>
														<td class="fs-6 pe-0"><?php echo date('d.m.Y', strtotime('+' . $i + $virtual_kon['cliff'] . ' month', strtotime($virtual_kon['start']))); ?></td>
														<?php if ($i == $virtual_kon['vesting'] - 1) { ?>
															<td class="text-end fs-6 pe-0"><?php echo number_format($virtual_kon['sum'] - round($virtual_kon['sum'] / $virtual_kon['vesting']) * $i, 0, ',', ' '); ?>
																KON
															</td>
														<?php } else { ?>
															<td class="text-end fs-6 pe-0"><?php echo number_format($virtual_kon['sum'] / $virtual_kon['vesting'], 0, ',', ' '); ?>
																KON
															</td>
														<?php } ?>

													</tr>
												<?php } ?>

											<?php } else { ?>
												<tr>
													<td class="fw-bolder fs-6 pe-0"><?php echo lang('staking')?></td>
													<td class="text-end fw-bolder fs-6 pe-0"><?php echo $virtual_kon['stacking_years'] ?>
														<?php echo lang('years')?>
													</td>
												</tr>

												<tr>
													<td class="fw-bolder fs-6 pe-0"><?php echo lang('start')?></td>
													<td class="text-end fw-bolder fs-6 pe-0">01.03.2022</td>
												</tr>
											<?php } ?>

											</tbody>
											<!--end::Table body-->
										</table>
										<!--end::Table-->
									</div>
								</div>
							</div>
						</div>
					<?php } ?>

				<?php } ?>
			<?php } ?>
		</div>


	<?php } ?>

	<div class="row">
		<div class="col">
			<div class="card shadow-sm project_description">
				<div class="card-header">
					<div class="card-title d-flex w-100">
						<h1><?php echo lang('staking_details')?></h1>
						<div class="ms-auto"><a href="/user/calc" class="btn btn-primary">
								<span class="svg-icon svg-icon-1">
<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
<path opacity="0.3" d="M3 3V17H7V21H15V9H20V3H3Z" fill="black"/>
<path d="M20 22H3C2.4 22 2 21.6 2 21V3C2 2.4 2.4 2 3 2H20C20.6 2 21 2.4 21 3V21C21 21.6 20.6 22 20 22ZM19 4H4V8H19V4ZM6 18H4V20H6V18ZM6 14H4V16H6V14ZM6 10H4V12H6V10ZM10 18H8V20H10V18ZM10 14H8V16H10V14ZM10 10H8V12H10V10ZM14 18H12V20H14V18ZM14 14H12V16H14V14ZM14 10H12V12H14V10ZM19 14H17V20H19V14ZM19 10H17V12H19V10Z"
	  fill="black"/>
</svg>
								</span>
								<?php echo lang('staking_calc')?></a></div>

					</div>
				</div>
				<div class="card-body">


					<p><strong><?php echo lang('staking_max')?></strong> <?php echo lang('staking_tokens_20')?></p>

					<p><strong><?php echo lang('staking_terms')?></strong> <?php echo lang('staking_years')?></p>

					<h4><?php echo lang('staking_reward')?></h4>

					<ul>
						<li><?php echo lang('staking_fixed')?></li>
						<li><?php echo lang('staking_var')?></li>
					</ul>

					<p><strong><?php echo lang('fixed_apr')?></strong></p>

					<ul>
						<li><?php echo lang('year_percent')?></li>
						<li><?php echo lang('year_2_percent')?></li>
						<li><?php echo lang('year_3_percent')?></li>
					</ul>

					<p><strong><?php echo lang('var_part')?> </strong><?php echo lang('var_part_dscr')?></p>

					<ul>
						<li><?php echo lang('schedule_venture')?>
						</li>
						<li><?php echo lang('tge_stable')?></li>
					</ul>

					<p><strong><?php echo lang('var_part_comment')?></strong></p>

					<ul>
						<li><?php echo lang('btc_amount')?>
						</li>
						<li><?php echo lang('staking_term')?></li>
						<li><?php echo lang('btc_price_investment')?>
						</li>
						<li><?php echo lang('dash_result')?></li>
					</ul>

					<p><?php echo lang('table_comment')?></p>

					<p><img src="https://telegra.ph/file/496c10fd2978340bb947b.png"/></p>

					<p>&nbsp;</p>

					<p><?php echo lang('staking_explanation')?></p>

					<p><strong><?php echo lang('fines_enter')?></strong></p>

					<ul>
						<li><?php echo lang('members_reward')?> <strong><?php echo lang('client_rewards_none')?></strong></li>
						<li><strong><?php echo lang('client_fines')?></strong></li>
					</ul>

					<h4><?php echo lang('example')?> 1:</h4>

					<ul>
						<li><?php echo lang('deposit')?>: 10 000 KON</li>
						<li><?php echo lang('staking_year')?></li>
						<li><?php echo lang('reward_fixed')?> 1500 KON</li>
						<li><?php echo lang('days_90')?></li>
					</ul>

					<p><?php echo lang('payment_amount_dscr')?></p>

					<p><?php echo lang('payment_amount_header')?> = 10 000 KON + 1500 KON / 365 * 90 - 1500 KON / 2 = 9620 KON</p>

					<p>&nbsp;</p>

					<h4><?php echo lang('example')?> 2</h4>

					<ul>
						<li><?php echo lang('deposit')?>: 10 000 KON</li>
						<li><?php echo lang('staking_year')?></li>
						<li><?php echo lang('reward_fixed')?> 1500 KON</li>
						<li><?php echo lang('days_280')?></li>
					</ul>

					<p><?php echo lang('payment_amount_dscr')?></p>

					<p><?php echo lang('payment_amount_header')?> = 10 000 KON + 1500 KON / 365 * 280 - 1500 KON / 2 = 10 400 KON&nbsp;</p>

					<p>&nbsp;</p>

					<h4><?php echo lang('example')?> 3</h4>

					<ul>
						<li><?php echo lang('deposit')?>: 10 000 KON</li>
						<li><?php echo lang('staking_year_2')?></li>
						<li><?php echo lang('kon_reward_fixed')?></li>
						<li><?php echo lang('term_year_days')?></li>
					</ul>

					<p><?php echo lang('payment_amount_dscr')?></p>

					<p><?php echo lang('payment_amount_header')?> = 10 000 KON + 1500 KON&nbsp;+ 2000 KON / 365 * 100 - (1500 KON + 2000 KON) / 2 =
						10 000 KON + 1500 KON&nbsp;+ 548 KON - 1750 KON = 10 290 KON</p>

					<p><?php echo lang('client_reward_timing')?></p>

					<p>&nbsp;</p>

					<p><?php echo lang('kon_fine_pool')?></p>
				</div>
			</div>
		</div>

	</div>


</div>

<?php if ($disable_buttons != true) { ?>

	<div class="modal fade" tabindex="-1" id="modal_kon_action">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title"><?php echo lang('vesting')?></h5>

					<!--begin::Close-->
					<div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
						 aria-label="Close">
						<span class="svg-icon svg-icon-2x"></span>
					</div>
					<!--end::Close-->
				</div>

				<div class="modal-body">
					<input type="hidden" id="selected_tarif_vesting" value="">
					<input type="hidden" id="selected_type_vesting" value="">
					<p><?php echo lang('airdrop_vesting')?></p>
					<p><?php echo lang('vesting_to_staking')?></p>

				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-bs-dismiss="modal"><?php echo lang('cancel')?></button>
					<button class="btn btn-danger save_action"><?php echo lang('save')?></button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" tabindex="-1" id="modal_real_kon_cancel_stacking">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title"><?php echo lang('cancel')?> Staking</h5>

					<!--begin::Close-->
					<div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
						 aria-label="Close">
						<span class="svg-icon svg-icon-2x"></span>
					</div>
					<!--end::Close-->
				</div>

				<div class="modal-body">
					<input type="hidden" id="stacking_id" value="">
					<p><?php echo lang('staking_cancel')?></p>

				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-bs-dismiss="modal"><?php echo lang('cancel')?></button>
					<button class="btn btn-danger save_real_kon_cancel_stacking"><?php echo lang('save')?></button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" tabindex="-1" id="modal_kon_action_stacking">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title"><?php echo lang('staking')?></h5>

					<!--begin::Close-->
					<div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
						 aria-label="Close">
						<span class="svg-icon svg-icon-2x"></span>
					</div>
					<!--end::Close-->
				</div>

				<div class="modal-body">

					<div id="stacking_real_block">
						<div class="alert alert-primary d-flex align-items-center p-5 mb-5">
							<!--begin::Svg Icon | path: icons/duotune/general/gen048.svg-->
							<span class="svg-icon svg-icon-2hx svg-icon-primary me-4">
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
											 viewBox="0 0 24 24"
											 fill="none">
											<path opacity="0.3"
												  d="M20.5543 4.37824L12.1798 2.02473C12.0626 1.99176 11.9376 1.99176 11.8203 2.02473L3.44572 4.37824C3.18118 4.45258 3 4.6807 3 4.93945V13.569C3 14.6914 3.48509 15.8404 4.4417 16.984C5.17231 17.8575 6.18314 18.7345 7.446 19.5909C9.56752 21.0295 11.6566 21.912 11.7445 21.9488C11.8258 21.9829 11.9129 22 12.0001 22C12.0872 22 12.1744 21.983 12.2557 21.9488C12.3435 21.912 14.4326 21.0295 16.5541 19.5909C17.8169 18.7345 18.8277 17.8575 19.5584 16.984C20.515 15.8404 21 14.6914 21 13.569V4.93945C21 4.6807 20.8189 4.45258 20.5543 4.37824Z"
												  fill="black"></path>
											<path d="M10.5606 11.3042L9.57283 10.3018C9.28174 10.0065 8.80522 10.0065 8.51412 10.3018C8.22897 10.5912 8.22897 11.0559 8.51412 11.3452L10.4182 13.2773C10.8099 13.6747 11.451 13.6747 11.8427 13.2773L15.4859 9.58051C15.771 9.29117 15.771 8.82648 15.4859 8.53714C15.1948 8.24176 14.7183 8.24176 14.4272 8.53714L11.7002 11.3042C11.3869 11.6221 10.874 11.6221 10.5606 11.3042Z"
												  fill="black"></path>
											</svg>
										</span>
							<!--end::Svg Icon-->
							<div class="d-flex flex-column">
								<!--begin::Title-->
								<h4 class="mb-1 text-dark"><?php echo lang('warning')?></h4>
								<!--end::Title-->
								<!--begin::Content-->
								<span><?php echo lang('application_dscr')?></span>
								<!--end::Content-->
							</div>
						</div>


						<h5 class="mb-5"><?php echo lang('staking_sum')?></h5>

						<input type="number" id="real_kon_sum" value="" class="form-control mb-5">
					</div>

					<div class="error_stacking d-none my-5"></div>


					<input type="hidden" id="selected_tarif" value="">
					<input type="hidden" id="selected_type" value="">
					<h5 class="modal-title"><?php echo lang('period_choose')?></h5>


					<div class="row g-9" data-kt-buttons="true" data-kt-buttons-target="[data-kt-button='true']">
						<!--begin::Col-->
						<div class="col-md-4 col-lg-12 col-xxl-4  " id="1god">
							<!--begin::Option-->
							<label class="btn btn-outline btn-outline-dashed btn-outline-default d-flex text-start p-6 "
								   data-kt-button="true">
								<!--begin::Radio-->
								<span class="form-check form-check-custom form-check-solid form-check-sm align-items-start mt-1 disabled">
															<input class="form-check-input" type="radio"
																   name="stacking_years" value="1">
														</span>
								<!--end::Radio-->
								<!--begin::Info-->
								<span class="ms-5">
															<span class="fs-4 fw-bolder text-gray-800 mb-2 d-block">1 <?php echo lang('year')?></span>
															<span class="fw-bold fs-7 text-gray-600 d-block"><?php echo lang('reward_fixed_sum')?></span>
															<span class="fs-4 fw-bolder text-gray-600 d-block"
																  id="selected_sum_1"> KON</span>
                                							<span class="fw-bold fs-7 text-gray-600 d-block">+ <?php echo lang('var_part_sm')?> </span>


                            </span>
								<!--end::Info-->
							</label>
							<!--end::Option-->
							<div class="fv-plugins-message-container invalid-feedback"></div>
						</div>
						<!--end::Col-->
						<!--begin::Col-->
						<div class="col-md-4 col-lg-12 col-xxl-4">
							<!--begin::Option-->
							<label class="btn btn-outline btn-outline-dashed btn-outline-default d-flex text-start p-6"
								   data-kt-button="true">
								<!--begin::Radio-->
								<span class="form-check form-check-custom form-check-solid form-check-sm align-items-start mt-1">
															<input class="form-check-input" type="radio"
																   name="stacking_years" value="2">
														</span>
								<!--end::Radio-->
								<!--begin::Info-->
								<span class="ms-5">
															<span class="fs-4 fw-bolder text-gray-800 mb-2 d-block">2 <?php echo lang('years')?></span>
															<span class="fw-bold fs-7 text-gray-600 d-block"><?php echo lang('reward_fixed_sum')?></span>
															<span class="fs-4 fw-bolder text-gray-600 d-block"
																  id="selected_sum_2"> KON</span>
                                							<span class="fw-bold fs-7 text-gray-600 d-block">+ <?php echo lang('var_part_sm')?> </span>														</span>
								<!--end::Info-->
							</label>
							<!--end::Option-->
						</div>
						<!--end::Col-->
						<!--begin::Col-->
						<div class="col-md-4 col-lg-12 col-xxl-4">
							<!--begin::Option-->
							<label class="btn btn-outline btn-outline-dashed btn-outline-default d-flex text-start p-6"
								   data-kt-button="true">
								<!--begin::Radio-->
								<span class="form-check form-check-custom form-check-solid form-check-sm align-items-start mt-1">
															<input class="form-check-input" type="radio"
																   name="stacking_years" value="3">
														</span>
								<!--end::Radio-->
								<!--begin::Info-->
								<span class="ms-5">
															<span class="fs-4 fw-bolder text-gray-800 mb-2 d-block">3 <?php echo lang('years')?></span>
                                          					<span class="fw-bold fs-7 text-gray-600 d-block"><?php echo lang('reward_fixed_sum')?></span>
															<span class="fs-4 fw-bolder text-gray-600 d-block"
																  id="selected_sum_3"> KON</span>
                                							<span class="fw-bold fs-7 text-gray-600 d-block">+ <?php echo lang('var_part_sm')?> </span>

														</span>
								<!--end::Info-->
							</label>
							<!--end::Option-->
						</div>
						<!--end::Col-->
					</div>
					<div class="row">
						<div class="col mt-5" id="1god_notice">
							<?php echo lang('staking_refusal')?>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-bs-dismiss="modal"><?php echo lang('cancel')?></button>
					<button class="btn btn-danger save_action_stacking" id="save_virtual_kon_stacking"><?php echo lang('save')?>
					</button>
					<button class="btn btn-danger save_real_kon_stacking" id="save_real_kon_stacking"><?php echo lang('save')?> KON
					</button>
				</div>
			</div>
		</div>
	</div>

	<script>
		var cancelModal = document.getElementById('modal_real_kon_cancel_stacking')
		cancelModal.addEventListener('show.bs.modal', function (event) {
			// Button that triggered the modal
			var button = event.relatedTarget
			// Extract info from data-bs-* attributes
			var stacking_id = button.getAttribute('data-stacking_id')

			// If necessary, you could initiate an AJAX request here
			// and then do the updating in a callback.
			//
			// Update the modal's content.
			var modalStacking = cancelModal.querySelector('#stacking_id')


			modalStacking.value = stacking_id;


		})
	</script>

	<script>
		var exampleModal = document.getElementById('modal_kon_action')
		exampleModal.addEventListener('show.bs.modal', function (event) {
			// Button that triggered the modal
			var button = event.relatedTarget
			// Extract info from data-bs-* attributes
			var type_id = button.getAttribute('data-type_id')
			var tarif_id = button.getAttribute('data-tarif_id')
			var sum = button.getAttribute('data-sum')

			// If necessary, you could initiate an AJAX request here
			// and then do the updating in a callback.
			//
			// Update the modal's content.
			var selected_tarif = exampleModal.querySelector('#selected_tarif_vesting')
			var selected_type = exampleModal.querySelector('#selected_type_vesting')


			selected_tarif.value = tarif_id;
			selected_type.value = type_id;


		})
	</script>
	<script>
		var Modal = document.getElementById('modal_kon_action_stacking')
		Modal.addEventListener('show.bs.modal', function (event) {
			// Button that triggered the modal
			var button = event.relatedTarget

			var real_kon_sum = button.getAttribute('data-real-kon')

			// Extract info from data-bs-* attributes
			var type_id = button.getAttribute('data-type_id')
			var tarif_id = button.getAttribute('data-tarif_id')
			var sum = button.getAttribute('data-sum')

			// If necessary, you could initiate an AJAX request here
			// and then do the updating in a callback.
			//
			// Update the modal's content.
			var stacking_real_block = Modal.querySelector('#stacking_real_block')
			var selected_real_kon = Modal.querySelector('#real_kon_sum')
			var selected_tarif = Modal.querySelector('#selected_tarif')
			var selected_type = Modal.querySelector('#selected_type')
			var selected_sum_1 = Modal.querySelector('#selected_sum_1')
			var selected_sum_2 = Modal.querySelector('#selected_sum_2')
			var selected_sum_3 = Modal.querySelector('#selected_sum_3')

			var button_virtual_stacking = Modal.querySelector('#save_virtual_kon_stacking')
			var button_real_stacking = Modal.querySelector('#save_real_kon_stacking')

			document.getElementById("1god").hidden = true;

			if (tarif_id == 1 || tarif_id == 2 || tarif_id == 4) {
				document.getElementById("1god").hidden = true;
				document.getElementById("1god_notice").hidden = false;
			} else {
				document.getElementById("1god").hidden = false;
				document.getElementById("1god_notice").hidden = true;
			}

			if (real_kon_sum > 0) {
				selected_real_kon.value = real_kon_sum;
				stacking_real_block.hidden = false;

				button_real_stacking.hidden = false
				button_virtual_stacking.hidden = true

			} else {
				stacking_real_block.hidden = true;

				button_real_stacking.hidden = true
				button_virtual_stacking.hidden = false
			}

			selected_tarif.value = tarif_id;
			selected_type.value = type_id;

			if (real_kon_sum > 0) {
				selected_sum_1.textContent = real_kon_sum * 0.15 + ' KON';
				selected_sum_2.textContent = real_kon_sum * 0.15 + real_kon_sum * 0.2 + ' KON';
				selected_sum_3.textContent = real_kon_sum * 0.15 + real_kon_sum * 0.2 + real_kon_sum * 0.25 + ' KON';
			} else {
				selected_sum_1.textContent = sum * 0.15 + ' KON';
				selected_sum_2.textContent = sum * 0.15 + sum * 0.2 + ' KON';
				selected_sum_3.textContent = sum * 0.15 + sum * 0.2 + sum * 0.25 + ' KON';
			}


		})
	</script>

<?php } ?>

<?php include VIEWPATH . $template_path . 'common/footer.php'; ?>
