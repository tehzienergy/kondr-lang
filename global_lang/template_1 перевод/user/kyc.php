<?php include VIEWPATH . $template_path . 'common/header.php'; ?>

<div id="kt_content_container" class="container-xxl">


	<div class="card">
		<div class="card-header">
			<div class="card-title">
				<h1>
					KYC
				</h1>
			</div>
		</div>
		<div class="card-body">
			<?php /*$this->my_functions->vardump($user);*/ ?>

			<?php if (!isset($user['kyc_id']) || !$user['kyc_id']) { ?>
				<a class="btn btn-success" href="<?php echo $kyc_url; ?>"><?php echo lang('verification_action')?></a>
			<?php } elseif (isset($user['approved']) || !$user['approved']) { ?>
				<?php if ($user['approved'] == 0) { ?>
					<?php echo lang('verification_status_moderation')?>
				<?php } elseif ($user['approved'] == 1) { ?>
					<?php echo lang('verification_status_done')?>
				<?php } elseif ($user['approved'] == 2) { ?>
					<?php echo lang('verification_status_refused')?>
				<?php } ?>
			<?php } ?>
		</div>
	</div>


</div>


<?php include VIEWPATH . $template_path . 'common/footer.php'; ?>
