<?php include VIEWPATH . $template_path . 'register/common/header.php'; ?>

<!--begin::Authentication - Password reset -->
<div class="auth">
	<!--begin::Aside-->
	<?php

	$style = '';
	if(isset($domain['background_auth'])){
		$style = 'style="background-image: url(';
		$style .= $domain['background_auth'];
		$style .= ')"';
	}

	?>

	<div class="auth__aside" <?php echo $style; ?> >
		<a href="/" class="auth__aside-logo">
			<img src="<?php echo (isset($domain['logo'])) ? $domain['logo'] : 'assets/kondr/logo.png'?>" alt="" class="auth__aside-logo-img">
		</a>
	</div>
	<!--end::Aside-->
	<!--begin::Body-->
	<div class="auth__content auth__content--start">
		<!--begin::Wrapper-->
		<form class="form w-100" novalidate="novalidate" id="kt_password_reset_form">
			<!--begin::Heading-->
			<div class="mb-12">
				<a href="/login" class="auth__back"></a>
				<!--begin::Title-->
				<h1 class="auth__header"><?php echo lang('password_recover')?></h1>
				<!--end::Title-->
			</div>
			<!--begin::Heading-->
			<!--begin::Input group-->
			<div class="fv-row form__item">
				<label class="form-label"><?php echo lang('recover_email')?></label>
				<input class="form-control form-control-solid" type="email" placeholder="Email" name="user_email" autocomplete="off" />
			</div>
			<!--end::Input group-->
			<a href="#" class="link-primary form__link"><?php echo lang('registered_telegram')?></a>
			<!--begin::Actions-->
			<div class="d-flex flex-wrap justify-content-center pb-lg-0">
				<button type="button" id="kt_password_reset_submit" class="btn btn-lg btn-primary w-100">
					<span class="indicator-label"><?php echo lang('recover')?></span>
					<span class="indicator-progress"><?php echo lang('wait')?>
              <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
				</button>
			</div>
			<!--end::Actions-->
		</form>
		<!--end::Form-->
	</div>
	<!--end::Body-->
</div>
<!--end::Authentication - Password reset-->

<?php include VIEWPATH . $template_path . 'register/common/footer.php'; ?>
