<?php include VIEWPATH . $template_path . 'register/common/header.php'; ?>

<!--begin::Authentication - Password reset -->
<div class="auth">
	<!--begin::Aside-->
	<?php

	$style = '';
	if(isset($domain['background_auth'])){
		$style = 'style="background-image: url(';
		$style .= $domain['background_auth'];
		$style .= ')"';
	}

	?>

	<div class="auth__aside" <?php echo $style; ?> >
		<a href="/" class="auth__aside-logo">
			<img src="<?php echo (isset($domain['logo'])) ? $domain['logo'] : 'assets/kondr/logo.png'?>" alt="" class="auth__aside-logo-img">
		</a>
	</div>
	<!--end::Aside-->
	<!--begin::Body-->
	<div class="auth__content auth__content--start">
		<!--begin::Wrapper-->
		<form class="form w-100" novalidate="novalidate" id="kt_new_password_form" method="post" action="register/validationnewpassword">
			<!--begin::Heading-->
			<div class="mb-12">
				<a href="/login" class="auth__back"></a>
				<!--begin::Title-->
				<h1 class="auth__header"><?php echo lang('password_new')?></h1>
				<!--<a href="#" class="key key--go">Google</a>-->
				<!--end::Title-->
			</div>


			<input type="hidden" name="verification_key" id="" value="<?php echo $verification_key; ?>">
			<!--begin::Input group-->
			<div class="fv-row form__item" data-kt-password-meter="true">
				<!--begin::Wrapper-->
				<div class="position-relative mb-3">
					<input class="form-control form-control-lg form-control-solid" type="password" placeholder="Password" name="user_password" autocomplete="off" />
					<span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2" data-kt-password-meter-control="visibility">
                <i class="bi bi-eye-slash fs-2"></i>
                <i class="bi bi-eye fs-2 d-none"></i>
              </span>
				</div>
				<!--end::Wrapper-->
				<!--begin::Meter-->
				<div class="d-flex align-items-center mb-3" data-kt-password-meter-control="highlight">
					<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
					<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
					<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
					<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px"></div>
				</div>
				<!--end::Meter-->
				<!--begin::Hint-->
				<div class="text-muted"><?php echo lang('login_rules')?></div>
				<!--end::Hint-->
			</div>
			<!--end::Input group-->
			<!--begin::Input group-->
			<div class="fv-row form__item" data-kt-password-meter="true">
				<!--begin::Input wrapper-->
				<div class="position-relative mb-3">
					<input class="form-control form-control-lg form-control-solid" type="password" placeholder="<?php echo lang('password_confirm')?>" name="confirm-password" autocomplete="off" />
					<span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2" data-kt-password-meter-control="visibility">
                <i class="bi bi-eye-slash fs-2"></i>
                <i class="bi bi-eye fs-2 d-none"></i>
              </span>
				</div>
				<!--end::Input wrapper-->
			</div>
			<!--end::Input group-->
			<!--begin::Actions-->
			<div class="d-flex flex-wrap justify-content-center pb-lg-0 mb-12">
				<button type="button" id="kt_new_password_submit" class="btn btn-lg btn-primary w-100">
					<span class="indicator-label"><?php echo lang('save')?></span>
					<span class="indicator-progress"><?php echo lang('wait')?>
              <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
				</button>
			</div>
			<!--end::Actions-->
			<!--begin::Actions-->
			<label class="form-label"><?php echo lang('account_question')?></label>
			<div class="d-flex flex-wrap justify-content-center pb-lg-0">
				<a href="login" class="btn btn-lg btn-outline-primary w-100">
					<span class="indicator-label"><?php echo lang('login')?></span>
				</a>
			</div>
			<!--end::Actions-->
		</form>
		<!--end::Form-->
	</div>
	<!--end::Body-->
</div>
<!--end::Authentication - Password reset-->


<?php include VIEWPATH . $template_path . 'register/common/footer.php'; ?>

