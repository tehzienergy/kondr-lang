<!DOCTYPE html>
<html>
<head>
	<title><?php echo lang('registration_complete_action')?></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
</head>

<body>
<div class="container">
	<br />
	<h3 align="center"><?php echo lang('registration_complete_action')?></h3>
	<br />
	<div class="panel panel-default">
		<div class="panel-heading"><?php echo lang('register')?></div>


		<div class="panel-body">
			<?php
			if($this->session->flashdata('message'))
			{
				echo '
                    <div class="alert alert-success">
                        '.$this->session->flashdata("message").'
                    </div>
                    ';
			} else {
			?>
			<form method="post" action="<?php echo base_url(); ?>register/validation">
				<div class="form-group">
					<label><?php echo lang('email_enter')?></label>
					<input type="text" name="user_email" class="form-control" value="<?php echo set_value('user_email'); ?>" />
					<span class="text-danger"><?php echo form_error('user_email'); ?></span>
				</div>
				<div class="form-group">
					<label><?php echo lang('password_enter')?></label>
					<input type="password" name="user_password" class="form-control" value="<?php echo set_value('user_password'); ?>" />
					<span class="text-danger"><?php echo form_error('user_password'); ?></span>
				</div>
				<div class="form-group">
					<input type="submit" name="register" value="Register" class="btn btn-info" />
				</div>
			</form>
		</div>
		<?php }?>
	</div>
</div>
</body>
</html>
