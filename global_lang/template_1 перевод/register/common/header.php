<!doctype html>
<html lang="ru">
<head>
	<base href="<?php echo site_url() ?>">
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">


	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">

	<!--begin::Fonts-->
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Russo+One&display=swap" rel="stylesheet">
	<!--end::Fonts-->
	<!--begin::Global Stylesheets Bundle(used by all pages)-->
	<link href="assets/metronic/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css"/>
	<link href="assets/metronic/css/style.bundle.css" rel="stylesheet" type="text/css"/>
	<!--end::Global Stylesheets Bundle-->
	<link href="assets/css/auth.css" rel="stylesheet" type="text/css"/>

	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function (m, e, t, r, i, k, a) {
			m[i] = m[i] || function () {
				(m[i].a = m[i].a || []).push(arguments)
			};
			m[i].l = 1 * new Date();
			k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
		})
		(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

		ym(86941430, "init", {
			clickmap: true,
			trackLinks: true,
			accurateTrackBounce: true,
			webvisor: true,
			ecommerce: "dataLayer",
			userParams: {
				<?php if (isset($user['active_tir']) && isset($tirs_tree[$user['active_tir']]['name'])) {
					echo ' tir: "' . $tirs_tree[$user['active_tir']]['name'] . '",';
				} ?>
				<?php if (isset($user['sum'])) {
					echo " sum: " . round($user['sum']) . ",";
				} ?>
				<?php if (isset($user['tg_id'])) {
					echo " UserID: " . $user['tg_id'];
				} ?>
			}
		});
	</script>
	<noscript>
		<div><img src="https://mc.yandex.ru/watch/86941430" style="position:absolute; left:-9999px;" alt=""/></div>
	</noscript>
	<!-- /Yandex.Metrika counter -->
	<script type="text/javascript">
		window.dataLayer = window.dataLayer || [];
	</script>


</head>
<body id="kt_body" class="bg-body">
