<script>var hostUrl = "/assets/metronic";</script>
<!--begin::Javascript-->
<!--begin::Global Javascript Bundle(used by all pages)-->
<script src="/assets/metronic/plugins/global/plugins.bundle.js"></script>
<script src="/assets/metronic/js/scripts.bundle.js"></script>


<?php if (isset($page) && $page == 'login_page') { ?>
	<script src="/assets/metronic/js/custom/authentication/sign-in/general.js"></script>
<?php } elseif (isset($page) && $page == 'register_page') { ?>
	<script src="/assets/metronic/js/custom/authentication/sign-up/general.js"></script>
<?php } elseif (isset($page) && $page == 'recover_password') { ?>
	<script src="/assets/metronic/js/custom/authentication/password-reset/password-reset.js"></script>
<?php } elseif (isset($page) && $page == 'new_password') { ?>
	<script src="/assets/metronic/js/custom/authentication/password-reset/new-password.js"></script>
<?php }  ?>


<script src="/assets/js/myscript.js"></script>


</body>
</html>
