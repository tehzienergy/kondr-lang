<?php include VIEWPATH . $template_path . 'register/common/header.php'; ?>

<!--begin::Authentication - Password reset -->
<div class="auth">
	<!--begin::Aside-->

	<?php

	$style = '';
	if (isset($domain['background_auth'])) {
		$style = 'style="background-image: url(';
		$style .= $domain['background_auth'];
		$style .= ')"';
	}

	?>

	<div class="auth__aside" <?php echo $style; ?> >
		<a href="/" class="auth__aside-logo">
			<img src="<?php echo (isset($domain['logo'])) ? $domain['logo'] : 'assets/kondr/logo.png' ?>" alt=""
				 class="auth__aside-logo-img">
		</a>
	</div>
	<!--end::Aside-->
	<!--begin::Body-->
	<div class="auth__content auth__content--start">
		<!--begin::Wrapper-->
		<form class="form w-100" novalidate="novalidate" id="kt_sign_up_form" method="post"
			  action="register/validation">
			<!--begin::Heading-->
			<div class="mb-12">
				<!--begin::Title-->
				<?php if (isset($user['tg_id'])) { ?>
					<h1 class="auth__header mt-10"><?php echo lang('login_update')?></h1>
					<?php if ($user['tg_id'] < 100000000000) { ?>
						<p><?php echo lang('login_telegram')?></p>

						<p><?php echo lang('data_enter_message')?></p>

						<p><?php echo lang('login_future')?></p>
					<?php } ?>

				<?php } else { ?>
					<a href="/login" class="auth__back"></a>
					<h1 class="auth__header"><?php echo lang('start_header')?></h1>
					<p class="auth__text"><?php echo lang('start_comment')?></p>
				<?php } ?>
				<!--<a href="#" class="key key--go">Google</a>-->
				<!--end::Title-->
			</div>
			<!--begin::Heading-->
			<!--begin::Input group-->
			<div class="fv-row form__item">
				<!--<label class="form-label">Or continue with</label>-->
				<input class="form-control form-control-solid" type="email" placeholder="Email" name="user_email"
					   autocomplete="off"/>
			</div>
			<!--end::Input group-->
			<!--begin::Input group-->
			<div class="fv-row form__item" data-kt-password-meter="true">
				<!--begin::Wrapper-->
				<div class="position-relative mb-3">
					<input class="form-control form-control-lg form-control-solid" type="password"
						   placeholder="Password" name="user_password" autocomplete="off"/>
					<span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2"
						  data-kt-password-meter-control="visibility">
                <i class="bi bi-eye-slash fs-2"></i>
                <i class="bi bi-eye fs-2 d-none"></i>
              </span>
				</div>
				<!--end::Wrapper-->
				<!--begin::Meter-->
				<div class="d-flex align-items-center mb-3" data-kt-password-meter-control="highlight">
					<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
					<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
					<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
					<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px"></div>
				</div>
				<!--end::Meter-->
				<!--begin::Hint-->
				<div class="text-muted"><?php echo lang('login_rules')?></div>
				<!--end::Hint-->
			</div>
			<!--end::Input group-->
			<!--begin::Input group-->
			<div class="fv-row form__item" data-kt-password-meter="true">
				<!--begin::Input wrapper-->
				<div class="position-relative mb-3">
					<input class="form-control form-control-lg form-control-solid" type="password"
						   placeholder="<?php echo lang('password_confirm')?>" name="confirm-password" autocomplete="off"/>
					<span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2"
						  data-kt-password-meter-control="visibility">
                <i class="bi bi-eye-slash fs-2"></i>
                <i class="bi bi-eye fs-2 d-none"></i>
              </span>
				</div>
				<!--end::Input wrapper-->
			</div>
			<!--end::Input group-->
			<!--begin::Actions-->
			<div class="d-flex flex-wrap justify-content-center pb-lg-0 mb-12">
				<button type="button" id="kt_sign_up_submit" class="btn btn-lg btn-primary w-100">
					<span class="indicator-label"><?php echo isset($user['tg_id']) ? <?php echo lang('account_update')?> : <?php echo lang('account_create')?> ?></span>
					<span class="indicator-progress"><?php echo lang('wait')?>
              <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
				</button>
			</div>
			<!--end::Actions-->
			<?php if (!isset($user['tg_id'])) { ?>
				<!--begin::Actions-->
				<label class="form-label"><?php echo lang('account_question')?></label>
				<div class="d-flex flex-wrap justify-content-center pb-lg-0">
					<a href="login" class="btn btn-lg btn-outline-primary w-100">
						<span class="indicator-label"><?php echo lang('login')?></span>
					</a>
				</div>
				<!--end::Actions-->
			<?php } ?>
		</form>
		<!--end::Form-->
	</div>
	<!--end::Body-->
</div>
<!--end::Authentication - Password reset-->


<?php include VIEWPATH . $template_path . 'register/common/footer.php'; ?>

