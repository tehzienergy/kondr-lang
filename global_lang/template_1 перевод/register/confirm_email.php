<?php include VIEWPATH . $template_path . 'register/common/header.php'; ?>

<!--begin::Authentication - Password reset -->
<div class="auth">
	<!--begin::Aside-->
	<?php

	$style = '';
	if(isset($domain['background_auth'])){
		$style = 'style="background-image: url(';
		$style .= $domain['background_auth'];
		$style .= ')"';
	}

	?>

	<div class="auth__aside" <?php echo $style; ?> >
		<a href="/" class="auth__aside-logo">
			<img src="<?php echo (isset($domain['logo'])) ? $domain['logo'] : 'assets/kondr/logo.png'?>" alt="" class="auth__aside-logo-img">
		</a>
	</div>
	<!--end::Aside-->
	<!--begin::Body-->
	<div class="auth__content">
		<!--begin::Wrapper-->
		<form class="form w-100" novalidate="novalidate">
			<!--begin::Heading-->
			<div class="mb-10">
				<!--begin::Title-->
				<h1 class="auth__header"><?php echo lang('email_confirm')?></h1>
				<!--end::Title-->
				<div class="auth__dscr">
					<h1 class="auth__subheader"><?php echo lang('thanks')?></h1>
					<p class="auth__text"><?php echo lang('email_confirm_sent')?></p>
					<?php if(isset($user['email'])) { ?>

								<p class="auth__text mb-5"><?php echo $user['email'] ?></p>
								<p class="auth__text mb-2 text-gray-600"><?php echo lang('email_mistake')?></p>

								<a data-bs-toggle="modal" data-bs-target="#restEmailModal" class="text-decoration-underline text-gray-400"><?php echo lang('email_reset')?></a>

					<?php } ?>
				</div>
			</div>
			<!--begin::Heading-->
			<?php if(!isset($user['tg_id'])) {?>
			<!--begin::Actions-->
			<label class="form-label"><?php echo lang('account_question')?></label>
			<div class="d-flex flex-wrap justify-content-center pb-lg-0">
				<a href="login" class="btn btn-lg btn-outline-primary w-100">
					<span class="indicator-label"><?php echo lang('login')?></span>
				</a>
			</div>
			<!--end::Actions-->
			<?php } ?>
		</form>
		<!--end::Form-->
	</div>
	<!--end::Body-->
</div>

<?php if(isset($user['email'])) { ?>

	<!-- Modal -->
	<div class="modal fade" id="restEmailModal" tabindex="-1" aria-labelledby="restEmailModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel"><?php echo lang('email_reset')?></h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<p><?php echo lang('email_remove')?></p>
				</div>
				<div class="modal-footer">
					<a class="btn btn-secondary" data-bs-dismiss="modal"><?php echo lang('close')?></a>
					<a href="/register/deleteemail" class="btn btn-primary"><?php echo lang('email_reset')?></a>
				</div>
			</div>
		</div>
	</div>


<?php } ?>
<!--end::Authentication - Password reset-->
<?php include VIEWPATH . $template_path . 'register/common/footer.php'; ?>
