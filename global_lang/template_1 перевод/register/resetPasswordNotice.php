<?php include VIEWPATH . $template_path . 'register/common/header.php'; ?>


<!--begin::Authentication - Password reset -->
<div class="auth">
	<!--begin::Aside-->
	<?php

	$style = '';
	if(isset($domain['background_auth'])){
		$style = 'style="background-image: url(';
		$style .= $domain['background_auth'];
		$style .= ')"';
	}

	?>

	<div class="auth__aside" <?php echo $style; ?> >
		<a href="/" class="auth__aside-logo">
			<img src="<?php echo (isset($domain['logo'])) ? $domain['logo'] : 'assets/kondr/logo.png'?>" alt="" class="auth__aside-logo-img">
		</a>
	</div>
	<!--end::Aside-->
	<!--begin::Body-->
	<div class="auth__content">
		<!--begin::Wrapper-->
		<form class="form w-100" novalidate="novalidate">
			<!--begin::Heading-->
			<div class="mb-10">
				<!--begin::Title-->
				<h1 class="auth__header"><?php echo lang('recovery_sent')?></h1>
				<!--end::Title-->
				<div class="auth__dscr">
					<h1 class="auth__subheader"><?php echo lang('thanks')?></h1>
					<p class="auth__text"><?php echo lang('recovery_message')?></p>
				</div>
			</div>
			<!--begin::Heading-->
			<!--begin::Actions-->
			<div class="d-flex flex-wrap justify-content-center pb-lg-0">
				<a href="login" class="btn btn-lg btn-outline-primary w-100">
					<span class="indicator-label"><?php echo lang('login')?></span>
				</a>
			</div>
			<!--end::Actions-->
		</form>
		<!--end::Form-->
	</div>
	<!--end::Body-->
</div>
<!--end::Authentication - Password reset-->


<?php include VIEWPATH . $template_path . 'register/common/footer.php'; ?>
