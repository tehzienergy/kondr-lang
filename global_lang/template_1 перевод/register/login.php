<?php include VIEWPATH . $template_path . 'register/common/header.php'; ?>


<!--begin::Authentication - Password reset -->
<div class="auth">
	<!--begin::Aside-->
	<?php

	$style = '';
	if(isset($domain['background_auth'])){
		$style = 'style="background-image: url(';
		$style .= $domain['background_auth'];
		$style .= ')"';
	}

	?>

	<div class="auth__aside" <?php echo $style; ?> >
		<a href="/" class="auth__aside-logo">
			<img src="<?php echo (isset($domain['logo'])) ? $domain['logo'] : 'assets/kondr/logo.png'?>" alt="" class="auth__aside-logo-img">
		</a>
	</div>
	<!--end::Aside-->
	<!--begin::Body-->
	<div class="auth__content">
		<!--begin::Wrapper-->
		<form class="form w-100" novalidate="novalidate" id="kt_sign_in_form"
			  action="<?php echo base_url(); ?>login/validation" method="post">
			<!--begin::Heading-->
			<div class="mb-12">
				<!--begin::Title-->
				<h1 class="auth__header"><?php echo lang('welcome_back')?></h1>
				<p class="auth__text"><?php echo lang('login_header')?></p>
				<!--end::Title-->
			</div>
			<!--begin::Heading-->
			<input type="hidden" name="redirect_url" value="<?php echo  isset($redirect_url) ? $redirect_url : '' ?>" />
			<!--begin::Input group-->
			<div class="fv-row form__item">
				<input class="form-control form-control-solid" type="email" placeholder="Email"
					   name="user_email" value="<?php echo set_value('user_email'); ?>"/>
			</div>
			<!--end::Input group-->
			<!--begin::Input group-->
			<div class="mb-10 fv-row" data-kt-password-meter="true">
				<!--begin::Wrapper-->
				<div class="mb-1">
					<!--begin::Input wrapper-->
					<div class="position-relative mb-3">
						<input class="form-control form-control-lg form-control-solid" type="password"
							   placeholder="Password" autocomplete="off" name="user_password"
							   value="<?php echo set_value('user_password'); ?>"/>
						<span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2"
							  data-kt-password-meter-control="visibility">
                  <i class="bi bi-eye-slash fs-2"></i>
                  <i class="bi bi-eye fs-2 d-none"></i>
                </span>
					</div>

					<!--end::Input wrapper-->
				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Input group-->
			<!--begin::Actions-->
			<div class="auth__btns">
				<button type="submit" id="kt_sign_in_submit" class="btn btn-lg btn-primary auth__btns-item">
					<span class="indicator-label"><?php echo lang('login')?></span>
					<span class="indicator-progress"><?php echo lang('wait')?>
              <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
				</button>
				<a href="password-reset" class="btn btn-link auth__btns-item"><?php echo lang('password_recover')?></a>
			</div>
			<div class="form-check form-switch auth__switch">
				<input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault" name="remember_me" value="">
				<label class="form-check-label" for="flexSwitchCheckDefault"><?php echo lang('remember')?></label>
			</div>

			<?php if (!isset($domain) || $domain == NULL) { ?>
			<!--end::Actions-->
			<div class="auth__keys">
				<p class="auth__text auth__keys-header"><?php echo lang('login_with')?></p>
				<!--<a href="#" class="key key--go auth__keys-item">Google</a>-->
				<div class="tbutton">
					<!--<a href="#" class="key key--tg auth__keys-item">Telegram</a>
					<div class="t_gener">-->
						<script async src="https://telegram.org/js/telegram-widget.js?2"
								data-telegram-login="kondr_kon_bot" data-size="large"
								data-auth-url="/login/login_telegram<?php echo isset($redirect_url) ? '?redirect_url=' . $redirect_url : ''; ?>"
								data-request-access="write"></script>
					<!--</div>-->
				</div>
			</div>
			<!--begin::Actions-->
			<?php } ?>


			<label class="form-label"><?php echo lang('no_account')?></label>
			<div class="d-flex flex-wrap justify-content-center pb-lg-0">
				<a href="/register" class="btn btn-lg btn-outline-primary w-100">
					<span class="indicator-label"><?php echo lang('sign_up')?></span>
				</a>
			</div>
			<!--end::Actions-->
		</form>
		<!--end::Form-->
	</div>
	<!--end::Body-->
</div>
<!--end::Authentication - Password reset-->


<?php include VIEWPATH . $template_path . 'register/common/footer.php'; ?>
