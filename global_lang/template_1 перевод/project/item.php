<?php include VIEWPATH . $template_path . 'common/header.php'; ?>

<div class="container-fluid">
	<ol class="breadcrumb text-muted fs-6 fw-bold mb-6">
		<li class="breadcrumb-item pe-3"><a href="/project/" class="pe-3"><?php echo lang('projects')?></a></li>
		<li class="breadcrumb-item px-3 text-muted"><?php echo $project['name']; ?></li>
	</ol>


	<?php if (isset($project) && !empty($project)) { ?>
	<div class="row">
		<div class="ps--project-show__main col-9">
			<div class="ps--project-show__logo">
				<img class="ps--table__project-img" src="https://invest-results.ru/<?php echo $project['logo'] ?>">
			</div>
			<div class="ps--project-show__information">

				<h1 class="mb-0"><?php echo $project['name']; ?></h1>

				<div class="ps--project-show__subtitle text-active-secondary mb-1"><?php echo $project['description_small']; ?></div>
				<?php if ($project['status'] == 1) { ?>
					<?php if (strtotime($project['date_start']) == 0) { ?>
						<span class="badge badge-secondary"><?php echo lang('collection_planned')?></span>
					<?php } elseif (strtotime($project['date_start']) > time()) { ?>
						<span class="badge badge-secondary"><?php echo lang('collection_soon')?></span>
					<?php } elseif (strtotime($project['date_start']) < time() && (strtotime($project['date_end']) > time() || strtotime($project['date_end']) == 0)) { ?>
						<span class="badge badge-success"><?php echo lang('collection_current')?></span>
					<?php } else { ?>
						<span class="badge badge-danger"><?php echo lang('collection_over')?></span>
					<?php } ?>
				<?php } elseif ($project['status'] == 4) { ?>
					<span class="badge badge-warning"><?php echo lang('collection_canceled')?></span>
				<?php } elseif ($project['status'] == 2) { ?>
					<span class="badge badge-danger"><?php echo lang('collection_over')?></span>
				<?php } ?>
			</div>
		</div>
		<?php if ($project['status'] == 1) { ?>
			<div class="countdown_block_project align-middle col-xl-3 text-center mb-4 mb-md-0">
				<?php if (strtotime($project['date_start']) > time()) {
					$countdowns[] = array(
							'date' => $project['date_start'],
							'id' => 'countdown' . $project['id'],
							'zag' => $lang['collection_start_after']
					);
					?>

					<div class="zag"><?php echo lang('collection_start_after')?></div>
					<div class="countdown" id="countdown"></div>


				<?php } elseif (strtotime($project['date_start']) < time() && strtotime($project['date_start']) > 0 && (strtotime($project['date_end']) > time() || strtotime($project['date_end']) == 0)) {
					$countdowns[] = array(
							'date' => $project['date_end'],
							'id' => 'countdown' . $project['id'],
							'zag' => $lang['collection_start_after']
					);
					?>

					<div class="zag"><?php echo lang('collection_end_after')?></div>
					<div class="countdown" id="countdown"></div>

				<?php } ?>
			</div>
		<?php } ?>
	</div>

	<div class="row">
		<div class="col-xl-9">
			<div id="kt_docs_swapper_parent_1">
				<img src="https://invest-results.ru/<?php echo $project['image'] ?>"
					 class="img-fluid shadow-sm card mb-6 w-100">

				<!--begin::Swapper-->
				<div class="sticky_col"
					 data-kt-swapper="true"
					 data-kt-swapper-mode="append"
					 data-kt-swapper-parent="{
                    default: '#kt_docs_swapper_parent_1',
					md: '#kt_docs_swapper_parent_1',
					lg: '#kt_docs_swapper_parent_1',
					xl: '#kt_docs_swapper_parent_2'
					}">
					<?php if (isset($user)) { ?>
					<?php if ($user['approved'] == 1/* && $user['terms'] == 1*/) { ?>

						<?php if ($project['status'] == 1) { ?>

								<?php $active_tir_in_project =  in_array($user['active_tir'], array_column($tirs, 'tir_id')); ?>

							<?php if ($user['active_tir'] > 0 && $active_tir_in_project) { ?>
								<div class="card shadow-sm mb-6 ">
									<div class="card-body">
										<h3 class="card-title"><?php echo lang('collected')?> <?php if (!isset($domain)) { ?> <?php echo lang('in_tir')?> <?php echo $tirs_tree[$user['active_tir']]['name']; } ?></h3>
										<p class="mb-0"><span
													class="display-5"><?php echo round($active_tir_values['collected_percent'] + $active_tir_values['reserve_percent']); ?>% </span>
											<?php echo lang('in_reserve')?> <?php echo round($active_tir_values['reserve_percent']); ?>
											%
											<?php // echo round($active_tir_values['collected_amount']); ?>
										</p>
										<div class="progress h-7px bg-success bg-opacity-50 mb-3">
											<div class="progress-bar bg-success" role="progressbar"
												 style="width: <?php echo round($active_tir_values['collected_percent']); ?>%"
												 aria-valuenow="<?php echo round($active_tir_values['collected_percent']); ?>"
												 aria-valuemin="0" aria-valuemax="100"></div>
											<div class="progress-bar bg-warning" role="progressbar"
												 style="width: <?php echo round($active_tir_values['reserve_percent']); ?>%"
												 aria-valuenow="<?php echo round($active_tir_values['reserve_percent']); ?>"
												 aria-valuemin="0" aria-valuemax="100"></div>
										</div>

										<?php if (strtotime($project['date_start']) < time() && strtotime($project['date_start']) != 0 && (strtotime($project['date_end']) > time() || strtotime($project['date_end']) == 0)) { ?>

											<p><span class="h3"><?php echo lang('amount_left')?></span><span
														class="display-6"> <?php echo $active_tir_values['remaind_sum']; ?>$</span>
											</p>
											<?php if (isset($active_tir_values['remaind_sum']) && $active_tir_values['remaind_sum'] > 0) { ?>
												<?php if (isset($active_tir_values['allowed_sum']) && $active_tir_values['allowed_sum'] > 0) { ?>
													<div class="d-grid gap-2">

														<button type="button" class="btn btn-primary"
																data-bs-toggle="modal"
																data-bs-target="#kt_modal_1"
																data-remaind_sum="<?php echo $active_tir_values['remaind_sum'] ?>"
																data-comission="<?php echo $active_tir_values['comission'] ?>"
																data-allowed_sum="<?php echo $active_tir_values['allowed_sum'] ?>"
																data-tir_id="<?php echo $user['active_tir'] ?>">
															<?php echo lang('participate')?>
														</button>
													</div>
												<?php } else { ?>
													<button type="button" class="btn btn-primary" disabled>
														<?php echo lang('limit_over')?>
													</button>
												<?php } ?>
											<?php } else { ?>
												<button type="button" class="btn btn-primary" disabled>
													<?php echo lang('collection_closed')?>
												</button>
											<?php } ?>
										<?php } ?>
									</div>
								</div>
							<?php } else { ?>

							<?php } ?>

							<?php if (isset($active_product_tir_values) && !empty($active_product_tir_values)) { ?>
								<?php foreach ($active_product_tir_values as $product_tir_value) { ?>
									<?php if ($product_tir_value['sum'] == 0) continue; ?>
									<div class="card shadow-sm mb-6 ">
										<div class="card-body">
											<h3 class="card-title"><?php echo lang('collected')?>  <?php if (!isset($domain)) { ?> в тире <?php echo $tirs_tree[$product_tir_value['tir_id']]['name']; } ?></h3>
											<p class="mb-0"><span
														class="display-5"><?php echo round($product_tir_value['collected_percent'] + $product_tir_value['reserve_percent']); ?>% </span>
												из них в
												резерве <?php echo round($product_tir_value['reserve_percent']); ?>
												%
												<?php // echo round($active_tir_values['collected_amount']); ?>
											</p>
											<div class="progress h-7px bg-success bg-opacity-50 mb-3">
												<div class="progress-bar bg-success" role="progressbar"
													 style="width: <?php echo round($product_tir_value['collected_percent']); ?>%"
													 aria-valuenow="<?php echo round($product_tir_value['collected_percent']); ?>"
													 aria-valuemin="0" aria-valuemax="100"></div>
												<div class="progress-bar bg-warning" role="progressbar"
													 style="width: <?php echo round($product_tir_value['reserve_percent']); ?>%"
													 aria-valuenow="<?php echo round($product_tir_value['reserve_percent']); ?>"
													 aria-valuemin="0" aria-valuemax="100"></div>
											</div>

											<?php if (strtotime($project['date_start']) < time() && strtotime($project['date_start']) != 0 && (strtotime($project['date_end']) > time() || strtotime($project['date_end']) == 0)) { ?>

												<p><span class="h3"><?php echo lang('amount_left')?></span><span
															class="display-6"> <?php echo $product_tir_value['remaind_sum']; ?>$</span>
												</p>
												<?php if (isset($product_tir_value['remaind_sum']) && $product_tir_value['remaind_sum'] > 0) { ?>
													<?php if (isset($product_tir_value['allowed_sum']) && $product_tir_value['allowed_sum'] > 0) { ?>
														<div class="d-grid gap-2">

															<button type="button" class="btn btn-primary"
																	data-bs-toggle="modal"
																	data-bs-target="#kt_modal_1"
																	data-remaind_sum="<?php echo $product_tir_value['remaind_sum'] ?>"
																	data-comission="<?php echo $product_tir_value['comission'] ?>"
																	data-allowed_sum="<?php echo $product_tir_value['allowed_sum'] ?>"
																	data-tir_id="<?php echo $product_tir_value['tir_id'] ?>"
															>
																<?php echo lang('participate')?>
															</button>
														</div>
													<?php } else { ?>
														<button type="button" class="btn btn-primary" disabled>
															<?php echo lang('limit_over')?>
														</button>
													<?php } ?>
												<?php } else { ?>
													<button type="button" class="btn btn-primary" disabled>
														<?php echo lang('collection_closed')?>
													</button>
												<?php } ?>
											<?php } ?>
										</div>
									</div>

								<?php } ?>
							<?php } ?>
						<?php } ?>

						<?php if (!isset($domain) || is_null($domain)) { ?>
							<div class="card shadow-sm mb-6">
								<div class="card-body">
									<h3 class="card-title mb-3"><?php echo lang('allocation')?></h3>
									<div class="card_project_table">
										<?php foreach ($tirs as $tir) { ?>
											<?php if ($tir['sum'] == 0) continue; ?>
											<?php if ($tirs_tree[$tir['tir_id']]['portfolio_id'] != NULL && !isset($active_product_tir_values[$tir['tir_id']])) continue; ?>
											<div class="ps--inline-table">
												<div class="ps--inline-table__title" <?php echo ($tir['tir_id'] == $user['active_tir']) ? 'style="font-weight:bold"' : ''; ?>><?php echo $tirs_tree[$tir['tir_id']]['name'] ?></div>
												<div class="ps--inline-table__dotted"></div>
												<div class="ps--inline-table__value"><?php echo round($tir['minsum']) ?>
													- <?php echo round($tir['maxsum']) ?> $
												</div>
											</div>
										<?php } ?>
									</div>
									<div class="d-grid gap-2 mt-3">

										<a href="#" data-bs-toggle="modal" data-bs-target="#modal_buy_kon" class="btn btn-outline btn-kondr-info"><?php echo lang('kon_buy')?></a>
									</div>
								</div>
							</div>
						<?php } ?>

					<?php } else { ?>

					<?php if ($user['approved'] != 1) { ?>
						<div class="alert alert-danger d-flex align-items-center p-5 mb-5">
							<div class="d-flex flex-column">
								<h4 class="mb-2 text-danger text-center"><?php echo lang('warning')?>!</h4>
								<p class="fw-bolder mb-4"><?php echo lang('kyc_message')?></p>
								<a href="/user" class="btn btn-danger"><?php echo lang('go_profile')?></a>
							</div>
						</div>
					<?php } ?>

					<?php if ($user['terms'] != 1) { ?>
					<!--<div class="alert alert-danger d-flex align-items-center p-5 mb-0">
						<div class="d-flex flex-column">
							<h4 class="mb-2 text-danger text-center"><?php echo lang('warning')?>!</h4>
							<p class="fw-bolder mb-4"><?php echo lang('kyc_message')?></p>
							<a href="/user" class="btn btn-danger"><?php echo lang('go_profile')?></a>
						</div>-->
				</div>
				<?php } ?>


				<?php } ?>

				<?php } else { ?>

					<div class="alert alert-primary d-flex align-items-center p-5 mb-0">
						<div class="d-flex flex-column">
							<h4 class="mb-2 text-primary text-center"><?php echo lang('warning')?>!</h4>
							<p class="fw-bolder mb-3"><?php echo lang('project_auth')?></p>
							<a class="btn btn-primary" href="/login"><?php echo lang('auth')?></a>
						</div>
					</div>


				<?php } ?>
			</div>
			<!--end::Swapper-->


		</div>


		<?php if ($project['added_info_settings']) { ?>
			<?php foreach ($project['added_info_settings'] as $item) { ?>

				<?php if (isset($user['added_info'][$item['added_info_id']])) { ?>

					<div class="alert alert-warning d-flex align-items-center p-5 mb-5">
								<span class="svg-icon svg-icon-2hx svg-icon-warning me-3">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
										 viewBox="0 0 24 24" fill="none">
                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"/>
                                        <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)"
											  fill="black"/>
                                        <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)"
											  fill="black"/>
                                        </svg>
								</span>
						<div class="d-flex flex-column">
							<h4 class="mb-1 text-warning"><?php echo lang('warning')?></h4>
							<span><?php echo lang('project_blockchain')?> <?php echo $item['name'] ?>. </span>
							<span><?php echo lang('application_confirmation')?> <?php echo $item['name'] ?>, <?php echo lang('appilcation_transfer')?></span>
						</div>
					</div>

				<?php } else { ?>

					<div class="alert alert-danger d-flex align-items-center p-5 mb-5">
								<span class="svg-icon svg-icon-2hx svg-icon-danger me-3">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
										 viewBox="0 0 24 24" fill="none">
                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"/>
                                        <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)"
											  fill="black"/>
                                        <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)"
											  fill="black"/>
                                        </svg>
								</span>
						<div class="d-flex flex-column">
							<h4 class="mb-2 text-danger"><?php echo lang('warning')?></h4>
							<span><?php echo lang('project_blockchain')?>  <?php echo $item['name'] ?>. <?php echo lang('wallet_none')?>  <?php echo $item['name'] ?> <?php echo lang('wallet_none_content')?></span>

							<span class="mt-2"><a href="/user/info" class="btn btn-danger"><?php echo lang('go_profile')?></a></span>
						</div>
					</div>

				<?php } ?>
			<?php } ?>
		<?php } ?>

		<?php if (isset($user_project_orders) && !empty($user_project_orders)) { ?>
			<div class="card p-10 mb-5 shadow-sm">

				<?php /*if ($active_tir_values['allowed_sum'] > 0) { */ ?><!--
							<h3 class="card-title">Вам доступна аллокация на
								сумму <?php /*echo $active_tir_values['allowed_sum'] */ ?> $</h3>
						--><?php /*} */ ?>


				<table class="table table-hover">
					<thead>
					<tr class="fw-bolder fs-6 text-gray-800  border-bottom  border-gray-400">
						<td>id</td>
						<td><?php echo lang('status')?></td>
						<td class="d-none d-sm-table-cell"><?php echo lang('allocation_sum')?></td>
						<td><?php echo lang('sum_commission')?></td>
						<td><?php echo lang('overpayment')?></td>
						<td class="d-none d-sm-table-cell"><?php echo lang('date_create')?></td>
					</tr>
					</thead>
					<tbody>
					<?php

					$itog = array(
							'sum' => 0,
							'sum_with_percent' => 0,
							'pereplata' => 0,
					);

					foreach ($user_project_orders as $order) {

						if ($order['status'] == 3) {
							$itog['sum_with_percent'] += $order['sum_with_percent'];
							$itog['sum'] += $order['sum'];
							$itog['pereplata'] += -$order['ostatok'];
						}
						?>
						<tr>
							<td>
								<a href="/project/order/<?php echo $order['id'] ?>"><?php echo $order['id'] ?></a>
							</td>
							<td>
								<span class="badge badge-<?php echo $statuses[$order['status']]['color'] ?>"><?php echo $statuses[$order['status']]['name'] ?></span>
							</td>
							<td class="d-none d-sm-table-cell <?php echo $order['status'] != 3 ? 'text-secondary' : '' ?>"><?php echo $order['sum'] ?>
								$
							</td>
							<td class="<?php echo $order['status'] != 3 ? 'text-secondary' : '' ?>"><?php echo $order['sum_with_percent'] ?>
								$
							</td>
							<td><?php echo $order['ostatok'] != 0 ? -$order['ostatok'] . ' $' : '' ?></td>
							<td class="d-none d-sm-table-cell"><?php echo $order['create_date'] ?></td>
						</tr>
					<?php } ?>
					<tr class="border-top-1 fw-bolder">
						<td></td>
						<td></td>
						<td class="d-none d-sm-table-cell"><?php echo $itog['sum'] ?> $</td>
						<td><?php echo $itog['sum_with_percent'] ?> $</td>
						<td><?php echo $itog['pereplata'] ?> $</td>
						<td class="d-none d-sm-table-cell"></td>
					</tr>
					</tbody>
				</table>

				<?php if ($itog['pereplata'] > 0) { ?>
					<div class="">
						<p><?php echo lang('overflow_reason')?></p>
						<ul>
							<li><?php echo lang('overflow_allocation')?></li>
							<li><?php echo lang('overlow_overdue')?>
							</li>
						</ul>
						<p><?php echo lang('overflow_end')?></p>
					</div>
				<?php } ?>


				<?php /*$this->my_functions->vardump($user_project_orders) */ ?>
			</div>
		<?php } ?>

		<div class="card p-10 shadow-sm project_description">

			<?php if (isset($user) && ($user['tg_nic'] == '@konar777' || $user['tg_nic'] == '@tih0n')) { ?>

				<?php /*$this->my_functions->vardump($project) */ ?>
				<?php /*$this->my_functions->vardump($tirs)*/ ?>
				<?php /*$this->my_functions->vardump($tirs_tree)*/ ?>
				<?php /*$this->my_functions->vardump($user)*/ ?>
				<?php /*var_dump(in_array(15, array_column($user['portfolios'], 'portfolio_id')))*/ ?>
				<?php /*$this->my_functions->vardump($active_product_tir_values)*/ ?>
				<?php /*$this->my_functions->vardump($active_tir_values)*/ ?>
				<?php /*$this->my_functions->vardump($user_project_orders) */ ?>
				<?php /*$this->my_functions->vardump($project['links'])*/ ?>

			<?php } ?>

			<?php echo $project['description_site'] ?>

			<?php if (isset($project['links']) && !empty($project['links'])) { ?>
				<h3><?php echo lang('project_resources')?></h3>
				<div class="socials">
					<?php foreach ($project['links'] as $link) { ?>
						<a class="social_link <?php echo $link['icon_classname'] ?>" data-bs-toggle="tooltip"
						   data-bs-custom-class="tooltip-dark" data-bs-placement="top"
						   title="<?php echo $link['anchor'] != '' ? $link['anchor'] : $link['name'] ?>"
						   href="<?php echo $link['value'] ?>">
						</a>
					<?php } ?>
				</div>
			<?php } ?>


		</div>
	</div>


	<div class="col-xl-3" id="kt_docs_swapper_parent_2">

	</div>

</div>

<?php if ($project['status'] == 1) { ?>
	<div class="modal fade" tabindex="-1" id="kt_modal_1">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content text-white"
				 style="background: linear-gradient(67.92deg, #622BBD -7.69%, #A92BBD 123.39%);">
				<!--begin::Close-->
				<div class="btn btn-icon btn-sm btn-active-light-primary ms-2 position-absolute top-0 end-0 z-index-2 mt-2 me-2"
					 data-bs-dismiss="modal"
					 aria-label="Close">
					<span class="svg-icon svg-icon-2x">x</span>
				</div>
				<!--end::Close-->
				<div class="modal-body">
					<h3 class="modal-title mb-3 text-white"><?php echo $project['name'] ?></h3>

					<p><?php echo lang('project_sum_enter')?></p>
					<input type="hidden" name="project_id" value="<?php echo $project['id'] ?>">
					<input type="hidden" name="tir_id" value="" id="tir_id">
					<div class="row d-flex my-6 align-items-end">
						<input type="hidden" name="comission" id="comission"
							   value="">
						<div class="col-6 col-md-4">
							<label for="form_sum" class="form-label  text-white"><?php echo lang('enter')?></label>
							<input type="number" class="form-control" id="form_sum" name="form_sum"
								   value="">
						</div>
						<div class="col-6 col-md-4">
							<label for="form_sumcomission" class="form-label  text-white"><?php echo lang('commission_relevant')?></label>
							<input type="number" class="form-control" id="form_sumcomission"
								   name="form_sumcomission" value=""
								   readonly>
						</div>
						<div class="col-12 col-md-4 mt-5 mt-md-0">
							<button class="btn btn-white form-control" type="button" id="createOrder">
								<?php echo lang('participate')?>
							</button>
						</div>
					</div>

					<p class="error_modal"></p>
					<p><?php echo lang('application_note')?></p>
				</div>
			</div>
		</div>
	</div>

	<script>
		var exampleModal = document.getElementById('kt_modal_1')
		exampleModal.addEventListener('show.bs.modal', function (event) {
			// Button that triggered the modal
			var button = event.relatedTarget
			// Extract info from data-bs-* attributes
			var remaind_sum = button.getAttribute('data-remaind_sum')
			var allowed_sum = button.getAttribute('data-allowed_sum')
			var comission = button.getAttribute('data-comission')
			var tir_id = button.getAttribute('data-tir_id')
			// If necessary, you could initiate an AJAX request here
			// and then do the updating in a callback.
			//
			// Update the modal's content.
			var modalSum = exampleModal.querySelector('#form_sum')
			var modalSumWithComission = exampleModal.querySelector('#form_sumcomission')
			var modalTirId = exampleModal.querySelector('#tir_id')
			var modalComission = exampleModal.querySelector('#comission')

			var sum = +remaind_sum > +allowed_sum ? allowed_sum : remaind_sum;
			var sumWithComission = +sum + sum * comission / 100;

			modalSum.value = parseFloat(sum).toFixed(2);
			modalSumWithComission.value = parseFloat(sumWithComission).toFixed(2);
			modalTirId.value = tir_id;
			modalComission.value = comission;

		})
	</script>
<?php } ?>

<?php } else { ?>

	<p><?php echo lang('project_none')?></p>

<?php } ?>

</div>

<script>

	function setTimer(elem_id, date) {
		// set the date we're counting down to
		var target_date = new Date(date).getTime();
		// variables for time units
		var days, hours, minutes, seconds;
		// get tag element

		var countDownElem = document.getElementById(elem_id);
		//update the tag with id "countdown" every 1 second
		setInterval(function () {

			var offset = ((new Date()).getTimezoneOffset() + 180) * 60 * 1000; // Получаем отклонение от гринвича в минутах, и прибавляем к нему 3 часа (180 минут)

			var current_date = new Date().getTime() + offset;

			var seconds_left = (target_date - current_date) / 1000;
			// do some time calculations
			days = parseInt(seconds_left / 86400);
			seconds_left = seconds_left % 86400;
			hours = parseInt(seconds_left / 3600);
			seconds_left = seconds_left % 3600;
			minutes = parseInt(seconds_left / 60);
			seconds = parseInt(seconds_left % 60);


			// format countdown string + set tag value
			if (current_date < target_date) {
				countDownElem.innerHTML = days + " Д : " + hours + " Ч : " + minutes + " M : " + seconds + " С";
			} else {
				//location.reload();
			}
		}, 1000);

	}
	<?php if ($project['status'] == 1) { ?>
	<?php if (strtotime($project['date_start']) > time()) { ?>
	setTimer("countdown", "<?php echo $project['date_start'] ?>");
	<?php } elseif (strtotime($project['date_start']) < time() && (strtotime($project['date_end']) > time() || strtotime($project['date_end']) == 0)) { ?>
	setTimer("countdown", "<?php echo $project['date_end'] ?>");
	<?php } ?>
	<?php } ?>

</script>

<?php include VIEWPATH . $template_path . 'common/footer.php'; ?>
