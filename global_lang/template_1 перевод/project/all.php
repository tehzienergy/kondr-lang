<?php include VIEWPATH . $template_path . 'common/header.php'; ?>

<div class="container-fluid">
	<?php if (!isset($domain)) { ?>
	<div class="row align-items-center">
		<div class="col col-xxl-4">
			<h1 class="fs-3hx text-kondr-1"><?php echo lang('kondr_welcome')?></h1>
			<p class="my-5"><?php echo lang('kondr_header')?></p>
		</div>
		<div class="col col-xxl-8">
			<div class="card">
				<div class="card-header align-items-center border-0">
					<div class="card-title">
						<h3 class="my-5"><?php echo lang('rating_system')?></h3>
					</div>
					<div class="">
						<?php if (isset($user['status']) && $user['status'] == 1) { ?>
							<div class="btn me-4 px-0">
								<span class="icon me-1">
								<svg width="18" height="18" viewBox="0 0 60 60" fill="none"
									 xmlns="http://www.w3.org/2000/svg">
								<path fill-rule="evenodd" clip-rule="evenodd"
									  d="M60 30C60 46.5685 46.5685 60 30 60C13.4315 60 0 46.5685 0 30C0 13.4315 13.4315 0 30 0C46.5685 0 60 13.4315 60 30ZM23 33.6L44 12L51 19.2L23 48L9 33.6L16 26.4L23 33.6Z"
									  fill="#4BD37B"/>
								</svg>
							</span>
								<?php echo lang('wallet_connected')?>
							</div>
						<?php } else { ?>
							<a href="/user/wallet" class="btn btn-outline btn-kondr-info me-4"><?php echo lang('wallet_connect')?></a>
						<?php } ?>

						<?php if (isset($user['approved']) && $user['approved'] == 1) { ?>
							<div class="btn me-4 px-0">
								<span class="icon me-1">
								<svg width="18" height="18" viewBox="0 0 60 60" fill="none"
									 xmlns="http://www.w3.org/2000/svg">
								<path fill-rule="evenodd" clip-rule="evenodd"
									  d="M60 30C60 46.5685 46.5685 60 30 60C13.4315 60 0 46.5685 0 30C0 13.4315 13.4315 0 30 0C46.5685 0 60 13.4315 60 30ZM23 33.6L44 12L51 19.2L23 48L9 33.6L16 26.4L23 33.6Z"
									  fill="#4BD37B"/>
								</svg>
							</span>
								<?php echo lang('kyc_done')?>
							</div>
						<?php } else { ?>

							<a href="<?php echo 'https://fractal.id/authorize?client_id='.  (isset($domain['kyc_client_id']) ? $domain['kyc_client_id'] : 'jiN1ehEE6Z3CW5eAcH_EqpSz6BJ01CdDvaAyAZSSS98')  .'&redirect_uri=https%3A%2F%2F'. (isset($domain['domain']) ? $domain['domain'] : 'lk.kondr.io') .'%2Fuser%2Fafractal&response_type=code&scope=contact%3Aread%20verification.plus%3Aread%20verification.plus.details%3Aread%20verification.liveness%3Aread%20verification.liveness.details%3Aread'; ?>" class="btn btn-outline btn-kondr-info me-4"><?php echo lang('kyc_do')?></a>

						<?php } ?>



						<?php if (isset($user) && ($user['sum'] > 0)) { ?>
							<div class="btn me-4 px-0">
								<span class="icon me-1">
								<svg width="18" height="18" viewBox="0 0 60 60" fill="none"
									 xmlns="http://www.w3.org/2000/svg">
								<path fill-rule="evenodd" clip-rule="evenodd"
									  d="M60 30C60 46.5685 46.5685 60 30 60C13.4315 60 0 46.5685 0 30C0 13.4315 13.4315 0 30 0C46.5685 0 60 13.4315 60 30ZM23 33.6L44 12L51 19.2L23 48L9 33.6L16 26.4L23 33.6Z"
									  fill="#4BD37B"/>
								</svg>
							</span>
								<?php echo lang('kon_bought')?>
							</div>
						<?php } else { ?>
							<a href="#" data-bs-toggle="modal" data-bs-target="#modal_buy_kon" class="btn btn-outline btn-kondr-info"><?php echo lang('kon_buy')?></a>
						<?php } ?>
					</div>
				</div>
				<div class="card-body pt-0">
					<div class="row">
						<div class="table-responsive">
							<table class="table text-center">
								<tr class="border-bottom-1">
									<td>
										<button class="btn btn-kondr-level w-100"><?php echo lang('levels')?></button>
									</td>
									<td>
										<button class="btn btn-kondr-pirhania w-100"><?php echo isset($tirs_tree[1]) ? $tirs_tree[1]['name'] : '' ?></button>
									</td>
									<td>
										<button class="btn btn-kondr-marlin w-100"><?php echo isset($tirs_tree[2]) ? $tirs_tree[2]['name'] : '' ?></button>
									</td>
									<td>
										<button class="btn btn-kondr-shark w-100"><?php echo isset($tirs_tree[3]) ? $tirs_tree[3]['name'] : '' ?></button>
									</td>
									<td>
										<button class="btn btn-kondr-whale w-100"><?php echo isset($tirs_tree[4]) ? $tirs_tree[4]['name'] : '' ?></button>
									</td>
								</tr>
								<tr class="border-bottom-1 align-middle">
									<td class="text-end"><?php echo lang('sum')?> <br><?php echo lang('kon_blocking')?></td>
									<td class="fw-boldest">10 000 KON</td>
									<td class="fw-boldest">30 000 KON</td>
									<td class="fw-boldest">100 000 KON</td>
									<td class="fw-boldest">200 000 KON</td>
								</tr>
								<tr class="align-middle">
									<td class="text-end"><?php echo lang('allocation_average')?> <br><?php echo lang('project_single')?></td>
									<td class="fw-boldest">$300-600</td>
									<td class="fw-boldest">$1200-1800</td>
									<td class="fw-boldest">$4000-6000</td>
									<td class="fw-boldest"><?php echo lang('to')?> $15000</td>
								</tr>

							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>

	<?php if (isset($open_projects) && !empty($open_projects)) { ?>
		<div class="row my-6">
			<h2 class="mb-10"><?php echo lang('projects_available')?></h2>
			<?php
			$countdowns = array();

			foreach ($open_projects as $project) {
				?>

				<?php include VIEWPATH . $template_path . '/project/card_item.php'; ?>


			<?php } ?>
		</div>

	<?php } ?>

	<?php if (isset($projects) && !empty($projects)) { ?>

		<div class="row my-6">
			<h2 class="mb-10"><?php echo lang('projects_last')?></h2>
			<?php foreach ($projects as $project) { ?>

				<?php include VIEWPATH . $template_path . '/project/card_item.php'; ?>

			<?php } ?>

			<?php echo $pagination; ?>

		</div>

	<?php } ?>




</div>


<script>
	function setTimer(elem_id, date) {
		// set the date we're counting down to
		var target_date = new Date(date).getTime();
		// variables for time units
		var days, hours, minutes, seconds;
		// get tag element

		var countDownElem = document.getElementById(elem_id);
		//update the tag with id "countdown" every 1 second
		setInterval(function () {

			var offset = ((new Date()).getTimezoneOffset() + 180) * 60 * 1000; // We get the deviation from Greenwich in minutes, and add to it 3 hours (180 minutes) multiplied by 60 seconds and 1000 milliseconds

			var current_date = new Date().getTime() + offset;

			var seconds_left = (target_date - current_date) / 1000;
			// do some time calculations
			days = parseInt(seconds_left / 86400);
			seconds_left = seconds_left % 86400;
			hours = parseInt(seconds_left / 3600);
			seconds_left = seconds_left % 3600;
			minutes = parseInt(seconds_left / 60);
			seconds = parseInt(seconds_left % 60);

			// format countdown string + set tag value
			if (current_date < target_date) {
				countDownElem.innerHTML = days + " Д : " + hours + " Ч : " + minutes + " M : " + seconds + " С";
			} else {
				countDownElem.closest('.countdown_block').style.display = 'none';
			}
		}, 1000);

	}

	<?php if(isset($countdowns) && !empty($countdowns)){ ?>
	<?php  foreach ($countdowns as $countdown){ ?>
	setTimer("<?php echo $countdown['id'] ?>", "<?php echo $countdown['date'] ?>");
	<?php } ?>
	<?php } ?>

</script>

<?php include VIEWPATH . $template_path . 'common/footer.php'; ?>
