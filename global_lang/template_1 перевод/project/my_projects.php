<?php include VIEWPATH . $template_path . 'common/header.php'; ?>

<div class="container-fluid">

	<div class="row my-6">
		<h1 class="mb-6"><?php echo lang('portfolio')?></h1>

		<?php if (isset($projects) && !empty($projects)) { ?>


			<div class="card p-0">
				<table class="table align-middle table-row-dashed table-row-solid gy-4 gs-9">
					<!--begin::Thead-->
					<thead class="border-gray-200 fs-5 fw-bold bg-lighten fw-bolder">
					<tr class="thead">
						<th><?php echo lang('project')?></th>
						<th class="d-none d-md-table-cell"><?php echo lang('collection_end')?></th>
						<th class="d-none d-md-table-cell text-center"><?php echo lang('paid_applications')?></th>
						<th class="text-end"><?php echo lang('allocation_sum')?></th>
					</tr>
					</thead>
					<tbody>
					<?php foreach ($projects as $project) { ?>
						<tr class="">
							<td>
								<div class="d-flex align-items-center">
									<div class="symbol symbol-45px me-5">
										<img class="w-50px h-50px"
											 src="<?php echo 'https://invest-results.ru/' . $project['logo'] ?>" alt="">
									</div>
									<div class="d-flex justify-content-start flex-column">
										<a href="/project/show/<?php echo $project['project_id'] ?>"
										   class="text-dark fw-bolder text-hover-primary fs-6"><?php echo $project['name'] ?></a>
										<?php if ($project['description_small'] != '') { ?>
											<span class="text-muted fw-bold text-muted d-block fs-7"><?php echo $project['description_small'] ?></span>
										<?php } ?>
									</div>
								</div>
							</td>
							<td class="d-none d-md-table-cell">
								<div class="d-flex justify-content-start flex-column">
									<span class="text-dark fw-bolder"><?php echo date('Y-m-d', strtotime($project['date_end'])) ?></span>
									<span class="text-muted fw-bold text-muted"><?php echo date('H:i:s', strtotime($project['date_end'])) ?></span>
								</div>

							</td>
							<td class="d-none d-md-table-cell text-center text-dark fw-bolder"><?php echo $project['count_payed_orders'] ?></td>
							<td class="text-end fw-bolder fs-2x text-success"><?php echo number_format($project['sum'], 0, ',', ' ') ?>
								$
							</td>


						</tr>

					<?php } ?>
					</tbody>
				</table>
			</div>


		<?php } else { ?>

			<p><?php echo lang('applications_none')?></p>

		<?php } ?>


	</div>


	<?php include VIEWPATH . $template_path . 'common/footer.php'; ?>
