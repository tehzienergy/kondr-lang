<div class="col-md-6 col-xl-4 col-xxl-3 ">
	<div class="card_project shadow-sm bg-white mb-5">
		<a href="project/show/<?php echo $project['id'] ?>">
			<!--
				Черновик - не публикуем
				 Скоро сбор - указывается дата начала
				 Планируется сбор - неизвестно когда начнется
				 Идет сбор - между датой начала и датой
				 Сбор окончен
			 -->
			<div class="card_project_status">
				<?php if ($project['status'] == 1) { ?>
					<?php if (strtotime($project['date_start']) == 0) { ?>
						<span class="badge badge-secondary"><?php echo lang('collection_planned')?></span>
					<?php } elseif (strtotime($project['date_start']) > time()) { ?>
						<span class="badge badge-secondary"><?php echo lang('collection_soon')?></span>
					<?php } elseif (strtotime($project['date_start']) < time() && (strtotime($project['date_end']) > time() || strtotime($project['date_end']) == 0)) { ?>
						<span class="badge badge-success"><?php echo lang('collection_current')?></span>
					<?php } else { ?>
						<span class="badge badge-danger"><?php echo lang('collection_over')?></span>
					<?php } ?>
				<?php } elseif ($project['status'] == 4) { ?>
					<span class="badge badge-warning"><?php echo lang('collection_canceled')?></span>
				<?php } elseif ($project['status'] == 2) { ?>
					<span class="badge badge-danger"><?php echo lang('collection_over')?></span>
				<?php } ?>
			</div>

			<div class="card_project_img">
				<img src="https://invest-results.ru/<?php echo $project['image'] ?>">
			</div>
			<div class="card_project_header">
				<div class="card_project_header_logo">
					<img class="" src="https://invest-results.ru/<?php echo $project['logo'] ?>">
				</div>
				<?php if ($project['status'] == 1) { ?>
					<?php if (strtotime($project['date_start']) > time()) {
						$countdowns[] = array(
								'date' => $project['date_start'],
								'id' => 'countdown' . $project['id'],
								'zag' => $lang['collection_start_after'];
						);
						?>
						<div class="countdown_block">
							<div class="zag"><?php echo lang('collection_start_after')?></div>
							<div class="countdown" id="countdown<?php echo $project['id'] ?>"></div>
						</div>

					<?php } elseif (strtotime($project['date_start']) < time() && strtotime($project['date_start']) != 0 && (strtotime($project['date_end']) > time() || strtotime($project['date_end']) == 0)) {
						$countdowns[] = array(
								'date' => $project['date_end'],
								'id' => 'countdown' . $project['id'],
								'zag' => $lang['collection_end_after'];
						);
						?>
						<div class="countdown_block">
							<div class="zag"><?php echo lang('collection_end_after')?></div>
							<div class="countdown" id="countdown<?php echo $project['id'] ?>"></div>
						</div>
					<?php } ?>
				<?php } ?>

			</div>
			<div class="card_project_wrapper">
				<div class="card_project_info">
					<div class="">
						<h2 class="font_pop"><?php echo $project['name']; ?></h2>
						<p><?php echo $project['description_small']; ?></p>
					</div>
				</div>

				<?php if(isset($user['active_tir']) && $user['active_tir'] > 0) { ?>
				<div class="progress h-7px bg-success bg-opacity-50">
					<div class="progress-bar bg-success" role="progressbar"
						 style="width: <?php echo $project['collected_percent'] ?>%"
						 aria-valuenow="<?php echo $project['collected_percent'] ?>" aria-valuemin="0"
						 aria-valuemax="100"></div>
					<div class="progress-bar bg-warning" role="progressbar"
						 style="width: <?php echo $project['reserve_percent'] ?>%" aria-valuenow="30"
						 aria-valuemin="<?php echo $project['reserve_percent'] ?>"
						 aria-valuemax="100"></div>
				</div>
				<div class="card_project_table">
					<div class="ps--inline-table">
						<div class="ps--inline-table__title"><?php echo lang('tir_sum')?></div>
						<div class="ps--inline-table__dotted"></div>
						<div class="ps--inline-table__value">
							$<?php echo round($project['tir_sum']) ?></div>
					</div>
					<div class="ps--inline-table">
						<div class="ps--inline-table__title"><?php echo lang('collected')?></div>
						<div class="ps--inline-table__dotted"></div>
						<div class="ps--inline-table__value">
							$<?php echo round($project['collected_amount']) ?></div>
					</div>
					<div class="ps--inline-table">
						<div class="ps--inline-table__title"><?php echo lang('in_reserve')?></div>
						<div class="ps--inline-table__dotted"></div>
						<div class="ps--inline-table__value">
							$<?php echo round($project['reserve_amount']) ?></div>
					</div>
					<div class="ps--inline-table">
						<div class="ps--inline-table__title"><?php echo lang('selling_start')?></div>
						<div class="ps--inline-table__dotted"></div>
						<div class="ps--inline-table__value"><?php echo strtotime($project['date_start']) != 0 ? date('Y-m-d', strtotime($project['date_start'])) : ''; ?></div>
					</div>
					<div class="ps--inline-table">
						<div class="ps--inline-table__title"><?php echo lang('allocation_max')?></div>
						<div class="ps--inline-table__dotted"></div>
						<div class="ps--inline-table__value">$<?php echo round($project['allocation'], 0) ?></div>
					</div>
				</div>
				<?php } ?>

			</div>
		</a>
	</div>
</div>
