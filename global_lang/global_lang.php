<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['send'] = 'Отправить';

$lang['logout'] = 'Выход';
$lang['auth'] = 'Авторизация';
$lang['tg_nic'] = 'Telegram ник';
$lang['my_profile'] = 'Мой профиль';
$lang['user_info'] = 'Информация пользователя';





// Меню
$lang['projects'] = 'Проекты';
$lang['portfolio'] = 'Портфель';
$lang['kon_applications'] = 'Заявки KON';



//Футтер
$lang['if_you_have_any_questions'] = 'Если у вас возникли вопросы:';
$lang['technical_support'] = 'Техническая поддержка';
$lang['video_instruction'] = 'Видеоинструкция';



//Скрипты в футтере
$lang['payment_credited'] = 'Оплата зачислена';
$lang['go_to_the_project'] = 'Перейти в проект';
$lang['no_new_operations'] = 'Нет новых операций';
$lang['the_application_is_overdue'] = 'Заявка просрочена';


//Меню в topbar
$lang['exit'] = 'Выход';

$lang['project'] = 'Проект';
$lang['in_tirs'] = 'Вы находитесь в тире';
$lang['on_your_wallet'] = 'На вашем кошельке';



$lang['order'] = 'Заявка';


// login register
$lang['error_not_valid_values'] 		= 'Ошибка заполнения полей';
$lang['error_contact_us'] 				= 'Error. Contact us in a convenient way';
$lang['error_send_verification_code'] 	= 'Error sending verification code. Contact us in a convenient way';
$lang['error_not_registered'] 			= 'Not registered';
$lang['error_email_is_registered'] 		= 'This email is registered';
$lang['error_invalid_email_or_password'] = 'Invalid email or password';
$lang['error_invalid_link'] 			= 'Invalid Link';
$lang['error_code_not_valid'] 			= 'Code is not valid';

$lang['email_succesfull_verified'] 		= 'Your Email has been successfully verified';
$lang['change_password'] 				= 'Change password';


// User
$lang['no_vesting_data'] 				= 'Нет данных по вестингу';
$lang['no_round_data'] 					= 'Нет данных по раунду';
$lang['no_orders'] 						= 'Нет заявок';
$lang['order_not_found'] 				= 'Заявка не найдена';
$lang['not_authorized'] 				= 'не авторизован';

$lang['not_valid_vallet'] 				= 'Укажите верный адрес кошелька';









$lang['back'] = 'Назад';
$lang['cancel'] = 'Отмена';
$lang['balance'] = 'Баланс';
$lang['invested'] = 'Инвестировано';
$lang['profile'] = 'Профиль';
$lang['wallet'] = 'Кошелек';
$lang['change'] = 'Сменить';
$lang['registration'] = 'Регистрация';
$lang['or'] = 'ИЛИ';
$lang['update'] = 'Обновление';
$lang['support_action'] = 'Обратиться в поддержку';
$lang['agreements'] = 'Соглашения';
$lang['sign'] = 'Подписать';
$lang['signed'] = 'Подписано';
$lang['remove'] = 'Удалить';
$lang['add'] = 'Добавить';
$lang['save'] = 'Сохранить';
$lang['year'] = 'год';
$lang['bonus'] = 'бонус';
$lang['in'] = 'В';
$lang['staking'] = 'Staking';
$lang['plan'] = 'План';
$lang['start'] = 'Старт';
$lang['cliff'] = 'Cliff';
$lang['vesting'] = 'Vesting';
$lang['deposit'] = 'Депозит';
$lang['example'] = 'Пример';
$lang['days'] = 'дней';
$lang['text'] = 'текст';
$lang['years'] = 'года';
$lang['cancel'] = 'Отменить';
$lang['approve'] = 'Approve';
$lang['date'] = 'Date';
$lang['unlock'] = 'Unlock';
$lang['completed'] = 'Completed';
$lang['read'] = 'Прочитать';
$lang['disagree'] = 'I disagree';
$lang['agree'] = 'I agree';
$lang['terms'] = 'Terms';
$lang['register'] = 'Register';
$lang['thanks'] = 'Thank you!';
$lang['login'] = 'Log In';
$lang['wait'] = 'Please wait...';
$lang['email'] = 'Email';
$lang['password'] = 'Password';
$lang['password_confirm'] = 'Confirm Password';
$lang['recover'] = 'Recover';
$lang['remember'] = 'Remember me';
$lang['close'] = 'Close';
$lang['enter'] = 'Вход';
$lang['edit'] = 'Редактировать';
$lang['commission'] = 'Комиссия';
$lang['overpayment'] = 'Переплата';
$lang['transactions'] = 'Транзакции';
$lang['from'] = 'От';
$lang['hash'] = 'Hash';
$lang['token'] = 'Токен';
$lang['date'] = 'Дата';
$lang['investment'] = 'Investment';
$lang['term'] = 'Срок';
$lang['not_chosen'] = 'Не выбрано';
$lang['to'] = 'до';
$lang['status'] = 'Статус';
$lang['vesting_staking'] = 'Vesting/Staking с приватов';





$lang['wallet_auth_header'] = 'Авторизация кошелька';
$lang['wallet_auth_dscr'] = 'Для того, чтобы участвовать в частных раундах, вам надо авторизовать в системе личный кошелек (Metamask, Safepal, Trust и тд). Только с данного кошелька будут приниматься заявки по проектам.';
$lang['warning'] = 'Внимание';
$lang['wallet_auth_id'] = 'Ваш адрес кошелька будет единственным идентификатором вас в системе.';
$lang['wallet_warning'] = 'Никогда не используйте кошелек биржи - ваши средства будут утеряны, так как биржа не зачислит токены проектов.';
$lang['wallet_add'] = 'Добавление кошелька';
$lang['wallet_current_dscr'] = 'Данный кошелек будет использоваться для пополнения и хранения KON, а так же с него необходимо будет оплачивать участие в проектах.';
$lang['wallet_current_projects'] = 'Данный кошелек будет использоваться для участие в проектах.';
$lang['wallet_auth_action'] = 'Авторизуйте ваш кошелек';
$lang['wallet_owner_id'] = 'Теперь нам нужно убедиться в том, что данный кошелек принадлежит имнно вам.';
$lang['wallet_owner_ways'] = 'Для этого есть 2 способа:';
$lang['wallet_owner_meta'] = 'Подписав специальное сообщение кошельком через метамаск - нужен комьютер';
$lang['wallet_owner_transfer'] = 'Произведя перевод со своего кошелька на наш 1$ - можно осуществит как через телефон, так и через компьютер';
$lang['wallet_owner_auth'] = 'Подтвердите кошелек одним из способов';
$lang['wallet_owner_message'] = 'Подтвердить подписав соообщение';
$lang['wallet_owner_computer'] = 'Удобно на компьютере';
$lang['wallet_owner_transfer_min'] = 'Подтвердить переводом 1$';
$lang['wallet_owner_phone'] = 'Можно оформить с телефона';
$lang['wallet_owner_sign'] = 'Подпишите сообщение от имени вашего кошелька';
$lang['wallet_owner_manual'] = 'Для того, чтобыы аодписать сообщение от имени вашего кошелька, следуйте инструкции:';
$lang['wallet_owner_message_copy'] = 'Скопируйте текст сообщения в форме ниже';
$lang['go_link'] = 'Перейдите по ссылке';
$lang['wallet_connect'] = 'Выбирете как подключить кошелек (MetaMask, WalletConnect, Ledger и.д.). Далее инструкция будет на основе MetaMask/';
$lang['wallet_connect_meta'] = 'Одобрите в MetaMask подключение вашего кошелека';
$lang['wallet_connect_manual'] = 'В поле Message вставьте скопированное в первом пункте сообщение и нажмите "Sign Message"';
$lang['wallet_sign_meta'] = 'Подпишите сообщение в MetaMask';
$lang['wallet_reply_copy'] = 'Полностью скопируйте получившийся ответ из поля "Signature" в поле "Текст подписанного сообщения"';
$lang['wallet_sign_message'] = 'Подпишите сообщение';
$lang['wallet_message_text'] = 'Тескт сообщения';
$lang['wallet_text_paste'] = 'Сюда вставьте текст подписанного	сообщения';
$lang['wallet_transfer_action'] = 'Произведите перевод 1$ с вашего кошелька';
$lang['wallet_number_copy'] = 'Скопируйте номер кошелька в форме ниже';
$lang['wallet_usd_address'] = 'Отправьте 1$ на следующий адрес';
$lang['wallet_bep_comment'] = 'Перевод необходимо произвести в сети BEP 20 в USDT/BUSD/USDC. Переводы с обменников и биржи не учитываются и не возвращаются';
$lang['wallet_amount'] = 'Сумма:';
$lang['wallet_which'] = 'С какого кошелька:';
$lang['test'] = 'Проверить';
$lang['wallet_change'] = 'Изменение номера кошелька';
$lang['wallet_reset'] = 'Вы действительно хотите сбросить авторизацию вашего кошелька?';
$lang['wallet_reset_comment'] = 'После сброса авторизации вам нужно будет заново проходить авторизацию с новым кошельком.';
$lang['wallet_change_item'] = 'Сменить кошелек';
$lang['wallet_kyc_disable'] = 'Отвязать KYC от учетной записи';
$lang['wallet_fractal_disable'] = 'Вы действительно хотите отвязать ваш аккаунт Fractal?';
$lang['wallet_reset_comment_sm'] = 'После сброса вам нужно будет пройти авторизацию повторно.';
$lang['profile_disable'] = 'Отвязать учетную запись';
$lang['address_header'] = 'This is my address -';




$lang['verification_action'] = 'Пройти верификацию';
$lang['verification_status_moderation'] = 'На модерации';
$lang['verification_status_done'] = 'Верифицирован';
$lang['verification_status_refused'] = 'Отклонен';





$lang['tir_none'] = 'Вы не состоите ни в одном тире.';
$lang['kyc_identification'] = 'Идентификация KYC';
$lang['auth_none'] = 'Авторизация не пройдена';
$lang['kyc_profile_auth'] = 'Вам необходимо пройти подтверждение личности KYC через наших партнеров Fractal id.';
$lang['auth_fractal_comment'] = 'Перейдите по кнопке выше и еще раз авторизуйтесь в Fractal с теми данными, которые вы указывали при KYC.  В результате вас должно перекинуть обратно в наш кабинет и через пару минут информация подтвердится.';
$lang['kyc_question'] = 'Прошел KYC, но сообщение не исчезло?';
$lang['data_confirmation_pending'] = 'Ожидаем подтвержения данных';
$lang['fractal_confirmation_coment'] = 'Сейчас Fractal id проверяет ваши данные. После их подтвержения, статус в нашей системе обновится';
$lang['fractal_go'] = 'Перейти в Fractal';
$lang['id_success'] = 'Идентификация пройдена';
$lang['fractal_data_success'] = 'Ваши данные в Fractal подтверждены';
$lang['auth_denied'] = 'В авторизации отказано';
$lang['auth_fractal_denied'] = 'К сожалению  Fractal id отказал в прохождении авторизации. Для уточнения подробностей перейдите в личный кабинет.';
$lang['wallet_add'] = 'Добавить кошелек';
$lang['wallet_confirm'] = 'Подтвердить кошелек';
$lang['mail_disconnect'] = 'Отвязать почту';
$lang['defi_end'] = 'Окончание подписки DeFi';
$lang['defi_start'] = 'Оформить подписку DeFi';
$lang['wallets_additional'] = 'Дополнительные кошельки:';
$lang['wallet_vesting_comment'] = 'В некоторых проектах необходимо указывать кошельки других сетей, на них будет происходить вестинг. Рекомендуем вам указать их заранее, чтобы не тратить время в момент оформления заявки. Кошельков можно указать несколько, а во время оформления заявки просто выбрать нужный.';
$lang['email_disconnect'] = 'Отвязать email от учетной записи';
$lang['email_disconnect_confirmation'] = 'Вы действительно хотите отвязать email?';
$lang['account_remove_comment'] = 'После сброса ваш аккаунт будет удален. Вы сможете привязать email к другой учетной записи, или создать новую.';
$lang['email_disconnect_header'] = 'Отвязать email';





$lang['staking_calculator'] = 'Staking calculator';
$lang['simple'] = 'Простой';
$lang['extended'] = 'Расширенный';
$lang['staking_conditions'] = 'Условия вашего стейкинга';
$lang['staking_sum'] = 'Сумма стейкинга';
$lang['staking_term'] = 'Срок стейкинга';
$lang['courses_average'] = 'Cредние курсы';
$lang['kon_average'] = 'Средняя стоимость KON';
$lang['kon_price'] = 'Стоимость покупки KON';
$lang['projects_profit'] = 'Средняя доходность проектов';
$lang['pool_parameters'] = 'Параметры пула';
$lang['pool_terms'] = 'Распределение пула по срокам';
$lang['fixed_part'] = 'Фиксированная часть';
$lang['final_payment'] = 'Итоговая выплата в конце периода';
$lang['floating_part'] = 'Плавающая часть';
$lang['total_year'] = 'Итого (выплачивается ежегодно)';
$lang['bonus_comment'] = '*Бонус: результат от последних месяцев инвестиций в новые проекты с отложенным вестингом от последних 6 месяцев майнинга на 3ий год. DAO сможет решить инвестировать или  сразу начислать прибыль от майнинга последние 6 месяцев и тд';
$lang['profit_period'] = 'Расчетная доходность за весь период';
$lang['projects_tokens'] = 'В токенах проекта';
$lang['portfolio_stats'] = 'Показатели портфеля';
$lang['before_staking'] = 'До стейкинга';
$lang['after_staking'] = 'После стейкинга';





$lang['user_balance'] = 'Ваш баланс';
$lang['market_bought'] = 'Куплено с рынка';
$lang['staking_send'] = 'Отправить в staking';
$lang['private_bought'] = 'Куплено с приватов';
$lang['staking/vesting'] = 'Vesting/Staking';
$lang['market_staking'] = 'Staking монет с рынка';
$lang['kon_options'] = 'Пока вы еще не выбрали как поступить с вашими KON. До конца 1 марта у вас есть возможность выбрать vestig или отправить в staсking.';
$lang['vesting_return'] = 'Вернуть vesting';
$lang['vesting_leave'] = 'Оставить vesting';
$lang['staking_send'] = 'Отправить в staking';
$lang['staking_details'] = 'STAKING &ndash; детали.';
$lang['staking_calc'] = 'Калькулятор стейкинга';
$lang['staking_max'] = 'Максимальный Пул стейкинга:';
$lang['staking_tokens_20'] = '20 млн&nbsp;Токенов (макс увлечение до 25 млн возможно при большом спросе ранних участников).';
$lang['staking_terms'] = 'Сроки стейкинга:';
$lang['staking_years'] = '1, 2 или 3 года. Голос в DAO только для тех, кто стейкает от 2 лет.';
$lang['staking_reward'] = 'Вознаграждение стейкинга состоит из 2 частей:';
$lang['staking_fixed'] = 'Фиксированная часть (выплата в конце срока стейкинга).';
$lang['staking_var'] = 'Переменная часть (выплата после каждого полного года стейкинга).';
$lang['fixed_apr'] = 'Фиксированная Часть зависит от выбранного срока APR:';
$lang['year_percent'] = '1 год&nbsp;- 15% За год';
$lang['year_2_percent'] = '2 года&nbsp;- 15% за первый год + 20% за второй год';
$lang['year_3_percent'] = '3 года - 15% за первый год + 20% за второй год + 25% за третий год';
$lang['var_part'] = 'Переменная часть:';
$lang['var_part_dscr'] = 'Переменная часть - это результат майнинга и дальнейшего венчурного инвестирования в проекты, отобранные фондом.';
$lang['schedule_venture'] = 'КАЖДЫЙ месяц мы будем входить как минимум в 10 венчурных проектов на ранних стадиях не более чем, &nbsp;0,1 BTC в каждый.';
$lang['tge_stable'] = 'После наступления TGE и разлоках наш маркет мейкер будет плавно выходить в STABLE COIN.';
$lang['var_part_comment'] = 'Переменная часть зависит от следующих величин и результатов инвестиционной деятельности:';
$lang['btc_amount'] = 'Количества Добытого BTC (в среднее в месяц при курсе 40&nbsp;000$ - около 1 BTC в месяц)';
$lang['btc_price_investment'] = 'Стоимость Биткоина на момент Инвестировании в проекты (зависит размер суммы инвестирования в USDT)';
$lang['dash_result'] = 'Результатов инвестирования &ndash; средний результат условно X3 &ndash; X5 -X10-X50';
$lang['table_comment'] = 'Ниже для иллюстрации Таблица, в которой посчитано условное вознаграждение в BTC, которое надо умножить на результат инвестирования. Попадания хотябы в парочку проектов с Иксами в сотни раз, кратно увеличит вознаграждение:';
$lang['staking_explanation'] = 'Все участники Стейкинга от 2х лет - становятся полноправными членами правления DAO Treasury и смогут влиять путем голосования на распределение вознаграждения из фонда DAO Treasury.';
$lang['fines_enter'] = 'Какие штрафы за ранний выход?';
$lang['members_reward'] = 'Участники получаю фиксированное вознаграждение в конце периода блокировки. Если участник хочет забрать свои монеты до достижения окончания срока блокировки, то такое снятие облагается штрафом по следующим правилам:';
$lang['client_rewards_none'] = 'Клиент полностью лишается всех невыплаченных переменных вознаграждений';
$lang['client_fines'] = 'С клиента взимается штраф в размере 50% всего фиксированного вознаграждения за весь планируемый срок блокировки.';
$lang['staking_year'] = 'Срок стейкинга: 1 год (365 дней)';
$lang['reward_fixed'] = 'Фиксированное вознаграждение за весь срок:';
$lang['days_90'] = 'Прошедший срок: 90';
$lang['days_280'] = 'Прошедший срок: 280 дней';
$lang['payment_amount_dscr'] = 'Сумма к выплате = Депозит + Текущее вознаграждение - Штраф за раннее снятие';
$lang['payment_amount_header'] = 'Сумма к выплате';
$lang['staking_year_2'] = 'Срок стейкинга: 2 года (2*365 дней)';
$lang['kon_reward_fixed'] = 'Фиксированное вознаграждение: 1й год 1500 KON, 2ой год 2000 KON';
$lang['term_year_days'] = 'Прошедший срок: 1 год и 100 дней';
$lang['client_reward_timing'] = 'Также, к этому моменту клиент уже получит переменную часть вознаграждения за первый год.';
$lang['kon_fine_pool'] = 'Штрафные KON добавляются к пулу переменного вознаграждения.';
$lang['airdrop_vesting'] = 'Vesting будет производится через airdrop контракт в соотвествии с графиком.';
$lang['vesting_to_staking'] = 'Поменять Vesting на Staking можно вплоть до конца 1 марта.';
$lang['staking_cancel'] = 'Отмена стейкинга';
$lang['application_dscr'] = 'Данная заявка является предварительной и позволит вам зафиксировать за собой дату начала стейкинга от 1 марта. До конца месяца заработает Staking контракт и вам необходимо будет отправить ваши KON на него. Об этом вы получите уведомление в бота.';
$lang['staking_sum'] = 'Укажите сумму которую вы хотите отправить в Staking';
$lang['period_choose'] = 'Выбирите период:';
$lang['reward_fixed_sum'] = 'Фиксированная сумма вознаграждения:';
$lang['var_part_sm'] = 'переменная часть';
$lang['staking_refusal'] = 'Вам не доступен staking сроком на 1 год, так как он заканчивается раньше, чем vesting в соотвествии с вашим планом.';






$lang['vesting_calendar'] = 'Vesting calendar';
$lang['project_only_users'] = 'Only my projects';
$lang['vesting_show_everyday'] = 'Show everyday vesting';
$lang['no_recent_payments'] = 'Нет выплат на ближайшие даты';




$lang['project_round'] = 'Project and round';
$lang['vesting_progress'] = 'Vesting progress';
$lang['coins_my'] = 'My coins';
$lang['seed_round'] = 'Seed round';
$lang['vesting_progress_tge'] = 'Vesting Progress since TGE started';





$lang['payments_schedule'] = 'Календарь выплат';





$lang['agreement_updated'] = 'У нас обновилось пользовательское соглашение';
$lang['read_sign_dscr'] = 'Необходимо внимательно ознакомиться и подписать';
$lang['terms_comment'] = 'By electronically accepting the Sales Agreement based on Standard Terms and Conditions, you expressly and univocally confirm that your have carefully read the Standard Terms and Conditions and consulted to your lawyer where required and agree to be legally bound by the terms and conditions of these Standard Terms and Conditions.';
$lang['agree_dao'] = 'I agree to be legally bound to DAO KONDR PTE LTD’s Standard Terms and Conditions';
$lang['agreement_signed'] = 'Соглашение подписано';





$lang['registration_complete_action'] = 'Complete User Registration and Login System in Codeigniter';
$lang['email_enter'] = 'Enter Your Valid Email Address';
$lang['password_enter'] = 'Enter Password';





$lang['recovery_sent'] = 'Recover code send';
$lang['recovery_message'] = "We've sent recovery instructions to your e-mail";





$lang['login_update'] = 'Update Login Data';
$lang['login_telegram'] = 'You have logged in via Telegram.';
$lang['data_enter_message'] = "You didn't set a username and password. Enter the necessary data.";
$lang['login_future'] = 'In the future, you will be able to log in via Telegram or Email';
$lang['start_header'] = 'Get Started With Us';
$lang['start_comment'] = 'Getting started is easy';
$lang['login_rules'] = 'Use 8 or more characters with a mix of letters, numbers &amp; symbols.';
$lang['account_question'] = 'Have an account?';
$lang['account_update'] = 'Update Account';
$lang['account_create'] = 'Create Account';
$lang['bot_connected'] = 'Подключен к боту';





$lang['password_recover'] = 'Recover Password';
$lang['recover_email'] = 'Enter Email for password recovery';
$lang['registered_telegram'] = 'I registered via Telegram';





$lang['password_new'] = 'Enter new password';
$lang['welcome_back'] = 'Welcome Back';
$lang['login_header'] = 'Login into your account';
$lang['login_with'] = 'Login with:';
$lang['no_account'] = "Don't have an account?";
$lang['sign_up'] = 'Sign up!';





$lang['login_ci'] = 'Complete Login Register system in Codeigniter';





$lang['email_confirm'] = 'Confirm your e-mail';
$lang['email_confirm_sent'] = 'We have sent a confirmation email. Check your email!';
$lang['email_mistake'] = 'If you made a mistake and entered the email incorrectly, you can reset it and enter the data for a new one.';
$lang['email_reset'] = 'Reset email';
$lang['email_remove'] = 'Are you sure you want to delete your email from your account?';






$lang['collection_end'] = 'Окончание сбора';
$lang['paid_applications'] = 'Оплаченых заявок';
$lang['allocation_sum'] = 'Сумма аллокации';
$lang['applications_none'] = 'Пока у вас нет оплаченных заявок';





$lang['collection_planned'] = 'Планируется сбор';
$lang['collection_soon'] = 'Скоро сбор';
$lang['collection_current'] = 'Идет сбор';
$lang['collection_over'] = 'Сбор окончен';
$lang['collection_canceled'] = 'Сбор отменен';
$lang['collection_start_after'] = 'Сбор начнется через ';
$lang['collection_end_after'] = 'Сбор завершится через ';
$lang['in_tir'] = 'в тире';
$lang['in_reserve'] = 'из них в резерве';
$lang['amount_left'] = 'Осталось';
$lang['participate'] = 'Участвовать';
$lang['limit_over'] = 'Ваш лимит исчерпан';
$lang['collection_closed'] = 'Сбор средств закрыт';
$lang['allocation'] = 'Аллокация';
$lang['kyc_message'] = 'Вы не можете участвовать в проектах, пока не пройдете KYC';
$lang['go_profile'] = 'Перейти в профиль';
$lang['project_auth'] = 'Для участия в проекте необходимо авторизоваться';
$lang['project_blockchain'] = 'Данный проект использует блокчейн';
$lang['application_confirmation'] = 'После оплаты заявки, не забудьте подтвердить тот кошелек';
$lang['appilcation_transfer'] = 'на который будет производится перевод токенов проекта.';
$lang['wallet_none'] = 'Вы пока не указали кошелек';
$lang['wallet_none_content'] = 'в вашем профиле. Перейдите в профиль и заранее укажите его. Без указания данного кошелька участие в проекте невозможно.';
$lang['sum_commission'] = 'Сумма с комиссией';
$lang['date_create'] = 'Дата создания';
$lang['overflow_reason'] = 'Переплата могла образоваться по двум причинам:';
$lang['overflow_allocation'] = 'вы оплатили больше, чем ваша аллокация в тире';
$lang['overlow_overdue'] = 'вы оплатили, когда заявка уже была просрочена и вам не хватило аллокации в проекте';
$lang['overflow_end'] = 'По итогам окончания сбора, переплата будет либо засчитана и сумма аллокации будет пересчитана, либо будет произведен возврат с удержанием штрафа.';
$lang['project_resources'] = 'Официальные ресурсы проекта';
$lang['project_sum_enter'] = 'На какую сумму вы хотите войти в проект';
$lang['commission_relevant'] = 'C учетом комиссии';
$lang['application_note'] = 'ВАЖНО: После подачи заявки у вас будет 10 минут для осуществления перевода. На это время будет зарезервирована необходимая сумма в аллокации';
$lang['project_none'] = 'Не существует такого проекта';





$lang['tir_sum'] = 'Сумма сбора в тире';
$lang['collected'] = 'Собрано';
$lang['in_reserve'] = 'В резерве';
$lang['selling_start'] = 'Старт продаж';
$lang['allocation_max'] = 'Max-аллокация';





$lang['kondr_welcome'] = 'Добро пожаловать в DAO KONDR VC';
$lang['kondr_header'] = 'Станьте частью закрытого клуба инвесторов и получите возможность участвовать в инвестиционных раундах откройте доступ согласно системе рейтинга. Уровни открываются согласно наличию KON на вашем счету';
$lang['rating_system'] = 'Система рейтинга';
$lang['wallet_connected'] = 'Кошелек привязан';
$lang['wallet_connect'] = 'Привязать кошелек';
$lang['kyc_done'] = 'KYC пройден';
$lang['kyc_do'] = 'Пройти KYC';
$lang['kon_bought'] = 'KON куплены';
$lang['kon_buy'] = 'Купить KON';
$lang['levels'] = 'Уровни';
$lang['sum'] = 'Сумма';
$lang['kon_blocking'] = 'блокировки KON:';
$lang['allocation_average'] = 'Средняя аллокация';
$lang['project_single'] = 'на 1 проект:';
$lang['projects_available'] = 'Вам доступны следующие проекты:';
$lang['projects_last'] = 'Последние проекты';






$lang['project_participation'] = 'на участие в проекте';
$lang['transfer_do'] = 'Произведите перевод';
$lang['wallet_from'] = 'с вашего кошелька';
$lang['wallet_transfer_do'] = 'Произведите перевод с вашего кошелька';
$lang['bep_sum'] = 'в сети BEP 20 в сумме';
$lang['application_tranfer'] = 'Одна заявка - один перевод!';
$lang['application_limit'] = 'Оплачивать заявку двумя платежами нельзя.';
$lang['payment_less'] = 'При необходиости, вы можете оплатить меньшую сумму от заявки.';
$lang['test_btn_comment'] = 'Псоле этого нажмите кнопку проверить';
$lang['operation_comment'] = 'Поступление операции в систему происходит в течении нескольких минут. Если операция пока не поступила, просто немного подождите';
$lang['payment_time'] = 'Осталось времени на оплату';
$lang['payment_transfer_address'] = 'Отправьте необходимую сумму на следующий адрес';
$lang['transfer_rules'] = 'Перевод необходимо произвести в сети BEP 20 в USDT/BUSD.';
$lang['transfers_comment'] = 'Переводы с обменников, бирж и другие ошибочные не учитываются и возвращаются со штрафом 10%';
$lang['application_cancel'] = 'Отменить заявку';
$lang['payment_check'] = 'Проверить оплату';
$lang['project_info_add'] = 'Для участия в проекте необходимо заполнить следющую информацию';
$lang['wallet_save'] = 'Сохранить кошелек';
$lang['application_number'] = 'Номер заявки';
$lang['enrollment_amount'] = 'Сумма зачисления';
$lang['application_time_end'] = 'Ваша заявка просрочена так как оплата не поступила в течении 10 минут после оформления заявки.';
$lang['payment_scenario'] = 'Если сейчас поступит оплата, то она будет обработана следующим образом';
$lang['tir_comment'] = '- Если в тире будет в этот момент свободная (не оплаченная и не забронированная) аллокация, то заявка будет удовлетворена полностью';
$lang['allocation_comment'] = '- Если аллокации свободной не будет хватать, то будет выдана та, которая будет свободна, а оставшаяся часть уйдет в переплату';
$lang['application_comment'] = 'Если вы еще не оплачивали, то вы можете оформить новую заявку.';
$lang['transactions_free'] = 'Нераспределенные транзакции';
$lang['application_none'] = 'Не существует такой заявки';





$lang['application_user'] = 'Ваши заявки';
$lang['application_user_none'] = 'У вас нет заявок.';





$lang['tir_follow_info'] = 'info how to follow our tirs';





$lang['tir_none'] = 'Тир не определен';




$lang['kon_buy_do'] = 'Buy KON';
$lang['vesting_claim'] = 'Vesting (claim)';
$lang['bridge'] = 'Bridge';





$lang['wallet_confirmation'] = 'Подтверждение кошелка';
$lang['wallet_ownership'] = 'Для продолжения работы с сервисом необходимо подтвердить собственность кошелька';
$lang['wallet_setting'] = 'Настройка кошелька';
$lang['profile_setting'] = 'Настройка учетной записи';
$lang['kyc_process'] = 'Прохождение KYC';
$lang['cabinet_access'] = 'С 11 февраля, доступ в личный кабинет смогут получить только пользователи,	прошедшие KYC. Это необходимо делать по соображениям безопасности и в соответствии с международными  законодательством по противодействию отмыванию денег и идентификации личности контрагента.';
$lang['kyc_comment'] = 'Внимание: для прохождения KYC вы не можете использовать документы США. Это требование проектов.';
$lang['cabinet_denial'] = 'У вас есть не принятые соглашения, доступ к личному кабинету ограничен. До подписания, вы не сможете принимать участие в новых проектах.';





$lang['buy_uniswap'] = 'Buy On Uniswap now';
$lang['buy_dextools'] = 'Buy On Dextools now';
$lang['buy_pancake'] = 'Buy On Pancake now';
$lang['buy_pookoin'] = 'Buy On Pookoin now';
$lang['kon_bep_access'] = 'Access the $KON ERC20 <=> BEP20 bridge by clicking here';